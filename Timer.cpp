
#include <sys/time.h>
#include <sys/resource.h>
#include "Timer.h"

/*
 * Start the global stopwatch
 */
void
Timer::start()
{
    struct rusage res;

    getrusage( RUSAGE_SELF, &res );
    t = (double) res.ru_utime.tv_sec +
	(double) res.ru_stime.tv_sec +
	(double) res.ru_utime.tv_usec / 1000000.0 +
	(double) res.ru_stime.tv_usec / 1000000.0;
}

/*
 * Get the elapsed time since the stopwatch was started
 */
double
Timer::elapsedTime()
{
    struct rusage res;

    getrusage( RUSAGE_SELF, &res );
    return( (double) res.ru_utime.tv_sec +
            (double) res.ru_stime.tv_sec +
            (double) res.ru_utime.tv_usec / 1000000.0 +
            (double) res.ru_stime.tv_usec / 1000000.0
            - t );
}

