#ifndef _ZDD_H_
#define _ZDD_H_

#include <vector>
#include <iostream>
#include <boost/functional/hash.hpp>
#include <unordered_map>
#include <unordered_set>
#include <queue>

#include <memory>
#include <set>

#include <span>

#include <optional>

#include <cassert>

#include <math.h>
#include <stdio.h>

#include "Types.hpp"
#include "Options.hpp"

// a reference to a ZDD node. It's just an int, but mostly opaque. It
// is meant to be passed by value only and it is converted to a
// ZDDnode* by ZDD::deref. A ZDDnode* should only ever be a local
// variable and not be stored anywhere. It can be invalidated by a
// call to newNode() or to compact(). A ZDDnoderef can be invalidated
// by a call to compact(), but will be updated if it is stored in one
// of the roots recognized by compact().
struct ZDDnoderef {
    int id{-1};

    ZDDnoderef() {}
    explicit ZDDnoderef(int i) : id(i) {}
    void reset() { id = -1; }
    void reset(int i) { id = i; }

    bool nonnull() const { return id >= 0; }
    bool isnull() const { return id < 0; }
    bool operator==(ZDDnoderef o) const { return id == o.id; }
};

struct ZDDnode
{
    ZDDnoderef falseChild;
    int falseEdgeCounter{0};
    ZDDnoderef trueChild;
    int trueEdgeCounter{0};
};

namespace detail {
    struct DTBuildState
    {
        using iterator = std::vector<Set>::iterator;
        std::vector<iterator> node_begin, node_end;
        std::vector<Set> vals;

        void init_node(ZDDnoderef n, iterator b, iterator e)
        {
            if (node_begin.size() <= static_cast<size_t>(n.id)) {
                node_begin.resize(n.id+1);
                node_end.resize(n.id+1);
            }
            node_begin[n.id] = b;
            node_end[n.id] = e;
        }
    };
}


struct DTmask
{
    // set mask[node.id] = false to hide a node
    int marker{0};
    std::vector<uint32_t> nonmask;
    std::vector<ZDDnoderef> unmasked;

    bool masked(ZDDnoderef n) const
    {
        return !nonmask[n.id];
    }

    void unmask(ZDDnoderef n)
    {
        if (!nonmask[n.id]) {
            nonmask[n.id] = marker;
            unmasked.push_back(n);
        }
    }

    void reset()
    {
        for(auto n : unmasked)
            nonmask[n.id] = 0;
        unmasked.clear();
    }
};

class ZDD
{
    public:
        /*
         *  Constructors
         */
        ZDD(int nvars, std::span<Set> values, std::span<Score> costs, int j,
            dtree_construction h);

        ZDD(int nvars, std::span<Set> values, std::span<Score> costs, int j,
            dtree_construction h, std::span<int> bestorder);

        void removeTrueChildrenOf(ZDDnoderef currentNodeRef, int currentLevel);

        void pruneParentSet(Set ps);

        int getVarIndex() {return thisVariable;}

        void printNodesByLevels();

        void printParentsByLevels();

        // returns true if it contains a value that is a subset of
        // precedingVars under mask
        bool hasValueThatIsSubsetOf(Set precedingVars, const std::optional<DTmask>& mask);

        // returns a specific value that is a subset of precedingVars
        int getSubsetOf(Set precedingVars, const std::optional<DTmask>& mask);

        // returns a value with minimum cost among those that are subsets of prec
        int getMincostSubsetOf(Set prec, const std::optional<DTmask>& mask);

        // is this set in the tree?
        bool valueExists(Set ps);

        void restoreValue(Set value);

        void restoreValues(const std::vector<int>& values);

        int getMaxWidth() { return width; }

        int getMaxDepth() { return maxDepth; }

        int getNbNodes() { return nbNodes; }

        DTmask getnewmask() {
            DTmask rv{};
            reset_mask(rv);
            return rv;
        }

        // hide everything
        void reset_mask(DTmask& mask);

        // unhide a value
        void unmask_value(DTmask& mask, Set a);

        void set_node_visit_stat(uint64_t *stat) { node_visit_stat = stat; }
        void set_minc_node_visit_stat(uint64_t *stat) { minc_node_visit_stat = stat; }

    private:
        // root and effective root: effective root is the first node with
        // two children. At the root of the search tree, these will be the
        // same. But as we prune values, the actual root of this tree may
        // get one of its children chopped off, so all queries will go in
        // the same direction, until we meet a node with two children
        ZDDnoderef root, froot;
        int nvars;
        int thisVariable; //variable for which this ZDD is constructed
        Set unionOfValues;
        int width;
        int maxDepth;
        int nbNodes;
        int nbLevels;
        dtree_construction heuristic;
        // best ordering found (so far), to be used as ordering for
        // the tree, if heuristic is BO or BO_VEB
        std::span<int> bestOrdering;

        std::vector< ZDDnode > nodes;
        std::vector< std::vector< ZDDnoderef >> nodesAtLevel;
        std::vector<int> nodeLevel;

        std::vector< int > widthAtLevel;

        struct BranchInfo {
            // the variable this node branches on. -1 for leaves
            int varindex{-1};
            // this node is consistent with all sets s such that s &
            // mask == path. Some values in the path are not
            // necessarily branching decisions. For example if we
            // branch on v1=true and all remaining sets contain v2 but
            // not v3, then path = 011, mask = 111, meaning the node
            // is only consistent with sets that have all three
            // variables set as in 'path'
            Set path{emptySet};
            Set mask{emptySet};
        };
        std::vector<BranchInfo> node_branchinfo;

        std::vector<Score> node_costinfo;

        // true if the Set s is consistent with the values in the path
        // leading to n, including implications. In other words, the
        // branching decisions leading from the root to n do not
        // exclude the possibility that a leaf reachable from n
        // corresponds to s
        bool consistent(Set s, ZDDnoderef n);

        // as above, but true if there may exist a leaf that
        // corresponds to a subset of s
        bool subset_consistent(Set s, ZDDnoderef n);

        // the value to which each leaf corresponds
        std::vector<int> node_val;
        // reverse mapping, from value index to leaf node
        std::vector<ZDDnoderef> val_node;

        // node's parent
        std::vector<ZDDnoderef> parent;

        // buffers used in restoreValues
        std::vector<int> restore_gained_paths;
        std::vector<ZDDnoderef> restore_q;
        std::vector<uint8_t> restore_inq;

        uint64_t *node_visit_stat{nullptr}, *minc_node_visit_stat{nullptr};

        // buffers used in minCostSearch
        struct qelem {
            ZDDnoderef r;
            Score lb;
        };
        struct qelem_compare {
            bool operator()(const qelem& e1, const qelem& e2) {
                return e1.lb > e2.lb;
            }
        };
        std::priority_queue<qelem, std::vector<qelem>, qelem_compare> minCostSearch_q;

        int varIndex(ZDDnoderef ref) { return node_branchinfo[ref.id].varindex; }
        void setVarIndex(ZDDnoderef ref, int idx) {
            node_branchinfo[ref.id].varindex = idx;
        }

        ZDDnoderef newNode();
        ZDDnode* deref(int id);
        bool isSink(ZDDnoderef r);

        void initialize(std::span<Set> values, std::span<Score> costs);

        void mergeNodes();
        bool markReachable(ZDDnoderef n);
        int updateCounters(ZDDnoderef n);

        void verifyValues(std::span<Set> values)
        {
            for(auto& value : values) {
                auto currentNodeRef = root;
                auto currentNode = deref(currentNodeRef.id);
                while (!isSink(currentNodeRef)) {
                    assert(consistent(value, currentNodeRef));
                    int var = varIndex(currentNodeRef);
                    if(element_of(var, value)){
                        currentNodeRef = currentNode->trueChild;
                        currentNode = deref(currentNode->trueChild.id);
                        continue;
                    }
                    else{
                        currentNodeRef = currentNode->falseChild;
                        currentNode = deref(currentNode->falseChild.id);
                        continue;
                    }
                }
                assert(isSink(currentNodeRef));
            }
        }

        int verifyPaths(ZDDnoderef);

        std::vector<uint8_t> seen;

        void fixEffectiveRoot();
        int followSubsetOf(ZDDnoderef node, Set precedingVars,
                           const std::optional<DTmask>& mask);

        int minCostSearch(ZDDnoderef node, Set precedingVars,
                          Score *ub,
                          const std::optional<DTmask>& mask);

        bool unmasked(ZDDnoderef node, const std::optional<DTmask>& mask);
        bool hasTrue(ZDDnoderef node, const std::optional<DTmask>& mask);
        bool hasFalse(ZDDnoderef node, const std::optional<DTmask>& mask);

        struct variableChoice {
            int var;
            Set implied;
            Set mask;
            Set all_intersection;
        };
        // helper for those heuristics which do not gather implied
        // variables as a side effect (lex, bestordering). Fills in
        // everything except var in variableChoice that it returns
        ZDD::variableChoice getImplied(detail::DTBuildState& state,
            int currentLevel, ZDDnoderef curref, Set seenVars, Set path);
        // lexicographic ordering
        variableChoice chooseBranchVar_Lex(detail::DTBuildState& state,
            int currentLevel, ZDDnoderef curref, Set seenVars, Set path);
        // same as best solution found so far. The reasoning is that
        // variables that appear early in a good solution will be early in
        // most branches we explore, therefore most sets we query will
        // contain these variables. The earlier we meet all variables
        // contained in a set we query, the earlier we ensure that we will
        // not have to backtrack to answer positively
        variableChoice chooseBranchVar_BO(detail::DTBuildState& state,
            int currentLevel, ZDDnoderef curref, Set seenVars, Set path);
        // information gain heuristic. Reasoning is small decision tree ->
        // not too bad worst case
        variableChoice chooseBranchVar_IG(detail::DTBuildState& state,
            int currentLevel, ZDDnoderef curref, Set seenVars, Set path);
        // dispatch to one of the above according to dtree_construction
        // heuristic
        variableChoice chooseBranchVar(detail::DTBuildState& state,
            int currentLevel, ZDDnoderef curref, Set seenVars, Set path);

        void exploreNode(detail::DTBuildState& state, ZDDnoderef currentNodeRef,
            int currentLevel, int falseCounter, int trueCounter, Set seenVars,
            Set path);

        void constructBinaryTree(std::span<Set> values, std::span<Score> costs);

        // helpers for traversal

        // 1. visit both direct children, if they exist, i.e., this
        // can make 0, 1, or 2 calls. call cb for each
        template<typename CB>
        void for_all_children(ZDDnoderef root, CB cb);

        // 2. visit all descendants in the subtree rooted at root and
        // call cb for each. If cb returns true, do not descend any
        // more (but does not stop completely)
        template<typename CB>
        void for_all_descendants(ZDDnoderef root, CB cb);

        // lay out the tree nodes according to the van Embe Boas
        // layout: the top half levels are together, i.e., if the top
        // half levels have k nodes, they are in positions 0...k-1,
        // (no order specified among them). Then, recursively apply
        // this to the top half and each of the subtrees starting at
        // the middle level.
        void relayout();
        void relayout_levels(ZDDnoderef root, int from, int to,
                               std::vector<ZDDnode>& newnodes,
                               std::vector<ZDDnoderef>& newrefs);

        // helpers for relayout()

        // 1. remap a single reference to its new position according to newrefs
        void remap_ref(ZDDnoderef& r, const std::vector<ZDDnoderef>& newrefs);

        // 2. remap a vector of references to their new positions
        // according to newrefs
        void remap_refs(std::vector<ZDDnoderef>& v,
                        const std::vector<ZDDnoderef>& newrefs);

        // 3. remap a reverse lookup vector (a mapping from a node to
        // something else)
        template<typename T>
        void remap_reverse(std::vector<T>& v,
                           const std::vector<ZDDnoderef>& newrefs);

        // annotate each leaf with the index of the Set it corresponds
        // to in 'values'
        void annotateLeaves(std::span<Set> values, std::span<Score> costs);

        // for bottom-up iteration, pointer from each node to its parent
        void setParent(ZDDnoderef child, ZDDnoderef parent);
};

#endif // _ZDD_H_
