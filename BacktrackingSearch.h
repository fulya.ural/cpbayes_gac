/*
 *  Routines for performing backtracking search on an instance of
 *  the combinatorial optimization problem for learning a Bayesian
 *  network from data.
 */
#ifndef _BACKTRACKINGSEARCH_H_
#define _BACKTRACKINGSEARCH_H_

class Instance;
class Timer;
class Bound;
class DAG;
class Memo;

class BacktrackingSearch
{
    public:
        BacktrackingSearch( Instance *instance, const Options& opt );
        ~BacktrackingSearch();

        /*
         *  Find the minimum cost solution. Returns the assignments
         *  to the variables that gives the lowest cost and returns
         *  the cost of that solution.
         */
        void solve( int *ordering, int *solution, Score *cost );

        /* Obvious, maybe */
        void printStats();
    private:

        const Options& opt;

        int *nbOtherFailuresPerLevel;
        int *nbClusterFailuresPerLevel;
        void initialize();
        Instance *instance;
        std::unique_ptr<Timer> timer;

        // set to true if we proved optimality
        bool optimum{false};

        /*
         *  Current (partial) solution and cost of the solution.
         */
        int *currentParents;
        int *currentOrdering;
        int *currentDepth;
        bool *instantiatedP;
        Score currentCost;

        /*
         *  Best solution found so far and cost of best solution,
         *  which is an upper bound on the cost of any optimal
         *  solution, and a lower bound on the cost.
         */
        int *bestParents;
        int *bestOrdering;
        Score lbCost;
        Score ubCost;
        bool restart; // if true then current backtrack ends and a new search is done until restart is false
        long long nbrestarts{0};
        long long nbnodes{0};
        std::set<long long> incompletenodes;

        /*
         *  Symmetry breaking based on I-equivalence.
         *
         *  Symmetry breaking must be treated differently from
         *  constraint propgatation for the purposes of memoization.
         *  Avoid the situation where a node is inserted into the
         *  table with a given cost and then is immediately rejected
         *  by symmetry breaking; subsequently a node of equivalent
         *  cost is looked-up in the table and is rejected, with
         *  the unintended consequence that both are rejected.
         *
         *  Only break symmetries if the costs of the two symmetric
         *  subnetworks are equal, otherwise there is a negative
         *  interaction with cost-based pruning.
         *
         *  Symmetry breaking is a satisfiability test. It could also
         *  be propagated, but this does not seem to improve performance.
         */
        bool symmetryEquivalent( int v, int d, int currentLevel );
        Score swapCost( int x, int y );

        /*
         *  Pruning at root.
         *
         *  Cost-based pruning rule:
         *      Let p, p' in dom(x), p != p'.
         *      If p subseteq p' and cost(p) <= cost(p'), then prune p'.
         *
         *  Symmetry breaking based on symmetric variables.
         */
        void costPruning();
        void symmetryVariables();

        /*
         *  Generate the permutations of the ordering[i..n-1] to
         *  find the ordering that gives the lowest cost.
         */
        void perm( Set ancestors, int *ordering, int i, int n, Score *cost );

        /*
         *  Constraint propagation.
         *
         *  Reduce the domains of the variables given the new
         *  instantiation a = p, where the assignment a = p denotes
         *  that parent variable x_a has parents p in the DAG.
         *
         *  Reduce the domains of the variables given the new
         *  instantiation v = a, where the assignment v = a denotes
         *  that parent variable x_a is in the v'th position in
         *  the total ordering of the parent variables.
         *
         *  Reduce the domains of the variables given the new
         *  instantiation v = d, where the assignment v = d denotes
         *  that parent variable x_a that occurs at the v'th
         *  position in the total ordering has depth d.
         */
        bool propagate( int v, int a, int p, int d, int currentLevel );
        bool propagateParents( int v, int d, int currentLevel );
        bool propagateOrdering( int v, int a, int d, int currentLevel );
        bool propagateDepth( int v, int d, int currentLevel );

        /*
         *  Restore domains to the state before constraint propagation.
         *
         *  checking[i][j] is true iff the domain of variable j changed
         *  during constraint propagation as a result of instantiating
         *  variable i.
         */
        bool **checkingO;
        bool **checkingD;
        void restoreDomains( int v, int currentLevel );

        /*
         *  Choose the next variable to be branched on.
         *  Returns -1 if all variables are instantiated.
         */
        int getNextVariable( int currentLevel );

        /*
         *  valueOrdering contains a permutation of the parent
         *  variables that is a low cost solution.
         *
         *  Here: just use the bestOrdering found during local search.
         *  A dynamic value ordering based on lowest cost consistent
         *  parent set of each parent variable gave poorer performance.
         *  A dynamic value ordering based on largest domain size of
         *  the parent variables gave just slightly better performance.
         */
        int *valueOrdering;

        /*
         *  Routines for finding lower and upper bounds.
         */
        void establishBounds();
        std::unique_ptr<Bound> bound;

        /*
         *  Routines for memoizing partial solutions.
         */
        std::unique_ptr<Memo> memo;

        /*
         *  Routines for enforcing acyclic constraint on
         *  parent variables.
         */
        std::unique_ptr<DAG> dag;

        /*
         *  Solve the instance by backtracking search.
         */
        void backtrack( int currentLevel );

        /*
         *  Tracking, debugging, statistics.
         */
        long long nodesVisited;
        double lowerBoundTime;
        double upperBoundTime;
        double dtTime{0.0};
};

#endif // _BACKTRACKINGSEARCH_H_
