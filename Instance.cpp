/*
 *  Instance: An instance of the combinatorial optimization problem
 *            for learning a Bayesian network from data.
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <algorithm>
#include <cassert>
#include "Graph.h"
#include "Instance.h"
#include "ZDD.h"
#include <set>

#include <boost/functional/hash.hpp>
#include <unordered_map>

/*
 *  Constructor for an instance.
 */
Instance::Instance( const Options& options )
    : opt(options)
    , domainP(options)
{
#ifdef DYNSET
    domainSizeP = new int [MAX_VARS]();      // domain size of parent variables
    domainP = new Value* [MAX_VARS]();       // domain of parent variables
    domainSizeO = new int [MAX_VARS]();      // domain size of ordering variables
    domainO = new int* [MAX_VARS]();         // domain of ordering variables
    domainSizeD = new int [MAX_VARS]();      // domain size of depth variables
    domainD = new int* [MAX_VARS]();         // domain of depth variables
    domainSizeCurrentP = new int [MAX_VARS]();
    domainSizeCurrentO = new int [MAX_VARS]();
    domainSizeCurrentD = new int [MAX_VARS]();
    lastValidP = new int [MAX_VARS]();
    necessaryParents = new Set(MAX_VARS)[MAX_VARS]();
#endif

    n = 0;

    for( int i = 0; i < MAX_VARS; i++ ) {
        domainSizeO[i] = 0;
        domainO[i] = NULL;
        domainSizeD[i] = 0;
        domainD[i] = NULL;
    }

    initialize( opt.inputfile.c_str() );
}

/*
 *  Destructor for an instance.
 */
Instance::~Instance()
{
    for( int i = 0; i < n; i++ ) {
        delete[] domainO[i];
    }
    for( int i = 0; i < n; i++ ) {
        delete[] domainD[i];
    }
    if (initial_ordering) delete[] initial_ordering;
#ifdef DYNSET
    delete[] domainSizeO;      // domain size of ordering variables
    delete[] domainO;         // domain of ordering variables
    delete[] domainSizeD;      // domain size of depth variables
    delete[] domainD;         // domain of depth variables
    delete[] domainSizeCurrentP;
    delete[] domainSizeCurrentO;
    delete[] domainSizeCurrentD;
    delete[] lastValidP;
    delete[] necessaryParents;
#endif
}

/*
 *  Read in the optimization problem.
 */
void
Instance::initialize( const char *fileName )
{
    FILE *fp;

    if( (fp = fopen( fileName, "r" )) == NULL ) {
        fprintf( stderr, "Could not open file %s\n", fileName );
        exit( 6 );
    }

/*
=================================================================
5
0 5
-577.904797 0
-550.387826 1 1
-550.387826 1 2
-550.387826 1 3
-550.387826 1 4
1 2
-1020.304609 0
-992.787639 1 0
2 2
-1020.304609 0
-992.787639 1 0
3 2
-1020.304609 0
-992.787639 1 0
4 2
-1020.304609 0
-992.787639 1 0
=================================================================
*/

    /*
     *  Get the number of random variables.
     */
    fscanf( fp, "%d\n", &n );
    if( n > MAX_VARS ) {
        fprintf( stderr, "Number of variables out of range %d\n", n );
        exit( 6 );
    }

    domainP.setNvars(n);
    std::vector<Value> buffer;
    /*
     *  For each random variable...
     */
    for( int i = 0; i < n; i++ ) {
        /*
         *  Get the parent variable name and domain size.
         */
        int v;
        int d;
        fscanf( fp, "%d %d\n", &v, &d );
        if( d <= 0 ) {
            fprintf( stderr, "Parent set too small for (%d <= 0)\n", d );
            exit( 6 );
        }

        buffer.clear();
        buffer.resize(d);
        for( int a = 0; a < d; a++ ) {
            /*
             *  Get the domain elements.
             */
            double s;
            fscanf( fp, "%lf", &s );
            buffer[a].cost = (Score)round((-s) * scale);
            buffer[a].valid = -1;
            int l;
            fscanf( fp, "%d", &l );
            buffer[a].cardinality = l;
            if( l >= MAX_VARS ) {
                fprintf( stderr, "Parent set too large (%d >= %d)\n", l, MAX_VARS );
                exit( 6 );
            }
            buffer[a].parentSet = emptySet;
            for( int k = 0; k < l; k++ ) {
                /*
                 *  Read in a parent set.
                 */
                int p;
                fscanf( fp, "%d", &p );
                add_element( p, buffer[a].parentSet );
            }
            fscanf( fp, "\n" );
        }
        domainP.addVariable(std::move(buffer));
        // initialize the shim, so our operator[] can return by reference
        domainPshims.push_back(domainP[i]);
    }

    /*
     *  For each random variable...
     */
    for( int i = 0; i < n; i++ ) {
        /*
         *  Set the domains of the ordering variables.
         */
        domainSizeO[i] = n;
        domainSizeCurrentO[i] = n;
        domainO[i] = new int[n]();
        for( int j = 0; j < n; j++ ) {
            domainO[i][j] = -1;
        }
        /*
         *  Set the domains of the depth variables.
         */
        domainSizeD[i] = i+1;
        domainSizeCurrentD[i] = i+1;
        domainD[i] = new int[n]();
        for( int j = 0; j <= i; j++ ) {
            domainD[i][j] = -1;
        }
        for( int j = i+1; j < n; j++ ) {
            domainD[i][j] = 0;
        }
    }

    //dumpInstance();

    fclose( fp );
}

void Instance::reconstructDtrees(std::span<int> bestOrdering)
{
    domainP.reconstructDtrees(bestOrdering);
}

/*
 *  Print out an instance for debug purposes.
 */
void
Instance::dumpInstance()
{
    /*
     *  Vertex variables.
     */

    int nFamilies = 0;
    for( int v = 0; v < n; v++ ) {
        printf( "parent set of v_%d\n", v );
        nFamilies += domainP[v].size();
        for (int a : domainP[v]) {
            printf( "\t" );
            printParentSet( v, a );
            printf( " %8.3f\n", (double)getCost( v, a ) / scale );
        }
    }
    printf( "nFamilies = %d\n", nFamilies );

    /*
     *  Ordering variables.
     */
    for( int v = 0; v < n; v++ ) {
        printf( "domain of o_%d:", v );
        for( int a = 0; a < domainSizeO[v]; a++ ) if( validO( v, a ) ) {
            printf( " %d", a );
        }
        printf( "\n" );
    }

    /*
     *  Depth variables.
     */
    for( int v = 0; v < n; v++ ) {
        printf( "domain of d_%d:", v );
        for( int a = 0; a < domainSizeD[v]; a++ ) if( validD( v, a ) ) {
            printf( " %d", a );
        }
        printf( "\n" );
    }
}

/*
 *  Print out an instance for debug purposes.
 */
void
Instance::dumpVariable(int v)
{
    /*
     *  Vertex variables.
     */
    printf( "parent set of v_%d\n", v );
    for (int a : domainP[v]) {
        printf( "\t" );
        printParentSet( v, a );
        printf( " %8.3f\n", (double)getCost( v, a ) / scale );
    }
}

void
Instance::dumpState(
    int currentLevel,
    int *currentParents,
    int *currentOrdering,
    int *currentDepth,
    bool *instantiatedP )
{
    /*
     *  Ordering variables.
     */
    for( int v = 0; v <= currentLevel; v++ ) {
        printf( "\tdomain of o_%d:", v );
        int w = currentOrdering[v];
        printf( " %d:", w );
        printf( "\t" );
        printParentSet( w, currentParents[w] );
        printf( "\tdepth = %d, cost = %8.3f\n",
            getDepthParentSet(
                currentOrdering, currentDepth, currentLevel+1,
                w, currentParents[w] ),
            (double) getCost( w, currentParents[w] ) / scale );
    }
}

void
Instance::dumpStateFull(
    int currentLevel,
    int *currentParents,
    int *currentOrdering,
    int *currentDepth,
    bool *instantiatedP )
{
    /*
     *  Vertex variables.
     */
    for( int v = 0; v < n; v++ ) {
        printf( "parent set of v_%d\n", v );
        if( !instantiatedP[v] ) {
            for (int a : domainP[v]) {
                printf( "\t" );
                printParentSet( v, a );
                printf( " %8.3f\n", (double)getCost( v, a ) / scale );
            }
        }
        else {
            printf( "\t" );
            printParentSet( v, currentParents[v] );
            printf( " depth = %d, cost = %8.3f\n",
                getDepthParentSet(
                        currentOrdering, currentDepth, currentLevel+1,
                        v, currentParents[v] ),
                (double)getCost( v, currentParents[v] ) / scale );
        }
    }
    printf( "\n" );

}

void
Instance::dumpDebug(
    bool *instantiatedP,
    std::vector< std::vector<Score> > deltaCost,
    Set variables )
{
    /*
     *  Vertex variables.
     */
    for( int v = 0; v < n; v++ ) if( element_of( v, variables ) ) {
        printf( "parent set of v_%d\n", v );
        if( !instantiatedP[v] ) {
            for (int a : domainP[v]) {
                printf( "\t" );
                printf("%d  ", a);
                printf( " %8.3f ", (double)getCost( v, a ) / scale );
                printf( " %8.3f ", (double)deltaCost[v][a] / scale );
                printf( " %8.3f ", ( (double)getCost( v, a ) - (double)deltaCost[v][a] ) / scale );
                printParentSet( v, a );
                printf("\n");
            }
        }
        else {
            printf( "INS\n" );
        }
    }
    printf( "\n" );

}

/*
 *  Number of random variables in instance.
 */
int
Instance::nVars() const
{
    return( n );
}

/*
 *  Domain size of
 *      parent variable v,
 *      ordering variable v,
 *      depth variable v.
 */
int Instance::getDomainSizeP( int v ) const { return( domainP[v].totalSize() ); }
int Instance::getDomainSizeO( int v ) const { return( domainSizeO[v] ); }
int Instance::getDomainSizeD( int v ) const { return( domainSizeD[v] ); }

/*
 *  Current domain size of
 *      parent variable v,
 *      ordering variable v,
 *      depth variable v.
 */
int Instance::getCurrentDomainSizeP( int v ) const { return(domainP[v].size()); }
int Instance::getCurrentDomainSizeO( int v ) const { return(domainSizeCurrentO[v]); }
int Instance::getCurrentDomainSizeD( int v ) const { return(domainSizeCurrentD[v]); }

const char *
Instance::benchmarkName() const
{
    return( opt.inputfile.c_str() );
}

/*
 *  Determine the minimum cost subnetwork (solution) and its cost
 *  for the ordering, given the ancestors.
 */
Score
Instance::minCostOrdering( Set ancestors, int *ordering, int m, int *solution )
{
    Set inOrdering = ancestors;
    Score totalCost = 0;
    for (int i = 0; i < m; i++) {
        int v = ordering[i];
        int a = domainP.minCostSubsetOf_any(v, inOrdering);
        if (solution)
            solution[v] = a;
        Score minCost = domainP[v].getCost(a);
        totalCost += minCost;

        /*
         *  Add the variable just chosen to inOrdering.
         */
        add_element(v, inOrdering);
    }

    return( totalCost );
}

/*
 *  Determine the parent set for the variable v that has the
 *  minimum cost given v comes next in the ordering; i.e.,
 *  all parents of v must be in the ordering.
 */
int
Instance::getMinCostParentSet( int *ordering, int m, int v )
{
    /*
     *  Set inOrdering to true for all vertices that are
     *  in the ordering; otherwise, false.
     */
    Set inOrdering = emptySet;
    for( int i = 0; i < m; i++ ) {
        add_element( ordering[i], inOrdering );
    }
    return domainP.minCostSubsetOf(v, inOrdering);
}

int
Instance::getMinCostParentSet( Set ancestors, int v )
{
    return domainP.minCostSubsetOf(v, ancestors);
}

int
Instance::getMinCostParentSet_any( int *ordering, int m, int v )
{
    /*
     *  Set inOrdering to true for all vertices that are
     *  in the ordering; otherwise, false.
     */
    Set inOrdering = emptySet;
    for( int i = 0; i < m; i++ ) {
        add_element( ordering[i], inOrdering );
    }
    return domainP.minCostSubsetOf_any(v, inOrdering);
}

int
Instance::getMinCostParentSet_any( Set ancestors, int v )
{
    return domainP.minCostSubsetOf_any(v, ancestors);
}

/*
 *  Given parents set a of variable v, determine whether there
 *  exists a parent set b that is of lower cost and is contained
 *  in the induced parent set of a; i.e., where it is known that
 *  all of the variables in the ordering precede variable v.
 */
bool
Instance::containedInIP( int *ordering, int m, int v, int a )
{
    /*
     *  Set inducedParentSet contains all vertices in the
     *  ordering and all vertices in the parent set a.
     */
    Set inducedParentSet = domainP[v].getParentSet(a);
    for( int i = 0; i < m; i++ ) {
        add_element( ordering[i], inducedParentSet );
    }

    /*
     *  Step through the domain elements b of lower cost.
     *  Return true if: b subseteq ip(a).
     */
    for( int b = a-1; b >= 0; b-- ) {
        if( validP( v, b ) ) {
            Set q = domainP[v].getParentSet(b);
            if( subset_of( q, inducedParentSet ) ) {
                return( true );
            }
        }
    }

    return( false );
}

/*
 * As above, with condensed milk
 */
bool
Instance::containedInIP_condensed( int *ordering, int m, int v, const DomainPs::vec_type& dom, size_t idx )
{
    if (idx == 0)
        return false;
    int a = dom[idx];

    /*
     *  Set inducedParentSet contains all vertices in the
     *  ordering and all vertices in the parent set a.
     */
    Set inducedParentSet = domainP[v].getParentSet(a);
    for( int i = 0; i < m; i++ ) {
        add_element( ordering[i], inducedParentSet );
    }

    /*
     *  Step through the domain elements b of lower cost.
     *  Return true if: b subseteq ip(a).
     */
    for( size_t idxb = 0; idxb < idx; ++idxb ) {
        int b = dom[idxb];
        Set q = domainP[v].getParentSet(b);
        if( subset_of( q, inducedParentSet ) ) {
            return( true );
        }
    }

    return( false );
}

/*
 *  Determine the depth of the parent set a for the variable v given
 *  that v comes next in the ordering; i.e., all parents a of v must
 *  exist in the ordering. Does not assume that the depths will be
 *  in ascending order.
 */
int
Instance::getDepthParentSetUnordered( int *ordering, int *depth, int m, int v, int a )
{
    /*
     *  Set inDepth to corresponding depth for all vertices that are
     *  in the ordering; otherwise, n (impossible depth).
     */
    int inDepth[n];
    for( int i = 0; i < n; i++ ) {
        inDepth[i] = n-1;
    }
    for( int i = 0; i < m; i++ ) {
        inDepth[ordering[i]] = depth[i];
    }

    /*
     *  Find the deepest ancestor for the parent set p in the
     *  ordering so far.
     */
    int d = 0;
    Set parentSet = domainP[v].getParentSet(a);
    for( int k = 0; k < n; k++ ) {
        if( element_of( k, parentSet ) ) {
            if( d < 1 + inDepth[k] ) {
                d = 1 + inDepth[k];
            }
        }
    }

    return( d );
}

/*
 *  Determine the depth of the parent set a for the variable v given
 *  that v comes next in the ordering; i.e., all parents a of v must
 *  exist in the ordering. Assumes that the depths will be in
 *  ascending order.
 */
int
Instance::getDepthParentSet( int *ordering, int *depth, int m, int v, int a )
{
    Set p = domainP[v].getParentSet(a);
    if( set_empty( p ) ) {
        return( 0 );
    }

    Set inOrdering = emptySet;
    for( int i = 0; i < m; i++ ) {
        add_element( ordering[i], inOrdering );
    }

    if( !subset_of( p, inOrdering ) ) {
        return( n );

    }
    else {
        /*
         *  Find the deepest ancestor for the parent set p in the
         *  ordering so far.
         */
        int d = 0;
        for( int i = m-1; i >= 0; i-- ) {
            if( element_of( ordering[i], p ) ) {
                d = 1 + depth[i];
                break;
            }
        }
        return( d );
    }
}

/*
 *  Return the cost of domain element a of parent variable v.
 */
Score
Instance::getCost( int v, int a )
{
    assert(v>=0 && a>=0);
    return( domainP[v].getCost(a) );
}

/*
 *  Determine whether x = a and y = b form a covered edge;
 *  i.e., x = P and y = (P union { x }).
 */
bool
Instance::coveredEdge( int x, int a, int y, int b )
{
    if( domainP[x].getCardinality(a)+1 != domainP[y].getCardinality(b) ) {
        return( false );
    }

    int w = -1;
    Set p = domainP[x].getParentSet(a);
    Set q = domainP[y].getParentSet(b);
    if( !subset_of( p, q ) ) {
        return( false );
    }
    for( int k = 0; k < n; k++ ) if( element_of( k, q ) ) {
        /*
         *  Look for a single w in parent set q and not in parent
         *  set p and check that the other elements are equal.
         */
        if( !element_of( k, p ) ) {
            if( w == -1 ) {
                w = k;
            }
            else {
                return( false );
            }
        }
    }

    if( w == x ) {
        return( true );
    }
    else {
        return( false );
    }
}

/*
 *  Determine whether y = b and z = c form a covered edge;
 *  if the covered edge x --> y is flipped to be y --> x.
 *  i.e., x = P
 *        y = (P union { x })
 *        z = (P union { y })
 *  Assumes that it is already known that x --> y is a covered edge.
 */
bool
Instance::flippedCoveredEdge( int x, int a, int y, int b, int z, int c )
{
    if( ((domainP[x].getCardinality(a)+1) != domainP[y].getCardinality(b)) ||
        ((domainP[x].getCardinality(a)+1) != domainP[z].getCardinality(c)) ) {
        return( false );
    }

    Set p = domainP[x].getParentSet(a);
    Set q = domainP[y].getParentSet(b);
    Set r = domainP[z].getParentSet(c);
    if( !subset_of( p, q ) ) {
        return( false );
    }
    if( !subset_of( p, r ) ) {
        return( false );
    }

    add_element( y, p );

    return( set_equal( p, r ) );
}

/*
 *  Look for the case:
 *      w = P
 *      x = P union {w}
 *      y = P union {w,x}
 *      z = P union {x,y}
 *  Assumes that it is already known that w --> x and x --> y
 *  are covered edges.
 */
bool
Instance::triangleCoveredEdge( int w, int a, int x, int b,
                               int y, int c, int z, int d )
{
    if( ((domainP[w].getCardinality(a)+1) != domainP[x].getCardinality(b)) ||
        ((domainP[w].getCardinality(a)+2) != domainP[y].getCardinality(c)) ||
        ((domainP[w].getCardinality(a)+2) != domainP[z].getCardinality(d)) ) {
        return( false );
    }

    Set p = domainP[w].getParentSet(a);
    Set q = domainP[x].getParentSet(b);
    Set r = domainP[y].getParentSet(c);
    Set s = domainP[z].getParentSet(d);
    if( !subset_of( p, q ) ) {
        return( false );
    }
    if( !subset_of( p, r ) ) {
        return( false );
    }
    if( !subset_of( p, s ) ) {
        return( false );
    }

    add_element( x, p );
    add_element( y, p );

    return( set_equal( p, s ) );
}

/*
 *  Print the parent set of element a of variable v.
 */
void
Instance::printParentSet( int v, int a )
{
    Set parentSet = domainP[v].getParentSet(a);
    printf( "{" );
    for( int i = 0; i < n; i++ ) {
        if( element_of( i, parentSet ) ) {
            printf( " %d", i );
        }
    }
    printf( " }" );
}

/*
 *  Print the parent set of element a of variable v.
 */
void
Instance::printSet( Set aSet )
{
    printf( "{" );
    for( int i = 0; i < n; i++ ) {
        if( element_of( i, aSet ) ) {
            printf( " %d", i );
        }
    }
    printf( " }\n" );
}

/*
 *  Print a solution.
 */
void
Instance::printSolution( int *ordering, int *solution, Score cost )
{
    int depth[n];

    for( int i = 0; i < n; i++ ) {
        int v = ordering[i];
        depth[i] = getDepthParentSet( ordering, depth, i, v, solution[v] );
        printf( "\tordering[%d] = %d <--", i, v );
        printParentSet( v, solution[v] );
        printf( "\tdepth = %d\n", depth[i] );
    }
    printf( "\tminimum cost = %0.*f\n", precision, (double)cost / scale );
    for( int i = 0; i < n; i++ ) {
        if (i>0) printf(" ");
        printf("%d", ordering[i]);
    }
    printf("\n");
    assert(cost == minCostOrdering(emptySet, ordering, n, NULL));
}

void
Instance::writeSolution( FILE *fp, int *ordering, int *solution, Score cost )
{
    for( int i = 0; i < n; i++ ) {
        if (i>0) fprintf(fp, " ");
        fprintf(fp, "%d", ordering[i]);
    }
    fprintf(fp, "\n");
    fflush(fp);
}

/*
 *  Routines for querying and setting whether domain element a
 *  of a variable v is valid. A value of -1 indicates valid;
 *  0, ..., n-1 indicate the element was removed at that level
 *  in the search.
 */
bool Instance::validP( int v, int a ) { return( domainP[v].valid(a) ); }
bool Instance::validO( int v, int a ) { return( domainO[v][a] == -1 ); }
bool Instance::validD( int v, int a ) { return( domainD[v][a] == -1 ); }
//int  Instance::getValidP( int v, int a ) { return( domainP[v][a].valid ); }
int  Instance::getValidO( int v, int a ) { return( domainO[v][a] ); }
int  Instance::getValidD( int v, int a ) { return( domainD[v][a] ); }

void
Instance::setValidP( int v, int a, int value )
{
    assert(value >= 0);
    domainP[v].remove(a, value);
}

void
Instance::setValidO( int v, int a, int value )
{
    domainO[v][a] = value;
    if( value == -1 ) domainSizeCurrentO[v]++;
    else              domainSizeCurrentO[v]--;
}

void
Instance::setValidD( int v, int a, int value )
{
    domainD[v][a] = value;
    if( value == -1 ) domainSizeCurrentD[v]++;
    else              domainSizeCurrentD[v]--;
}

/*
 *  Determine set of necessary parents for all vertices.
 *
 *  A vertex v is a necessary parent of vertex w if v occurs
 *  in every currently valid parent set for variable w.
 */
void
Instance::findNecessaryParents()
{
    for( int w = 0; w < n; w++ ) {
        findNecessaryParents(w);
    }
}

/*
 *  Determine set of necessary parents for vertex w.
 *
 *  A vertex v is a necessary parent of vertex w if v occurs
 *  in every currently valid parent set for variable w.
 */
void
Instance::findNecessaryParents( int w )
{
    Set p = universalSet;
    for (int a : domainP[w]) {
        if (set_empty(p))
            break;
        p = intersection(p, domainP[w].getParentSet(a));
    }
    necessaryParents[w] = p;
}

/*
 *  Determine set of necessary edges for variable v.
 *
 *  An edge v --> w is necessary if v occurs in every
 *  currently valid parent set for variable w.
 *
 *  Assumes that findNecessaryParents() has been
 *  called to properly set necessaryParents[].
 */
Set
Instance::necessaryEdges( int v )
{
    Set s = emptySet;
    for( int w = 0; w < n; w++ ) if( v != w ) {
        if( element_of( v, necessaryParents[w] ) ) {
            add_element( w, s );
        }
    }

    return( s );
}

Set
Instance::getParentSet( int v, int a )
{
    return( domainP[v].getParentSet(a) );
}

/*
 *  Update parents sets given that parent variables
 *  v and w are symmetric.
 */
void
Instance::symmetricParentVariables( int v, int w )
{
    /*
     *  Remove v, add w to each parent set.
     */
    for( int i = 0; i < nVars(); i++ ) if( i != w ) {
#ifdef SET128
        std::unordered_map< Set, int, boost::hash<Set> > seenValues;
#else
        std::unordered_map< Set, int > seenValues;
#endif
        for (int a : domainP[i])
                seenValues.insert({domainP[i].getParentSet(a), a});
        for (int a : domainP[i]) {
            Set p = domainP[i].getParentSet(a);
            if( element_of( v, p) ) {
                remove_element( v, p );
                add_element( w, p );
                domainP[i].updateParentSet(a, p);
                auto got = seenValues.find(p);
                if (got != seenValues.end() ){
                    if(getCost(i, a) < getCost(i, got->second))
                        domainP[i].updateCost(got->second, getCost(i, a));
                    setValidP( i, a, n );
                }
            }
        }
    }

    /*
     *  Add v to each parent set in domain of w.
     */
    for (int a : domainP[w]) {
            if( !set_empty( getParentSet(w, a) ) ) {
                auto s = getParentSet(w, a);
                add_element(v, s);
                domainP[w].updateParentSet(a, s);
            }
    }
    printf("added false edge %d --> %d\n",v,w);
}

/*
 *  Add the parent sets as edges in the graph.
 */
void
Instance::addEdges( Graph *graph )
{
    for( int v = 0; v < n; v++ ) {
        for (int a : domainP[v]) {
            Set parentSet = getParentSet(v, a);
            for( int k = 0; k < n; k++ ) {
                if( element_of( k, parentSet ) ) {
                    graph->addEdge( k, v );
                }
            }
        }
    }
}

/*
 *  Add the parent sets as edges in the graph, but.
 *  omitting instantiated variables.
 */
void
Instance::addEdges( Graph *graph, bool *instantiated )
{
    for( int v = 0; v < n; v++ ) if( !instantiated[v] ) {
        for (int a : domainP[v]) {
            Set parentSet = getParentSet(v, a);
            for( int k = 0; k < n; k++ ) if( !instantiated[k] ) {
                if( element_of( k, parentSet ) ) {
                    graph->addEdge( k, v );
                }
            }
        }
    }
}

/*
 *  Add the top parent sets as edges in the graph.
 */
void
Instance::addEdges( Graph *graph, int top )
{
    for( int v = 0; v < n; v++ ) {
        for (int a : domainP[v]) {
            if (a >= top) break;
            Set parentSet = getParentSet(v, a);
            for( int k = 0; k < n; k++ ) {
                if( element_of( k, parentSet ) ) {
                    graph->addEdge( k, v );
                }
            }
        }
    }
}

DomainPs& Instance::getDomainPs()
{
    return domainP;
}

DomainP& Instance::getDomainP(int v)
{
    return domainPshims[v];
}


