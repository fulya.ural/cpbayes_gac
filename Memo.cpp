/*
 *  Routines for memoizing partial solutions.
 */

#include "Instance.h"
#include "Memo.h"

/*
 *  Constructor for Memo.
 */
Memo::Memo()
{
    nLookups = 0;
    nCollisions = 0;
    nTotalProbes = 0;
    nInserted = 0;
    nDominated = 0;
    nUpdated = 0;
    nEqual = 0;
    nFailed = 0;

    key = new Set[mask+1]();
    prefixCost = new Score[mask+1]();
    suffixCost = new Score[mask+1]();
#ifdef RESTART
    timeStamp = new long long[mask+1]();
#endif
}

/*
 *  Destructor for Memo.
 */
Memo::~Memo()
{
    delete[] key;
    delete[] prefixCost;
    delete[] suffixCost;
#ifdef RESTART
    delete[] timeStamp;
#endif
}

/*
 *  Thomas Wang's 64 bit mix function:
 *  http://www.cris.com/~Ttwang/tech/inthash.htm
 */
unsigned int
Memo::hash( Set key )
{
#ifdef DYNSET
	std::vector<uint64_t> v;
	boost::to_block_range(key, std::back_inserter(v));
	return( static_cast<unsigned int>(boost::hash_value(v) & mask) );
#else
#ifdef BITSET
	std::hash<Set> hash_bitset;
	return( static_cast<unsigned int>(hash_bitset(key) & mask) );
#else
#ifdef SET32
	key = (key+0x7ed55d16) + (key<<12);
	key = (key^0xc761c23c) ^ (key>>19);
	key = (key+0x165667b1) + (key<<5);
	key = (key+0xd3a2646c) ^ (key<<9);
	key = (key+0xfd7046c5) + (key<<3);
	key = (key^0xb55a4f09) ^ (key>>16);
#else
	key += ~(key << 32);
    key ^= (key >> 22);
    key += ~(key << 13);
    key ^= (key >> 8);
    key += (key << 3);
    key ^= (key >> 15);
    key += ~(key << 27);
    key ^= (key >> 31);
#endif
    key &= mask;

    return( static_cast<unsigned int>(key) );
#endif
#endif
}

/*
 *  Memoize the ordering with prefix cost pCost and
 *  suffix cost sCost in the memoization database.
 *  Returns:
 *	NotFound  - insert = false, ordering not in db
 *	Inserted  - insert = true, ordering not in db, inserted into db
 *	Updated   - ordering in db, pCost < existing entry
 *	Dominated - ordering in db, explored, pCost >= existing entry
 *	Equal     - ordering in db, not yet explored, pCost == existing
 *	Failed    - too many probes
 */
Memo::MemoVal
Memo::memoize( int *ordering, int m, Score pCost, Score sCost, bool insert, long long nbnodes )
{
	Set p = emptySet;
	for( int i = 0; i < m; i++ ) {
		add_element( ordering[i], p );
	}

	/*
	 *  Hashing with quadratic probing.
	 */
	unsigned int h = hash( p );
	nLookups++;;
	int nProbes = 1;
	unsigned int index = h;
	if( (key[index] != p) && (key[index] != 0) ) {
		nCollisions++;
	}
	unsigned int step = 0;
	while( (key[index] != p) && (key[index] != 0) ) {
		step++;
		index = h + step*step;
		if( index > mask ) index = 0;
		nProbes++;
		if( nProbes > 100 ) {
			nFailed++;
			//TODO: replacement policies: keep most recent, most work
			key[index] = 0;
			break;
			//return( Failed );
		}
	}

	nTotalProbes += nProbes;

	if( key[index] == 0 ) {
		if( !insert ) {
			return( NotFound );
		}
		else {
			key[index] = p;
			prefixCost[index] = pCost;
			suffixCost[index] = sCost;
#ifdef RESTART
			timeStamp[index] = nbnodes;
#endif
			nInserted++;
			return( Inserted );
		}
	}
	else
		if( lt( pCost, prefixCost[index] ) ) {
			prefixCost[index] = pCost;
			suffixCost[index] = sCost;
#ifdef RESTART
			timeStamp[index] = nbnodes;
#endif
			nUpdated++;
			return( Updated );
		}
		else
			if( gt( pCost, prefixCost[index] ) ) {
				nDominated++;
				return( Dominated );
			}
			else {
				/*
				 *  suffixCost == 0 indicates that this prefix cost
				 *  is known to exist, but it hasn't yet been explored
				 *  in the search tree.
				 */
				if( suffixCost[index] == 0 ) {
					nEqual++;
					suffixCost[index] = sCost;
#ifdef RESTART
					timeStamp[index] = nbnodes;
#endif
					return( Equal );
				}
				else {
					nDominated++;
					return( Dominated );
				}
			}
}

#ifdef RESTART
long long Memo::erase(std::set<long long> &incompletenodes)
{
	long long erased = 0;
	for (unsigned int i = 0; i<mask+1; i++) {
		if (key[i] != 0 && incompletenodes.find(timeStamp[i]) != incompletenodes.end()) {
			key[i] = 0;
		    prefixCost[i] = 0;
			suffixCost[i] = 0;
			timeStamp[i] = 0;
			++erased;
		}
	}
	return erased;
}
#endif

void
Memo::printStats()
{
    printf( "Memo table size       = %12d\n", mask+1 );
    printf( "Memo table lookups    = %12d\n", nLookups );
    printf( "Memo table inserted   = %12d\n", nInserted );
    printf( "Memo table dominated  = %12d\n", nDominated );
    printf( "Memo table updated    = %12d\n", nUpdated );
    printf( "Memo table collisions = %12d\n", nCollisions );
    printf( "Memo table equal      = %12d\n", nEqual );
    printf( "Memo table failed     = %12d\n", nFailed );
    if( nLookups ) {
	printf( "Memo table average probes per lookup = %0.3f\n",
	    (double)nTotalProbes / (double)nLookups );
    }
}

