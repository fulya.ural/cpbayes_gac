/*
 *  Routines for performing backtracking search on an instance of
 *  the combinatorial optimization problem for learning a Bayesian
 *  network from data.
 */

#include "Instance.h"
#include "Bound.h"
#include "DAG.h"
#include "Memo.h"
#include "Timer.h"
#include "BacktrackingSearch.h"
#include "ZDD.h"

#include <cinttypes>

/*
 *  Constructor for backtracking search.
 */
BacktrackingSearch::BacktrackingSearch( Instance *instance, const Options& options )
    : opt(options)
    , instance(instance)
    , timer(std::make_unique<Timer>())
    , bound(std::make_unique<Bound>(instance, options))
    , memo(std::make_unique<Memo>())
    , dag(std::make_unique<DAG>(instance, bound->getLocalSearch(), timer.get(), options))
{
    int n = instance->nVars();

    currentParents = new int[n]();
    currentOrdering = new int[n]();
    currentDepth = new int[n]();
    instantiatedP = new bool[n]();
    bestParents = new int[n]();
    bestOrdering = new int[n]();
    valueOrdering = new int[n]();
    nbClusterFailuresPerLevel = new int[n]();
    nbOtherFailuresPerLevel = new int[n]();

    checkingO = new bool*[n+1]();
    for( int v = 0; v <= n; v++ ) {
        checkingO[v] = new bool[n]();
    }

    checkingD = new bool*[n+1]();
    for( int v = 0; v <= n; v++ ) {
        checkingD[v] = new bool[n]();
    }
}

void
BacktrackingSearch::initialize()
{
    currentCost = 0;
    lbCost = 0;
    ubCost = 0;
    restart = false;
    nodesVisited = 0;

    for( int v = 0; v < instance->nVars(); v++ ) {
        currentParents[v] = -1;
        currentOrdering[v] = -1;
        currentDepth[v] = -1;
        instantiatedP[v] = false;
        bestParents[v] = -1;
        bestOrdering[v] = -1;
    }

    for( int v = 0; v <= instance->nVars(); v++ ) {
        for( int w = 0; w < instance->nVars(); w++ ) {
            checkingO[v][w] = false;
        }
    }

    for( int v = 0; v <= instance->nVars(); v++ ) {
        for( int w = 0; w < instance->nVars(); w++ ) {
            checkingD[v][w] = false;
        }
    }
}

/*
 *  Destructor for backtracking search.
 */
BacktrackingSearch::~BacktrackingSearch()
{
    delete[] currentParents;
    delete[] currentOrdering;
    delete[] currentDepth;
    delete[] instantiatedP;
    delete[] bestParents;
    delete[] bestOrdering;
    delete[] valueOrdering;
    delete[] nbClusterFailuresPerLevel;
    delete[] nbOtherFailuresPerLevel;

    for( int v = 0; v <= instance->nVars(); v++ ) {
        delete[] checkingO[v];
    }
    delete[] checkingO;

    for( int v = 0; v <= instance->nVars(); v++ ) {
        delete[] checkingD[v];
    }
    delete[] checkingD;
}

/*
 *  Establish bounds on the cost of an optimal solution,
 *  prior to search.
 *
 *  The bounds are established in stages, where subsequent
 *  stages are only performed if the bounds found so far
 *  are considered too loose.
 */
void
BacktrackingSearch::establishBounds()
{
    Timer boundtimer;
    boundtimer.start();
    bound->partition( instantiatedP );
    lbCost = currentCost + bound->getLowerBound( instantiatedP );
    lowerBoundTime = boundtimer.elapsedTime();

    ubCost = bound->getUpperBound( bestOrdering, bestParents, 0 );
    upperBoundTime = boundtimer.elapsedTime() - lowerBoundTime;

    printf( "%s: upper bound solution, time = %0.3f\n",
        instance->benchmarkName(), boundtimer.elapsedTime() );
    if (opt.printsolutions)
        instance->printSolution( bestOrdering, bestParents, ubCost );
    double percentGap = (100.0 * (double)(ubCost - lbCost)) / ((double)lbCost);
    printf( "bounds: [%0.*f, %0.*f], gap = %0.*f (%0.3f)\n",
        precision, (double)lbCost / scale,
        precision, (double)ubCost / scale,
        precision, (double)(ubCost - lbCost) / scale,
        percentGap );
    fflush( stdout );

    if( percentGap >= 1.5 ) {
        double t = boundtimer.elapsedTime();
        Score cost = bound->getUpperBound( bestOrdering, bestParents, 1 );
        if( ubCost > cost ) {
            ubCost = cost;
            printf( "%s: upper bound solution, time = %0.3f\n",
                instance->benchmarkName(), boundtimer.elapsedTime() );
            if (opt.printsolutions)
                instance->printSolution( bestOrdering, bestParents, ubCost );
            percentGap = (100.0 * (double)(ubCost - lbCost)) / ((double)lbCost);
            printf( "bounds: [%0.*f, %0.*f], gap = %0.*f (%0.3f)\n",
                precision, (double)lbCost / scale,
                precision, (double)ubCost / scale,
                precision, (double)(ubCost - lbCost) / scale,
                percentGap );
            fflush( stdout );
        }
        upperBoundTime += boundtimer.elapsedTime() - t;
    }

    if( percentGap >= 1.5 ) {
        double t = boundtimer.elapsedTime();
        bound->repartition( instantiatedP );
        Score cost = currentCost + bound->getLowerBound( instantiatedP );
        if( lbCost < cost ) {
            lbCost = cost;
            percentGap = (100.0 * (double)(ubCost - lbCost)) / ((double)lbCost);
            printf( "bounds: [%0.*f, %0.*f], gap = %0.*f (%0.3f)\n",
                precision, (double)lbCost / scale,
                precision, (double)ubCost / scale,
                precision, (double)(ubCost - lbCost) / scale,
                percentGap );
            fflush( stdout );
        }
        lowerBoundTime += boundtimer.elapsedTime() - t;
    }
}

/*
 *  Pruning at root.
 *
 *  Cost-based pruning rule:
 *      Let p, p' in dom(x), p != p'.
 *      If p subseteq p' and cost(p) <= cost(p'), then prune p'.
 */
void
BacktrackingSearch::costPruning()
{
    /*
     *  Cost-based pruning rule at root of search tree.
     *  Let p, p' in dom(x), p != p'.
     *  If p subseteq p' and cost(p) <= cost(p'),
     *  then prune p'.
     */
    for( int i = 0; i < instance->nVars(); i++ ) {
        for( int a = 0; a < instance->getDomainSizeP( i ); a++ ) {
            if( instance->validP( i, a ) ) {
                Set pp = instance->getParentSet( i, a );
                /*
                 *  If there exists a parent set b of lower or equal
                 *  cost such that b is a subset of a, can prune a.
                 */
                for( int b = a-1; b >= 0; b-- ) {
                    if( instance->validP( i, b ) ) {
                        Set p = instance->getParentSet( i, b );
                        if( subset_of( p, pp ) ) {
                            //printf("(root) prune %d from domain of parent variable %d\n",a,i);
                            instance->getDomainP(i).remove( a, instance->nVars() );
                            //(instance->getZDDofVariable(i))->pruneParentSet(instance->getParentSet( i, a ));
                            break;
                        }
                    }
                }
            }
        }
    }
}

/*
 *  Pruning at root.
 *
 *  Symmetry breaking based on symmetric variables.
 */
void
BacktrackingSearch::symmetryVariables()
{
    /*
     *  Symmetry-breaking satisfiability test based on symmetric
     *  parent variables [x/y]dom(x) = dom(y); i.e., the domains
     *  are the same once the substitution "replace all occurrences
     *  of y by x" has been applied to the domain of x. Break
     *  symmetry by enforcing x < y; i.e., lexicographic ordering.
     */
    int xSym[instance->nVars()];
    int ySym[instance->nVars()];
    int m = 0;
    for( int y = 0; y < instance->nVars(); y++ ) if( !instantiatedP[y] )
    for( int x = 0; x < y; x++ ) if( !instantiatedP[x] ) {
        if( instance->getCurrentDomainSizeP( x ) ==
            instance->getCurrentDomainSizeP( y ) ) {
            bool equalDomains = true;
            for (int a : instance->getDomainP(x)) {
                if (!instance->validP(y, a)) {
                    equalDomains = false;
                    break;
                }
                /*
                 *  Apply substitution [x/y]p for p in dom(x).
                 */
                Set p = instance->getParentSet( x, a );
                if( element_of( y, p ) ) {
                    remove_element( y, p );
                    add_element( x, p );
                }
                Score cost = instance->getCost( x, a );
                if( (p != instance->getParentSet( y, a )) ||
                    (cost != instance->getCost( y, a )) ) {
                    equalDomains = false;
                    break;
                }
            }
            if( equalDomains ) {
                xSym[m] = x;
                ySym[m] = y;
                m++;
            }
        }
    }
    for( int i = 0; i < m; i++ ) {
        int x = xSym[i];
        int y = ySym[i];
        //printf("(root) breaking symmetry %d --> %d\n",x,y);
        Set necessary = emptySet;
        add_element( y, necessary );
        for (int a : instance->getDomainP(x)) {
                /*
                 *  Remove the parent set a from domain of x if the
                 *  intersection of necessary and a is not empty.
                 */
                Set p = instance->getParentSet( x, a );
                if( !set_empty( intersection( necessary, p ) ) ) {
                    //printf("(root) prune %d from domain of parent variable %d\n",a,x);
                    instance->getDomainP(x).remove( a, instance->nVars() );
                    //(instance->getZDDofVariable(i))->pruneParentSet(instance->getParentSet( x, a ));
                }
        }
        instance->symmetricParentVariables( x, y );
    }
}

/*
 *  Solve the instance using backtracking search interleaved
 *  with constraint propagation.
 */
void
BacktrackingSearch::backtrack( int currentLevel )
{
        if( timer->elapsedTime() > opt.timeLimit ) {
            printStats();
            exit(0);
        }

        int v = getNextVariable( currentLevel );

        if( v == -1 ) {
                /*
                 *  A new solution has been found. If the new solution has a
                 *  lower cost, update best solution, best ordering, and ubCost.
                 */
                if( gt( ubCost, currentCost ) ) {
                        ubCost = currentCost;
                        for( int i = 0; i < instance->nVars(); i++ ) {
                                bestParents[i] = currentParents[i];
                                bestOrdering[i] = currentOrdering[i];
                        }
                        printf(FGREEN "%s: backtracking solution %g, time = %0.3f, "
                               "nodes = %lld\n" FNONE,
                               instance->benchmarkName(),
                               (double)ubCost / scale,
                               timer->elapsedTime(), nbnodes );
                        if (opt.printsolutions)
                            instance->printSolution( bestOrdering, bestParents, ubCost );
#ifdef RESTART
                        restart = true;
#endif
                }
                return;
        }

        if (nbnodes == opt.bestOrdering_switchover
            && (opt.dtree_heuristic == dtree_construction::BO
                || opt.dtree_heuristic == dtree_construction::BO_VEB)) {
            printf("Rebuilding decision trees...");
            fflush(stdout);
            instance->reconstructDtrees(
                { bestOrdering, static_cast<size_t>(instance->nVars()) });
            printf("Done\n");
        }

        ++nbnodes;

        long long currentNbNodes = nbnodes;
        Memo::MemoVal ret = memo->memoize( currentOrdering, currentLevel,
                        currentCost, lbCost-currentCost, false, currentNbNodes );
        if( ret == Memo::Dominated ) {
                /*
                 *  This prefix of variables has been seen before,
                 *  and the current ordering is dominated by the
                 *  previously seen ordering.
                 */
#ifdef EXTRADEBUG
                assert(dag->debugBacktrack(currentLevel-1, currentOrdering));
#endif
                return;
        }
        long long saveNodes = nodesVisited;

        for (int k = 0; k < instance->getDomainSizeO(v); k++) {
            if (restart)
                break;
            int a = valueOrdering[k];
            if (instance->validO(v, a)) {

                int p = instance->getMinCostParentSet(
                    currentOrdering, currentLevel, a);
                if (p == -1) {
#ifdef EXTRADEBUG
                    assert(v == currentLevel);
                    currentOrdering[v] = a;
                    assert(dag->debugBacktrack(currentLevel, currentOrdering));
                    currentOrdering[v] = -1;
#endif
                    continue;
                }
                int d = instance->getDepthParentSet(
                    currentOrdering, currentDepth, currentLevel, a, p);
#if VERBOSE >= 1
                printf(FGREEN "[%d,%d] Try x%d = %d ", currentLevel, v, a, p);
                instance->printParentSet(a, p);
                printf(FNONE "\n");
#endif
                currentParents[a] = p;
                currentOrdering[v] = a;
                currentDepth[v] = d;
                instantiatedP[a] = true;
                currentCost += instance->getCost(a, p);
                nodesVisited++;

                if (symmetryEquivalent(v, d, currentLevel)) {

                    Score previouslbCost = lbCost;
                    // static lower bound built in preprocessing
                    Score lbCost1
                        = currentCost + bound->getLowerBound(instantiatedP);
                    // dynamic lower bound taking into account
                    // pruning
                    Score lbCost2 = currentCost
                        + bound->getGreedyLowerBound(instantiatedP);
                    // take the best of the two bounds
                    lbCost = std::max(lbCost1, lbCost2);

                    if (gt(ubCost, lbCost)) {
                        if (propagate(v, a, p, d, currentLevel)) {
                            backtrack(currentLevel + 1);
                        } else {
#ifdef EXTRADEBUG
                            assert(dag->debugBacktrack(
                                currentLevel, currentOrdering));
#endif
                            if (dag->clusterFailed)
                                nbClusterFailuresPerLevel[currentLevel]++;
                            else
                                nbOtherFailuresPerLevel[currentLevel]++;
                        }
                    } else {
#ifdef EXTRADEBUG
                        assert(
                            dag->debugBacktrack(currentLevel, currentOrdering));
#endif
                    }

                    lbCost = previouslbCost;
                } else {
#ifdef EXTRADEBUG
                    assert(dag->debugBacktrack(currentLevel, currentOrdering));
#endif
                }

                restoreDomains(v, currentLevel);
                currentParents[a] = -1;
                currentOrdering[v] = -1;
                currentDepth[v] = -1;
                instantiatedP[a] = false;
                currentCost -= instance->getCost(a, p);
            } else {
#ifdef EXTRADEBUG
                assert(v == currentLevel);
                currentOrdering[v] = a;
                assert(dag->debugBacktrack(currentLevel, currentOrdering));
                currentOrdering[v] = -1;
#endif
            }
        }

        /*
         *  If this prefix of variables has not been seen before,
         *  insert if it is not cheap to recompute.
         */
        if (opt.restarts && restart) {
            incompletenodes.insert(currentNbNodes);
        } else {
            if ((ret == Memo::NotFound) && ((nodesVisited - saveNodes) > 10)) {
                memo->memoize(currentOrdering, currentLevel, currentCost,
                    lbCost - currentCost, true,
                    currentNbNodes); // bug correction: why lbCost was not
                                     // restore in original version?
                                     // suffixCost!=0 has not impact!
            }
        }
}

/*
 *  Symmetry breaking based on I-equivalence.
 *
 *  Symmetry breaking must be treated differently from
 *  constraint propagation for the purposes of memoization.
 *  Avoid the situation where a node is inserted into the
 *  table with a given cost and then is immediately rejected
 *  by symmetry breaking; subsequently a node of equivalent
 *  cost is looked-up in the table and is rejected, with
 *  the unintended consequence that both are rejected.
 *
 *  Only break symmetries if the costs of the two symmetric
 *  subnetworks would be equal, otherwise there is a negative
 *  interaction with cost-based pruning.
 *
 *  Symmetry breaking is a satisfiability test. It could also
 *  be propagated, but this does not seem to improve performance.
 */
bool
BacktrackingSearch::symmetryEquivalent( int v, int d, int currentLevel )
{
    /*
     *  Symmetry-breaking satisfiability test based on I-equivalence.
     *  Let y be the recently instantiated parent variable. Look
     *  through the parents of y for an x such that,
     *      x = P               depth i
     *      y = P union {x}     depth i+1
     *  Such an edge x --> y is called a covered edge and can be
     *  reversed to get an I-equivalent network. Break symmetry by
     *  enforcing x < y; i.e., lexicographic ordering.
     */
    if( d >= 1 ) {
        int y = currentOrdering[currentLevel];
        for( int j = 0; j < currentLevel; j++ ) {
            int x = currentOrdering[j];
            if( x < y ) continue;
            if( (currentDepth[j]+1 == d) &&
                instance->coveredEdge(
                        x, currentParents[x],
                        y, currentParents[y] ) ) {
                Score symCost = swapCost( j, currentLevel );
                if( (currentCost == symCost) && (x > y) ) {
                    //printf("(b) breaking symmetry %d --> %d\n",x,y);
                    return( false );
                }
            }
        }
    }

    /*
     *  Symmetry-breaking satisfiability test based on I-equivalence.
     *  Look for the case:
     *      x = P               depth i
     *      y = P union {x}     depth i+1
     *      z = P union {y}     depth i+2
     *  where x < y, z < y, and x > z. The above symmetry test
     *  allows both {x --> y, y --> z} and {z --> y, y --> x}.
     *  Want to keep only one of these. Use lexicographic ordering
     *  to break symmetry; i.e., prune if x > z.
     */
    if( d >= 2 ) {
        int z = currentOrdering[currentLevel];
        for( int j = 0; j < currentLevel; j++ ) {
            int x = currentOrdering[j];
            if( x < z ) continue;
            for( int k = j+1; k < currentLevel; k++ ) {
                int y = currentOrdering[k];
                if( (x < y) && (z < y) &&
                    (currentDepth[j]+2 == d) &&
                    (currentDepth[k]+1 == d) &&
                    instance->coveredEdge(
                                x, currentParents[x],
                                y, currentParents[y] ) &&
                    instance->flippedCoveredEdge(
                                x, currentParents[x],
                                y, currentParents[y],
                                z, currentParents[z] ) ) {

                    Score symCost = swapCost( j, currentLevel );
                    if( (currentCost == symCost) && (x > z) ) {
                        //printf("(c) breaking symmetry %d %d %d\n",x,y,z);
                        return( false );
                    }
                }
            }
        }
    }

    /*
     *  Symmetry-breaking satisfiability test based on I-equivalence.
     *  Look for the case:
     *      w = P               depth i
     *      x = P union {w}     depth i+1
     *      y = P union {w,x}   depth i+2
     *      z = P union {x,y}   depth i+3
     *  where w < x, z < x, x < y, and w > z.
     *  The above symmetry tests allow both
     *  { w --> x, w --> y, x --> y, x --> z, y --> z} and
     *  { z --> x, z --> y, x --> y, x --> w, y --> w}.
     *  Want to keep only one of these. Use lexicographic ordering
     *  to break symmetry; i.e., prune if w > z.
     */
    if( d >= 3 ) {
        int z = currentOrdering[currentLevel];
        for( int j = 0; j < currentLevel; j++ ) {
            int w = currentOrdering[j];
            if( w < z ) continue;
            for( int k = j+1; k < currentLevel; k++ ) {
                int x = currentOrdering[k];
                if( (x < z) || (x < w) ) continue;
                for( int l = k+1; l < currentLevel; l++ ) {
                    int y = currentOrdering[l];
                    if( (w < x) && (z < x) && (x < y) &&
                        (currentDepth[j]+3 == d) &&
                        (currentDepth[k]+2 == d) &&
                        (currentDepth[l]+1 == d) &&
                        instance->coveredEdge(
                                w, currentParents[w],
                                x, currentParents[x] ) &&
                        instance->coveredEdge(
                                x, currentParents[x],
                                y, currentParents[y] ) &&
                        instance->triangleCoveredEdge(
                                w, currentParents[w],
                                x, currentParents[x],
                                y, currentParents[y],
                                z, currentParents[z] ) ) {

                        Score symCost = swapCost( j, currentLevel );
                        if( (currentCost == symCost) && (w > z) ) {
                            //printf("(d) breaking symmetry %d %d %d %d\n",w,x,y,z);
                            return( false );
                        }
                    }
                }
            }
        }
    }

    return( true );
}

/*
 *  Determine the cost of a subnetwork that has
 *  x and y, x < y, swapped in the ordering.
 */
Score
BacktrackingSearch::swapCost( int x, int y )
{
    Score cost;

    /*
     *  Determine the ancestors and the cost of the ancestors.
     *  These will remain fixed and want to avoid recomputing.
     */
    Score prefixCost = 0;
    Set ancestors = emptySet;
    for( int i = 0; i < x; i++ ) {
        int v = currentOrdering[i];
        int a = currentParents[v];
        prefixCost += instance->getCost( v, a );
        add_element( v, ancestors );
    }

    /*
     *  Determine the subset of the ordering that is going
     *  to be rearranged.
     */
    int k = 0;
    int ordering[instance->nVars()];
    for( int i = x; i <= y; i++ ) {
        ordering[k] = currentOrdering[i];
        k++;
    }

    /*
     *  Swap to get the new network and determine the new cost.
     */
    int u = ordering[k-1];
    ordering[k-1] = ordering[0];
    ordering[0] = u;

    cost = prefixCost + instance->minCostOrdering( ancestors, ordering, k );

    return( cost );
}

/*
 *  Generate the permutations of the ordering[i..n-1] to
 *  find the ordering that gives the lowest cost.
 */
void
BacktrackingSearch::perm( Set ancestors, int *ordering, int i, int n, Score *cost )
{
    if( i == n ) {
        Score c = instance->minCostOrdering( ancestors, ordering, n, NULL );
        if( gt( *cost, c ) ) {
            *cost = c;
        }
    }
    else {
        for( int j = i; j < n; j++ ) {
            int t = ordering[i];
            ordering[i] = ordering[j];
            ordering[j] = t;

            perm( ancestors, ordering, i+1, n, cost );

            t = ordering[i];
            ordering[i] = ordering[j];
            ordering[j] = t;
        }
    }
}

/*
 *  Constraint propagation.
 *
 *  Reduce the domains of the variables given the new
 *  instantiation a = p, where the assignment a = p denotes
 *  that parent variable x_a has parents p in the DAG->
 *
 *  Reduce the domains of the variables given the new
 *  instantiation v = a, where the assignment v = a denotes
 *  that parent variable x_a is in the v'th position in
 *  the total ordering of the parent variables.
 *
 *  Reduce the domains of the variables given the new
 *  instantiation v = d, where the assignment v = d denotes
 *  that parent variable x_a that occurs at the v'th
 *  position in the total ordering has depth d.
 *
 *  Assumes a static order of instantiation on the
 *  ordering and depth variables.
 *
 *  Propagating the parent variables could be iterated
 *  but this does not seem to improve performance.
 */
bool
BacktrackingSearch::propagate( int v, int a, int p, int d, int currentLevel )
{
    bool success;

    success = propagateOrdering( v, a, d, currentLevel );
    if( !success ) return( false );

    success = propagateParents( v, d, currentLevel );
    if( !success ) return( false );

    success = propagateDepth( v, d, currentLevel );
    if( !success ) return( false );

//TODO: more pruning between parent, ordering, and depth variables
//TODO: decomposition during search into SCCs
//TODO: component/lower bound caching
//TODO: propagation of permutation constraint
//TODO: more symmetry breaking: look for equal cost

    return( true );
}

/*
 *  Constraints over parent variables.
 */
bool
BacktrackingSearch::propagateParents( int v, int d, int currentLevel )
{
        /*
         *  Pruning based on most recent depth d.
         *  Any parent set with depth l less than d can be pruned.
         *  Any parent set with depth l equal to d and the parent variable is
         *  lexicographically less than the most recently instantiated
         *  variable can be pruned.
//TODO: this one is kind of odd, but not incorrect. The pruning should
//TODO: happen on the ordering variables or in addition should happen
//TODO: on the ordering variables.
         */
        for( int i = 0; i < instance->nVars(); i++ ) if( !instantiatedP[i] ) {
                for (int a : instance->getDomainP(i)) {
                                int l = ((v<instance->nVars())?instance->getDepthParentSet(
                                                currentOrdering, currentDepth, currentLevel+1, i, a ):0);
                                if(  (l  < d) ||
                                                ((l == d) && (currentOrdering[v] > i)) ) {
                                        if(false && set_empty(instance->getParentSet( i, a )))
                                                printf("(e) prune %d from domain of parent variable %d\n",a,i);
                                        instance->setValidP( i, a, currentLevel );
                                        if( instance->getCurrentDomainSizeP( i ) == 0 ) {
                                                return( false );
                                        }
                                }
                }
        }

        /*
         *  Cost constraint over parent variables.
         *    lbCost = lower bound on cost of current solution being constructed
         *    ubCost = cost of current best solution found so far
         */
        for( int i = 0; i < instance->nVars(); i++ ) if( !instantiatedP[i] ) {
                /*
                 *  Find a lower bound on the cost without parent variable i.
                 *  This can be done by temporarily instantiating variable i,
                 *  but not adding in any cost for variable i; i.e., cost of 0.
                 */
                instantiatedP[i] = true;
                Score lbCurrentCost = currentCost +
                                bound->getLowerBound( instantiatedP );
                instantiatedP[i] = false;
                for (int a : instance->getDomainP(i)) {
                                Score costEst = instance->getCost( i, a ) + lbCurrentCost;
                                if( ge( costEst, ubCost ) ) {
                                        if(false && set_empty(instance->getParentSet( i, a )))
                                                printf("(f) prune %d from domain of parent variable %d\n",a,i);
                                        instance->setValidP( i, a, currentLevel );
                                        if( instance->getCurrentDomainSizeP( i ) == 0 ) {
                                                return( false );
                                        }
                                }
                }
        }
        Score ubCurrentCost = currentCost +
                        bound->getGreedyUpperBound( instantiatedP );
        for( int i = 0; i < instance->nVars(); i++ ) if( !instantiatedP[i] ) {
                /*
                 *  Find the last valid domain element.
                 *  As the domains are sorted, this will also
                 *  be the highest cost domain element.
                 */
                Score maxCost = 0;
                for (int a : instance->getDomainP(i)) {
                                maxCost = instance->getCost( i, a );
                                break;
                }
                for (int a : instance->getDomainP(i)) {
                                Score costEst =
                                                instance->getCost( i, a ) + ubCurrentCost - maxCost;
                                if( lt( costEst, lbCost ) ) {
                                        if(false && set_empty(instance->getParentSet( i, a )))
                                                printf("(g) prune %d from domain of parent variable %d\n",a,i);
                                        instance->setValidP( i, a, currentLevel );
                                        if( instance->getCurrentDomainSizeP( i ) == 0 ) {
                                                return( false );
                                        }
                                }
                }
        }

        /*
         *  Cost-based pruning rule based on ordering so far.
         *  Let p, p' in dom(x), p != p'.
         *  If p subseteq ordering and cost(p) <= cost(p'),
         *  then prune p'.
         */
        for( int i = 0; i < instance->nVars(); i++ ) if( !instantiatedP[i] ) {
                /*
                 *  Parent set p is the lowest cost parent set for variable i
                 *  that has all of its elements in the ordering.
                 */
                int a = instance->getMinCostParentSet(
                                currentOrdering, currentLevel+((v < instance->nVars())?1:0), i );
                if( a == -1 ) continue;
                /*
                 *  Remove any p' of greater than or equal cost.
                 */
                for (int b : instance->getDomainP(i)) {
                                if (b <= a) continue;
                                instance->setValidP( i, b, currentLevel );
                                if( instance->getCurrentDomainSizeP( i ) == 0 ) {
                                        return( false );
                                }
                }
        }

        /*
         *  Generalized cost-based pruning rule.
         *  Let p, p' in dom(x), p != p'.
         *  If p subseteq ip(p') and cost(p) <= cost(p'),
         *  then prune p'.
         */

        if (v==instance->nVars() && currentLevel>0) { // only done in preprocessing and if some variables have been preassigned
                for( int i = 0; i < instance->nVars(); i++ ) if( !instantiatedP[i] ) {
                        const auto& dom = instance->getDomainP(i).asvec();
                        for(size_t idx = 0, eidx = dom.size(); idx != eidx; ++idx) {
                                int a = dom[idx];
                                assert(instance->validP(i, a));
                                /*
                                 *  If there exists a parent set b of lower cost such that b
                                 *  is contained in the induced parent set of a, can prune a.
                                 */
                                if( instance->containedInIP_condensed(
                                                currentOrdering, currentLevel+((v < instance->nVars())?1:0), i, dom, idx ) ) {
                                        // if( instance->containedInIP(
                                        //          currentOrdering, currentLevel+((v < instance->nVars())?1:0), i, a) ) {
                                        //printf("(i) prune %d from domain of P%d\n",a,i);
                                        instance->setValidP( i, a, currentLevel );
                                        if( instance->getCurrentDomainSizeP( i ) == 0 ) {
                                                return( false );
                                        }
                                }
                        }
                }
        }

        Score currentLB = currentCost + bound->getGreedyLowerBound(instantiatedP);
        bool success = dag->propagate( currentParents, currentOrdering,
                        currentDepth, currentLevel,
                        instantiatedP, v, currentLB, &ubCost, &bestParents, &bestOrdering, &restart );
        if( !success ) return( false );

        //instance->dumpStateFull( currentLevel, currentParents, currentOrdering, currentDepth, instantiatedP );
        return( true );
}

/*
 *  Constraints over ordering variables.
 */
bool
BacktrackingSearch::propagateOrdering( int v, int a, int d, int currentLevel )
{
    /*
     *  Permutation of prefix constraint.
     *  Satisfiability test (part I).
     */
    Score cost = MAX_SCORE;
    int start = currentLevel+1 - opt.bt_width_prefix_permute;
    if( start < 0 ) start = 0;

    /*
     *  Determine the ancestors and the cost of the ancestors.
     *  These will remain fixed and want to avoid recomputing.
     */
    Score prefixCost = 0;
    Set ancestors = emptySet;
    for( int i = 0; i < start; i++ ) {
        int w = currentOrdering[i];
        int b = currentParents[w];
        prefixCost += instance->getCost( w, b );
        add_element( w, ancestors );
    }

    /*
     *  Determine the subset of the ordering that is going
     *  to be rearranged.
     */
    int k = 0;
    int ordering[instance->nVars()];
    for( int i = start; i < currentLevel+1; i++ ) {
        ordering[k] = currentOrdering[i];
        k++;
    }
    /*
     *  Step through the rearrangements and determine the best cost.
     */
    perm( ancestors, ordering, 0, k, &cost );
    cost += prefixCost;

    if( lt( cost, currentCost ) ) {
        memo->memoize( currentOrdering, currentLevel+1, cost, 0, true, nbnodes );
        return( false );
    }

    /*
     *  Permutation of prefix constraint.
     *  Satisfiability test (part II).
     *  Also looked at replacing these permutation tests
     *  with a hill-climbing local search, but even when
     *  the cost updates were done incrementally, still
     *  couldn't get it to be better.
     */
    for( int j = 0; j < currentLevel; j++ ) {
        Score cost = swapCost( j, currentLevel );
        if( lt( cost, currentCost ) ) {
            memo->memoize( currentOrdering, currentLevel+1, cost, 0, true, nbnodes );
            return( false );
        }
    }

    /*
     *  Lexicographic constraint. If there exists a parent
     *  variable i with a singleton domain that has the same depth
     *  as the current working depth, then prune any variable j
     *  from the next level that is lexicographically greater.
     *  The reasoning is that if j were to be instantiated at
     *  the next level, i would have to come after j, violating
     *  the lexicographic constraint.
//TODO: more pruning could be done here using d_i = d_{i+1} ==> o_i < o_{i+1}?
     */
    for( int i = 0; i < instance->nVars(); i++ ) if( !instantiatedP[i] ) {
        if( instance->getCurrentDomainSizeP( i ) == 1 ) {
            auto& idom = instance->getDomainP(i).asvec();
            int a = idom[0];
            int di = instance->getDepthParentSet(
                        currentOrdering, currentDepth, currentLevel+1, i, a );
            if( di == d ) {
                //printf("v_%d\n",i);
                int l = currentLevel+1;
                for( int k = i+1; k < instance->getDomainSizeO( l ); k++ ) {
                    if( instance->validO( l, k ) ) {
                        //printf("(lex) prune %d from domain of parent variable %d\n",k,l);
                        instance->setValidO( l, k, currentLevel );
                        checkingO[v][l] = true;
                        if( instance->getCurrentDomainSizeO( l ) == 0 ) {
                            printf("(lexicographic) false\n");
                            return( false );
                        }
                    }
                }

            }
        }
    }


    /*
     *  Not equals constraint. Remove domain value a from the
     *  domains of the uninstantiated ordering variables.
     */
    for( int i = currentLevel+1; i < instance->nVars(); i++ ) {
        if( instance->validO( i, a ) ) {
            instance->setValidO( i, a, currentLevel );
            checkingO[v][i] = true;
            if( instance->getCurrentDomainSizeO( i ) == 0 ) {
                printf("(not equal) false\n");
                return( false );
            }
        }
    }

    return( true );
}

/*
 *  Constraints over depth variables.
 */
bool
BacktrackingSearch::propagateDepth( int v, int d, int currentLevel )
{
    /*
     *  Depth (difference of 1) constraint.
     */
    for( int i = currentLevel+1; i < instance->nVars(); i++ ) {
        for( int k = 0; k < instance->getDomainSizeD( i ); k++ ) {
            if( instance->validD( i, k ) ) {
                bool l, r;
                if( i == currentLevel+1 ) {
                    l = (k != d) && (k-1 != d);
                }
                else {
                    l = (!instance->validD( i-1, k ) &&
                         (k==0 || !instance->validD( i-1, k-1 )));
                }
                r = (i != instance->nVars()-1) &&
                     !instance->validD( i+1, k ) &&
                     !instance->validD( i+1, k+1 );
                if( l || r ) {
                    instance->setValidD( i, k, currentLevel );
                    checkingD[v][i] = true;
                    if( instance->getCurrentDomainSizeD( i ) == 0 ) {
                        printf("(depth) false\n");
                        return( false );
                    }
                }
            }
        }
    }

    return( true );
}

/*
 *  Choose the next variable to be branched on using static variable ordering.
 *  Returns -1 if all variables are instantiated.
 */
int
BacktrackingSearch::getNextVariable( int currentLevel )
{
    if( currentLevel < instance->nVars() ) {
        return( currentLevel );
    }
    else {
        return( -1 );
    }
}

/*
 *  Restore domains to the state before constraint propagation.
 */
void
BacktrackingSearch::restoreDomains( int v, int currentLevel )
{
    /*
     *  Restore the domains of the parent variables.
     */
    instance->getDomainPs().restore(currentLevel);

    /*
     *  Restore the domains of the uninstantiated ordering variables.
     */
    for( int j = currentLevel+1; j < instance->nVars(); j++ ) {
        if( checkingO[v][j] ) {
            checkingO[v][j] = false;
            for( int a = 0; a < instance->getDomainSizeO( j ); a++ ) {
                if( instance->getValidO( j, a ) == currentLevel ) {
                     instance->setValidO( j, a, -1 );
                }
            }
        }
    }

    /*
     *  Restore the domains of the uninstantiated depth variables.
     */
    for( int j = currentLevel+1; j < instance->nVars(); j++ ) {
        if( checkingD[v][j] ) {
            checkingD[v][j] = false;
            for( int a = 0; a < instance->getDomainSizeD( j ); a++ ) {
                if( instance->getValidD( j, a ) == currentLevel ) {
                    instance->setValidD( j, a, -1 );
                }
            }
        }
    }
}

/*
 *  Find the minimum cost solution. Returns the assignments
 *  to the variables that gives the lowest cost and the
 *  cost of that solution.
 */
void
BacktrackingSearch::solve( int *ordering, int *solution, Score *cost )
{
    timer->start();

    initialize();
    costPruning();

    for (int v = 0; v != instance->nVars(); ++v)
        dtTime += instance->getDomainP(v).getStats().dt_construction_time;

    /*
     *  Instantiate parent variables that have a singleton domain.
     *  The domain value will always be the empty set.
     */
    int level = 0;
    for( int v = 0; v < instance->nVars(); v++ ) {
        if( instance->getCurrentDomainSizeP( v ) == 1 ) {
            auto& vdom = instance->getDomainP(v).asvec();
            int a = vdom[0];
            printf("Instantiated: v_%d = %d\n",v,a);
            fflush( stdout );
            currentParents[v] = a;
            currentOrdering[level] = v;
            currentDepth[level] = 0;
            instantiatedP[v] = true;
            currentCost += instance->getCost( v, a );
            for( int i = level+1; i < instance->nVars(); i++ ) {
                instance->setValidO( i, v, level );
            }
            level++;
        }
    }

    establishBounds();

    symmetryVariables();

    nbrestarts = 0;
    Score lbCostInit = lbCost;
    do {
        ++nbrestarts;
        ++nbnodes;
#ifdef RESTART
        incompletenodes.clear();
        incompletenodes.insert(nbnodes); // current root node is potentially incomplete if restart
#endif
        restart = false;
        lbCost = lbCostInit;

        /*
         *  Use bestOrdering as a value ordering.
         */
#ifndef NDEBUG
        for( int v = 0; v < instance->nVars(); v++ ) {
                bool found = false;
                for( int i = 0; i < instance->nVars(); i++ ) {
                        if (bestOrdering[i] == v) {
                                found = true;
                                break;
                        }
                }
                assert(found);
        }
#endif
        for( int v = 0; v < instance->nVars(); v++ ) {
                valueOrdering[v] = bestOrdering[v];
        }

        if( gt( ubCost, lbCost ) ) {
                if( propagateParents( instance->nVars(), -1, level ) ) { //TODO:  if( propagateParents( instance->nVars(), -1, level && propagateParents( instance->nVars(), -1, level )) { // first propagate find cuts, second propagate exploits ordered cuts
                    if (nbrestarts == 1) dag->printStats();
                    backtrack( level );
                } else {
#ifdef EXTRADEBUG
                assert(dag->debugBacktrack(level-1, currentOrdering));
#endif
                }
        } else {
#ifdef EXTRADEBUG
                assert(dag->debugBacktrack(level-1, currentOrdering));
#endif
        }
#ifdef RESTART
        if (restart) {
                long long nberased = memo->erase(incompletenodes);
                printf("\nClear %lld prefixes due to restart #%lld (%ld incomplete nodes).\n\n", nberased, nbrestarts, incompletenodes.size());
        }
#endif
    } while (restart);

    /*
     *  Copy over the best solution and cost of the solution.
     */
    for( int v = 0; v < instance->nVars(); v++ ) {
        ordering[v] = bestOrdering[v];
        solution[v] = bestParents[v];
    }
    *cost = ubCost;
    optimum = true;
    printStats();
}

void BacktrackingSearch::printStats()
{
    double searchTime = timer->elapsedTime() -
                            (lowerBoundTime + upperBoundTime);

    memo->printStats();
    printf( "%s: lower bound time = %0.1f\n",
                instance->benchmarkName(), lowerBoundTime );
    printf( "%s: upper bound time = %0.1f\n",
                instance->benchmarkName(), upperBoundTime );
    printf( "%s: bound time = %0.1f\n",
                instance->benchmarkName(), lowerBoundTime + upperBoundTime );

    int64_t reprsize{0};
    int64_t numpsets{0};
    for(int v = 0; v != instance->nVars(); ++v) {
        auto& vdom = instance->getDomainP(v);
        for (int a = 0; a != vdom.totalSSize(); ++a)
            reprsize += count(vdom.getParentSet(a));
        numpsets += vdom.totalSSize();
    }
    printf( "%s: Avg domain size = %.1f (%" PRId64 " parent sets over %d variables)\n",
            instance->benchmarkName(),
            numpsets/(double)instance->nVars(),
            numpsets, instance->nVars());
    printf( "%s: Avg representation size = %.1f (%" PRId64 " edges over %d variables)\n",
            instance->benchmarkName(),
            reprsize/(double)instance->nVars(),
            reprsize, instance->nVars());
    if (opt.dtrees != dtree_use::NONE) {
        printf( "%s: DT construction time = %.1f\n", instance->benchmarkName(), dtTime);
        int64_t dtnodes{0};
        double ratio{0.0}, reprratio{0.0};
        int64_t subsetcalls{0}, node_visits{0};
        int64_t mincsubsetcalls{0}, mincnode_visits{0};
        for(int v = 0; v != instance->nVars(); ++v) {
            auto& vdom = instance->getDomainP(v);
            auto stats = vdom.getStats();
            dtnodes += stats.dt_nodes;
            ratio += stats.dt_nodes / (double)(vdom.totalSize());
            int thisreprsize{0};
            for (int a = 0; a != vdom.totalSSize(); ++a)
                thisreprsize += count(vdom.getParentSet(a));
            reprratio += stats.dt_nodes / (double)(thisreprsize);
            subsetcalls += stats.subsetcalls;
            node_visits += stats.dt_nodes_visited_subset;
            mincsubsetcalls += stats.mincsubsetcalls;
            mincnode_visits += stats.dt_nodes_visited_mincsubset;
        }
        printf( "%s: DT avg nodes = %.0f (%.2f %% of domain size, %.2f %% of representation)\n",
                instance->benchmarkName(),
                dtnodes/(double)(instance->nVars()),
                (ratio*100.0)/(double)(instance->nVars()),
                (reprratio*100.0)/(double)(instance->nVars()));
        printf( "%s: DT nodes visited = %" PRId64 " (%.1f over %" PRId64 " calls)\n",
                instance->benchmarkName(),
                node_visits,
                node_visits/(double)subsetcalls,
                subsetcalls);
        if (mincnode_visits)
            printf( "%s: DT mincost nodes visited = %" PRId64 " (%.1f over %" PRId64 " calls)\n",
                    instance->benchmarkName(),
                    mincnode_visits,
                    mincnode_visits/(double)mincsubsetcalls,
                    mincsubsetcalls);
    }
    int64_t subsetcalls{0}, subsetchecks{0};
    int64_t mincsubsetcalls{0}, mincsubsetchecks{0};
    for(int v = 0; v != instance->nVars(); ++v) {
        auto& vdom = instance->getDomainP(v);
        auto stats = vdom.getStats();
        subsetcalls += stats.subsetcalls;
        subsetchecks += stats.subsetchecks;
        mincsubsetcalls += stats.mincsubsetcalls;
        mincsubsetchecks += stats.mincsubsetchecks;
    }
    if (subsetchecks)
        printf( "%s: avg #subset checks = %.1f over %" PRId64 " calls\n",
                instance->benchmarkName(),
                subsetchecks/(double)subsetcalls, subsetcalls);
    if (mincsubsetchecks)
        printf( "%s: avg mincost #subset checks = %.1f over %" PRId64 " calls\n",
                instance->benchmarkName(),
                mincsubsetchecks/(double)mincsubsetcalls, mincsubsetcalls);
    printf( "%s: search time = %0.1f\n",
                instance->benchmarkName(), searchTime );
    printf( "%s: total time = %0.1f\n",
                instance->benchmarkName(), timer->elapsedTime() );
    if (optimum)
        printf( "%s: OPTIMUM, nodes = %lld, cost = %0.*f\n",
                instance->benchmarkName(), nodesVisited,
                precision, (double)(ubCost) / scale );
    else
        printf( "%s: TIMEOUT, nodes = %lld, cost = %0.*f\n",
                instance->benchmarkName(), nodesVisited,
                precision, (double)(ubCost) / scale );
    if (nbrestarts > 1) {
        printf( "%s: number of restarts = %lld\n\n", instance->benchmarkName(), nbrestarts);
    }

    this->bound->printNonZeroQueries();

    printf("\nNumber of times cluster reasoning failed at each level:\n");
    for(int i=0;i<instance->nVars(); i++) {
        if (nbClusterFailuresPerLevel[i] == 0)
            continue;
        printf("%d:%d ", i, nbClusterFailuresPerLevel[i]);
    }
    printf("\ncumulative:\n");
    int sumcr{0};
    for(int i=0;i<instance->nVars(); i++) {
        if (nbClusterFailuresPerLevel[i] == 0)
            continue;
        sumcr += nbClusterFailuresPerLevel[i];
        printf("%d:%d ", i, sumcr);
    }

    printf("\nNumber of BTs for other reasons at each level:\n");
    for(int i=0;i<instance->nVars(); i++) {
        if (nbOtherFailuresPerLevel[i] == 0)
            continue;
        printf("%d:%d ", i, nbOtherFailuresPerLevel[i]);
    }
    printf("\ncumulative:\n");
    int sumo{0};
    for(int i=0;i<instance->nVars(); i++) {
        if (nbOtherFailuresPerLevel[i] == 0)
            continue;
        sumo += nbOtherFailuresPerLevel[i];
        printf("%d:%d ", i, sumo);
    }
    printf("\n");

    dag->printStats();
}
