#ifndef ELSA_OPTIONS_HPP
#define ELSA_OPTIONS_HPP

#include <string>

enum class dtree_use {
    NONE, TREES, LAZY_DD
};


// _VEB means to use the van Emde Boas layout for the trees, but
// surprisingly that makes things worse
enum class dtree_construction { LEX, IG, BO, LEX_VEB, IG_VEB, BO_VEB };

struct Options
{
    std::string inputfile;
    std::string cmdline;

    bool hassolutionfile{false}; // for debugging
    std::string solutionfile; // for debugging

    double timeLimit{3600.0};

    bool printsolutions{false};

    int bound_max_elem_small{20};
    int bound_max_elem_large{26};

    /*
     *  Number of restarts for local search.  Two stage process: if
     *  gap between lower bound and upper bound is too large, redo
     *  with larger number of restarts in an effort to reduce the
     *  upper bound further.
     */
    int ls_nrestarts_small{50};
    int ls_nrestarts_large{500};

    // backtracking search width
    int bt_width_prefix_permute{4};

    bool GAC{true};
    bool gacsupport{false};

    bool RCclusters{true};
    bool restarts{false};

    dtree_use dtrees{dtree_use::TREES};
    // use decision trees to answer mincost subset queries? off by
    // default because it is SLOW
    bool mincostdtrees{false};
    dtree_construction dtree_heuristic { dtree_construction::BO };
    // how many nodes should we wait before we reconstruct our
    // decision trees with the best ordering
    int bestOrdering_switchover { 1000 };
};

#endif
