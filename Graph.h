/*
 *  Graph: Routines for a directed graph.
 */
#ifndef _GRAPH_H_
#define _GRAPH_H_

#include <vector>
#include <stack>

class Graph
{
    public:
	Graph( int n );
	~Graph();

	/*
	 *  Number of vertices in graph.
	 */
	int nVertices();

	/*
	 *  Empty the graph.
	 */
	void clear();

	/*
	 *  Add a directed edge (v, w) to graph.
	 */
        void addEdge( int v, int w );

        /*
         *  Routines for strongly connected components.
         */
        typedef std::vector<int> Component;
        void findSCCs();
        void sortSCCs();
        int nSCCs();
        int maxSCC();
	Component getSCC( int i );

	void dumpGraph();

    private:
	int n;		// number of vertices
	bool **graph;	// adjacency matrix graph representation

        /*
         * Strongly Connected Components
         */
        std::vector<Component> sccs;
        void depthFirstSearch( int v, bool *visited, std::stack<int> &stack );
        void reverseDFS( int v, bool *inComponent, std::stack<int> &stack );
};

#endif // _GRAPH_H_
