/*
 * Managing domains of parent set variables: explicit and compressed
 * representation, reduced costs, membership queries, pruning,
 * backtracking
 */


#ifndef ELSA_DOMAIN_HPP_
#define ELSA_DOMAIN_HPP_

#include <vector>

#include "Types.hpp"
#include "ZDD.h"

class ZDD;

/*
 *  Structure of a domain value: The parent set, the cost
 *  of this parent set, and whether the domain value is valid;
 *  i.e., has not been pruned away by constraint propagation.
 */
struct Value {
    Score  cost;
    Set    parentSet;
    int    cardinality;
    int    valid;
};

/* Stats */
struct DomainPStats
{
    uint64_t size{0};
    uint64_t values_visited{0};

    uint64_t dt_nodes{0};
    double dt_construction_time{0.0};

    uint64_t subsetcalls{0};
    uint64_t subsetchecks{0};
    uint64_t mincsubsetcalls{0};
    uint64_t mincsubsetchecks{0};
    uint64_t dt_nodes_visited_subset{0};
    uint64_t dt_nodes_visited_mincsubset{0};
};

template<typename Ps>
class DomainP_base;

/* A store for all domains in the problem. */
class DomainPs
{
public:
    using vec_type = std::vector<int>;
public:
    DomainPs(const Options& opt)
        : opt(opt)
    {
    }

    void setNvars(int n)
    {
        assert(nvars==0);
        nvars = n;
    }
    void addVariable(std::vector<Value>&& values);

    // once we have a bestOrdering, reconstruct the decision trees
    // using this as the heuristic
    void reconstructDtrees(std::span<int> bestOrdering);

    // a shim forwarding all calls back to here
    DomainP_base<DomainPs> getDomainP(int var);
    DomainP_base<const DomainPs> getDomainP(int var) const;
    DomainP_base<DomainPs> operator[](int var);
    DomainP_base<const DomainPs> operator[](int var) const;

    // get the parent set corresponding to value with index 'a'
    Set getParentSet(int var, int a) const { return parentSets[var][a]; }
    int getCardinality(int var, int a) const { return cardinalities[var][a]; }

    /* check domain membership, remove, restore.  */

    //
    bool valid(int var, int a) const;
    // remove a single value or all values that intersect
    // 'vars'. Returns true if domain is empty after removal. Using
    // level == nVars means remove permanently
    bool remove(int var, int a, int level);
    bool removeSetsIntersecting(int var, Set vars, int level);

    // restore all values that were pruned at level or later
    void restore(int level);

    // current and total size
    std::size_t size(int var) const { return currentSize[var]; }
    std::ptrdiff_t ssize(int var) const { return currentSize[var]; }

    std::size_t totalSize(int var) const { return sizes[var]; }
    std::ptrdiff_t totalSSize(int var) const { return sizes[var]; }

    bool empty(int var) const { return size(var) == 0; }

    // Reveal the cached vector representation. Similar caveats as the
    // vector interface in DomainP
    const vec_type& asvec(int var) const
    {
        prepareCondensedDomain(var);
        return condensedDomain[var];
    }

    // cost access, modification
    Score getCost(int var, int a) const
    {
        return costs[var][a];
    }

    Score getReducedCost(int var, int a) const
    {
        return getCost(var, a) - delta_costs[var][a];
    }

    // it is not necessary to expose this, except for some
    // optimizations in DAG::clusterCost
    Score getDeltaCost(int var, int a) const
    {
        return delta_costs[var][a];
    }

    void incReducedCost(int var, int a, Score r)
    {
        delta_costs[var][a] += r;
        assert(delta_costs[var][a] <= getCost(var, a));
        if (delta_costs[var][a] == getCost(var, a) && ddRepr[var])
            ddRepr[var]->unmask_value(*mask[var], getParentSet(var, a));
    }

    void resetReducedCosts(); // resets to original cost (as given by getCost())

    // set-based access. Returns optional<int> which contains a
    // witness if such a value exists
    int hasSubsetOf(int var, Set s) const;
    int has0RCSubsetOf(int var, Set s) const;

    // cost- & set-based access. Returns options<int> which contains
    // the minimum cost value which is subset of 's', or is empty if
    // no such value exists. Note this is cost, not reduced
    // cost. First version looks only at current domains, second
    // version considers the entire domain
    int minCostSubsetOf(int var, Set s) const;
    int minCostSubsetOf_any(int var, Set s) const;

    // there is only one use case for these, in
    // Instance::symmetricParentVariables. It should never be used
    // otherwise.
    void updateCost(int var, int a, Score newCost);
    void updateParentSet(int var, int a, Set newPSet);

    // stats
    DomainPStats getStats(int var) const { return stats[var]; }

    // For debugging
    void verifyDDconsistency(int var);
private:
    // options. not indexed by var
    const Options& opt;

    int nvars{0};

    // all following member variables are indexed by variable id

    // all values. On pruning, valid becomes false
    std::vector<std::size_t> sizes;
    std::vector<std::unique_ptr<Score[]>> costs;
    std::vector<std::unique_ptr<Set[]>> parentSets;
    std::vector<std::unique_ptr<int[]>> cardinalities;
    std::vector<std::unique_ptr<int[]>> valids;

    // cached info on domain
    std::vector<int> currentSize;
    std::vector<int> lastValid;

    // a cached set of only the unpruned values. Gets updated lazily
    // on pruning. When restoring, needsReset is set to true and
    // denseDomain is lazily reconstructed from scratch
    mutable std::vector<std::vector<int>> condensedDomain;
    mutable std::vector<std::vector<Set>> condensedParentSets;
    mutable std::vector<uint8_t> needsReset;
    mutable std::vector<uint8_t> needsResetPSets;

    // delta costs, used to compute reduced costs. So reduced cost ==
    // getCost() - delta cost
    std::vector<std::vector<Score>> delta_costs;

    // chronological trail of pruned values. trail_sep contains the
    // index at which the prunings of the corresponding level begin,
    // so all prunings at level l are in the right-open range
    // trail[trail_sep[l]]...trail[trail_sep[l+1]]
    struct pruning { int var; int val; };
    std::vector<pruning> trail;
    std::vector<int> trail_sep;

    // the decision tree representation
    std::vector<std::unique_ptr<ZDD>> ddRepr;
    std::vector<std::optional<DTmask>> mask;

    // what it says. mutable because it is not state
    mutable std::vector<DomainPStats> stats;

    // helpers for managing condensedDomain
    void prepareCondensedDomain(int var) const; // it modifies the mutable, so it is const :-/
    bool dirty(int var) const;

    void prepareCondensedPSets(int var) const;
};

/* This provides a shim to access the domain of a single variable. All
 * of its member functions are also present in DomainPs */
template<typename Ps>
class DomainP_base
{
public:
    DomainP_base(Ps& ds, int var)
        : domains(ds)
        , thisvar(var)
    {
    }

    // documentation for all these in DomainPs

    Set getParentSet(int a) const
    {
        return domains.getParentSet(thisvar, a);
    }
    int getCardinality(int a) const
    {
        return domains.getCardinality(thisvar, a);
    }

    bool valid(int a) const
    {
        return domains.valid(thisvar, a);
    }
    bool remove(int a, int level)
    {
        return domains.remove(thisvar, a, level);
    }
    bool removeSetsIntersecting(Set vars, int level)
    {
        return domains.removeSetsIntersecting(thisvar, vars, level);
    }

    std::size_t size() const { return domains.size(thisvar); }
    std::ptrdiff_t ssize() const { return domains.ssize(thisvar); }

    std::size_t totalSize() const { return domains.totalSize(thisvar); }
    std::ptrdiff_t totalSSize() const { return domains.totalSSize(thisvar); }

    bool empty() const { return size() == 0; }

    // vector interface. To be used with care, because if we prune()
    // something while we iterate this, it will change under our feet.
    using iterator = std::vector<int>::iterator;
    using const_iterator = std::vector<int>::const_iterator;

    const_iterator begin() const { return asvec().begin(); }
    const_iterator end() const { return asvec().end(); }

    const std::vector<int>& asvec() const
    {
        return domains.asvec(thisvar);
    }

    Score getCost(int a) const
    {
        return domains.getCost(thisvar, a);
    }

    Score getReducedCost(int a) const
    {
        return domains.getReducedCost(thisvar, a);
    }

    Score getDeltaCost(int a) const
    {
        return domains.getDeltaCost(thisvar, a);
    }

    void incReducedCost(int a, Score r)
    {
        domains.incReducedCost(thisvar, a, r);
    }

    int hasSubsetOf(Set s) const
    {
        return domains.hasSubsetOf(thisvar, s);
    }

    int has0RCSubsetOf(Set s) const
    {
        return domains.has0RCSubsetOf(thisvar, s);
    }

    int minCostSubsetOf(Set s) const
    {
        return domains.minCostSubsetOf(thisvar, s);
    }

    void updateCost(int a, Score newCost)
    {
        domains.updateCost(thisvar, a, newCost);
    }
    void updateParentSet(int a, Set newPSet)
    {
        domains.updateParentSet(thisvar, a, newPSet);
    }

    // stats
    DomainPStats getStats() const { return domains.getStats(thisvar); }

    // For debugging
    void verifyDDconsistency() { domains.verifyDDconsistency(thisvar); }
private:
    Ps& domains;
    int thisvar;
};

using DomainP = DomainP_base<DomainPs>;
using const_DomainP = DomainP_base<const DomainPs>;

inline DomainP_base<DomainPs> DomainPs::getDomainP(int var)
{
    return DomainP_base<DomainPs>(*this, var);
}
inline DomainP_base<const DomainPs> DomainPs::getDomainP(int var) const
{
    return DomainP_base<const DomainPs>(*this, var);
}
inline DomainP_base<DomainPs> DomainPs::operator[](int var) { return getDomainP(var); }
inline DomainP_base<const DomainPs> DomainPs::operator[](int var) const { return getDomainP(var); }


#endif
