#ifndef _CLUSTERCUT_H_
#define _CLUSTERCUT_H_

#include "Instance.h"

class ClusterCut
{
    public:
        ClusterCut(Instance *i, Set vars, int ID);
        ~ClusterCut();

        Set getVariables() const;
        int getSize() const { return size; }
        int getSupportVar() const { return supportvar; }
        int getSupportVal() const { return supportval; }
        int getInfSupport(int w) const { return supportinf[w]; }
        int getSupSupportNotLast(int w) const { return supportsup[w]; }  // do not consider last domain value as it should be the empty parent set which is always a support
        void setSupportVar(int supvar) const { supportvar = supvar; }
        void setSupportVal(int supval) const { supportval = supval; }
        long long getActivity() const { return activity; }
        long long getNbChecked() const { return nbchecked; }
        void incActivity() const { activity++; }
        void incChecked() const { nbchecked++; }
        void incZero() const { nbzero++; }
        void print() const;
        void dump_01LP(FILE *file) const;
        int getID() const { return ID;}

        //bool operator<(const ClusterCut& right) const { return (size < right.size); }   // sort by increasing cluster size (number of parent variables)
        //bool operator<(const ClusterCut& right) const { return (size < right.size || (size == right.size && log_geometric_mean_support_score < right.log_geometric_mean_support_score)); }   // sort by increasing cluster size followed by minimum log of geometric mean of scores of supporting parentset values
        bool operator<(const ClusterCut& right) const { return (size < right.size || (size == right.size && support_initial_score > right.support_initial_score)); }   // sort by increasing cluster size followed by decreasing minimum score of a supporting parentset value
        //bool operator<(const ClusterCut& right) const { return ((long double)size / support_initial_score < (long double)right.size / right.support_initial_score); }   // sort by increasing cluster size divided by minimum score of a supporting parentset value
        //bool operator<(const ClusterCut& right) const { return (ID < right.ID); }   // sort by increasing ID

    private:

        Instance *instance;
        int size;
        int nbsupports;
        int ID;
        mutable int supportvar;
        mutable int supportval;
        Score support_initial_score;
        //double log_geometric_mean_support_score;
        mutable long long activity;
        mutable long long nbchecked;
        mutable long long nbzero;
        Set clusterVariables;
        std::vector<int> supportinf;
        std::vector<int> supportsup;
};

#endif // _CLUSTERCUT_H_
