/*
 *  Routines for enforcing the constraint that the parent
 *  variables must form a directed acyclic graph.
 */
#ifndef _DAG_H_
#define _DAG_H_

#include <vector>
#include <iostream>
#include <boost/functional/hash.hpp>
#include <unordered_map>
#include <memory>
#include <queue>
#include <set>
#include "ClusterCut.h"

// remove cluster cuts if activity * ratio < checks
#define CLUSTERPOOLACTIVITYRATIO 1000LL
// keep all cluster cuts below this size
#define CLUSTERPOOLMINSIZE 10
// update best upper bound from isAcylic/GAC DAG support

class Instance;
class LocalSearch;
class Timer;

using ClusterList = std::multiset<ClusterCut>;
struct DTmask;

class DAG
{
    public:
        DAG( Instance *instance, LocalSearch *localSearch, Timer *timer,
             const Options &options);
        ~DAG();

        /*
         *  Propagate the acyclic constraint over the parent variables.
         */
        bool propagate(
                int *currentParents, int *currentOrdering, int *currentDepth,
                int currentLevel, bool *instantiatedP, int v,
                Score currentLB, Score *currentUB, int **bestParents, int **bestOrdering,
                bool *restart );

        bool clusterFailed;

        void printStats();
        void dump_01LP(FILE *file) const;

        bool debugBacktrack(int currentLevel, const int *currentOrdering) const;

    private:
        Instance *instance;
        LocalSearch *localSearch;
        Timer *timer;
        const Options& opt;
        bool printonlyonce{true};

        int nbKeyLookups_cluster{0};
        int nbKeyFound_cluster{0};
        int nbSuccessfulCompletions_cluster{0};
        int nbKeyLookups_minCluster{0};
        int nbKeyFound_minCluster{0};
        int nbSuccessfulCompletions_minCluster{0};
        mutable long long nbDomainIterations{0};

        long long nbNC{0};
        long long nbone{0};

        std::vector<int> validOrdering, validOrderingIndexOf;
        std::vector<int> vO_buf, vOIdx_buf;
        std::vector<Set> support;
        std::vector<int> supportIndex; // foreach variable index, its domain value index
        std::vector<int> supportDepth; // foreach variable index, its depth in the associated DAG
        std::vector<int> supportOrdering; // foreach ordering position, its corresponding variable index
        Score supportCost{0};
        std::vector<int> seenOrdering, seenOrderingIndexOf, seenOrderingSupportOf;
        std::vector<Score> deltaCostMax;   // upper bound on cumulative score per variable extended to the current lower bound for some valid domain values
        std::vector<int> firstValidValueP;   // first valid domain value per variable (updated for propagateCluster)
        std::vector<int> lastModifiedValueP;   // last valid domain value per variable whose score has been reduced by a cluster cut propagation (updated for propagateCluster)

        ClusterList clusterCuts;

        mutable long long nbparentsetvaluechecked{0};
        mutable long long nbparentsetvalueactive{0};
        mutable long long nbclusterpoolchecked{0};
        mutable long long nbclusterpoolactive{0};
        mutable long long nbclusterpoolfastzero{0};

        int lastClusterCutID;

        using cache_element = std::vector<int>;
#ifdef SET128
        std::unordered_map< Set, std::shared_ptr<cache_element>, boost::hash<Set> > seenSubsequences;
#else
        std::unordered_map< Set, std::shared_ptr<cache_element> > seenSubsequences;
#endif

        void saveSeenSubsequences(int minSubSeqSize);
        template<typename P>
        bool subSeqIsValid(Set *subseq, Set *cluster, int *k, bool *instantiatedP, P pred);

        /*
         *  Pruning rule based on necessary edges.
         */
        bool propagateNecessaryEdges(
                int currentLevel, bool *instantiatedP, int v );

        /*
         *  Satisfiability test.
         */
        Set orderVariables_0rc(int* currentParents, int* currentOrdering,
            int currentLevel, bool* instantiatedP, int currentOrder);

        bool isAcyclic( int *currentOrdering, int currentLevel, int *currentParents, bool *instantiatedP, int v ); // this one does GAC
        bool isAcyclic( int *currentOrdering, int currentLevel, int v, int a );
        bool isAcyclic( int *currentOrdering, int *currentDepth, int currentLevel );
        bool propagateClusters( int *currentParents, int *currentOrdering, int currentLevel, bool *instantiatedP,
                                int v, Score currentLB, int **bestParents, int **bestOrdering, Score *currentUB, bool *restart);
        Set minimizeCluster(Set cluster, bool *instantiatedP, int *currentParents);
        bool isClusterValue(int v, int a, Set clustervars, const bool *instantiatedP, const int *currentParents) const;
        bool isSatisfyingValue(int v, int a, Set clustervars, const bool *instantiatedP, const int *currentParents) const;
        Score getCost(int v, int a, const bool *instantiatedP, const int *currentParents) const;
        Score getCost(int v, int a) const { return instance->getDomainP(v).getReducedCost(a); }
        Score clusterCost(const ClusterCut& cut, int *currentParents, bool *instantiatedP) const;

        Score updateBestOrdering(int *bestParents, int *bestOrdering );

        void printParentSetsWithDeltaCosts(int v, const bool *instantiatedP, const int *currentParents) const;
};

#endif // _DAG_H_
