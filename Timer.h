#ifndef _TIMER_H_
#define _TIMER_H_

class Timer
{
    public:
        void    start();
        double  elapsedTime();

    private:
        double t{0.0};
};

#endif // _TIMER_H_
