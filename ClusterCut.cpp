#include "ClusterCut.h"
#include "Instance.h"
#include <algorithm>
#include <cmath>

/*
 *  Constructor for cluster cut.
 */
ClusterCut::ClusterCut(Instance *i, Set vars, int ID_) :
    instance(i),
    size(0),
    nbsupports(0),
    ID(ID_),
    supportvar(-1),
    supportval(-1),
    support_initial_score(MAX_SCORE),
    activity(1),
    nbchecked(1),
    nbzero(0),
    clusterVariables(vars)
{
        for( int w = 0; w < instance->nVars(); w++ )
            if( element_of( w, vars ) )
                size++;

        for( int w = 0; w < instance->nVars(); w++ ) {
            supportinf.push_back(0);
            supportsup.push_back( i->getDomainSizeP( w ) - 1 );
            if( !element_of( w, vars ) )
                continue;
            bool first = true;
            for( int a = 0; a < i->getDomainSizeP( w ); a++ ) {
                if( !set_empty(intersection(i->getParentSet(w, a), vars)) )
                    continue;

                nbsupports++;
                Score score = i->getCost(w, a);
                if (score < support_initial_score) {
                    support_initial_score = score;
                    supportvar = w;
                    supportval = a;
                }
                // do not consider last domain value as it should be
                // the empty parent set which is always a support
                if (first || a < i->getDomainSizeP( w ) - 1) {
                    supportsup[w] = a;
                }
                if (first) {
                    supportinf[w] = a;
                    first  = false;
                }
            }
        }
        if (supportvar < 0) {
            printf("cut %d: size %d, vars = ", ID, size);
            for(int w = 0; w != instance->nVars(); ++w)
                if (element_of(w, vars))
                    printf("%d ", w);
            printf("\n");
        }
        assert(supportvar >= 0);
        assert(supportval >= 0);
}

/*
 *  Destructor for cluster cut.
 */
ClusterCut::~ClusterCut()
{
}

Set
ClusterCut::getVariables() const
{
    return clusterVariables;
}

void
ClusterCut::print() const
{
    printf(FRED "%d vars = [", size);
    for( int w = 0; w < instance->nVars(); w++ )
        if( element_of( w, clusterVariables ) )
            printf("%d ",  w);
    //printf("]/%d sup:%d (%d,%d) geoscore:%g minscore:%0.*f\n" FNONE, activity, nbsupports, supportvar, supportval, log_geometric_mean_support_score, precision, (double)support_initial_score/scale);
    printf("]/%lld/%lld/%lld sup:(%d,%d)/%d initscore:%0.*f\n" FNONE, activity, nbchecked, nbchecked-nbzero, supportvar, supportval, nbsupports, precision, (double)support_initial_score/scale);
}

void
ClusterCut::dump_01LP(FILE *file) const
{
        bool first = true;
        for( int w = 0; w < instance->nVars(); w++ ) if( element_of( w, clusterVariables ) ) {
                for( int a = 0; a < instance->getDomainSizeP( w ); a++ ) {
                        if( set_empty(intersection(instance->getParentSet(w, a), clusterVariables)) ) {
                                if (!first)
                                        fprintf(file, " + ");
                                fprintf(file, "x%d_%d", w, a);
                                first = false;
                        }
                }
        }
        fprintf(file, " >= 1\n");
}
