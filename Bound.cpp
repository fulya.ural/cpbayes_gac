/*
 *  Routines for determining bounds.
 *
 *  A lower bound on the cost of completing a partial solution
 *  of an instance of the combinatorial optimization problem for
 *  learning a Bayesian network from data. Must be an admissible
 *  heuristic; i.e., less than or equal to the optimal cost.
 *
 *  Lower bound is based on:
 *  Fan, X. and Yuan, C.
 *  An improved lower bound for Bayesian network structure learning.
 *  AAAI-2015.
 *
 *  An upper bound on the cost of a solution, using local search.
 */

#include "Graph.h"
#include "Pattern.h"
#include "Instance.h"
#include "Bound.h"
#include "LocalSearch.h"

Bound::Bound( Instance *instance, const Options& options )
    : opt(options)
    , instance(instance)
    , graph(std::make_unique<Graph>(instance->nVars()))
    , localSearch(std::make_unique<LocalSearch>(instance, options))
{
    this->instance = instance;

#ifdef DYNSET
    block = new int*[MAX_VARS]();
    last = new int[MAX_VARS]();
#endif

    nBlocks = 0;

    for( int i = 0; i < MAX_VARS; i++ ) {
        block[i] = new int[opt.bound_max_elem_large]();
    }
}

Bound::~Bound()
{
    for( int i = 0; i < MAX_VARS; i++ ) {
        delete[] block[i];
    }
    for( int i = 0; i < nBlocks; i++ ) {
        delete pattern[i];
    }
    delete[] pattern;
#ifdef DYNSET
        delete[] block;
        delete[] last;
#endif
}

void
Bound::printNonZeroQueries()
{
        printf("nBlocks is %d\n",nBlocks);
        for( int i = 0; i < nBlocks; i++ ) {
                printf("Partition number %d of size ",i);
            pattern[i]->printNonZeroQueries(i);
        }
}

/*
 *  Greedily construct a lower bound on the cost of completing
 *  a partial solution by picking the lowest cost domain element
 *  for each variable. Relies on the domains being sorted by
 *  increasing costs.
 */
Score
Bound::getGreedyLowerBound( bool *instantiated )
{
    Score cost;

    cost = 0;
    for( int v = 0; v < instance->nVars(); v++ ) {
        if( !instantiated[v] ) {
            /*
             *  Find the first valid domain element.
             *  As the domains are sorted, this will also
             *  be the lowest cost domain element.
             */
            const auto& dom = instance->getDomainP(v);
            int a = *dom.begin();
            cost += instance->getCost( v, a );
        }
    }

    return( cost );
}

/*
 *  Greedily construct an upper bound on the cost of completing
 *  a partial solution by picking the highest cost domain element
 *  for each variable. Relies on the domains being sorted by
 *  increasing costs.
 */
Score
Bound::getGreedyUpperBound( bool *instantiated )
{
    Score cost;

    cost = 0;
    for( int v = 0; v < instance->nVars(); v++ ) if( !instantiated[v] ) {
        /*
         *  Find the last valid domain element.
         *  As the domains are sorted, this will also
         *  be the highest cost domain element.
         */
         auto &vdom = instance->getDomainP(v).asvec();
         int a = vdom.back();
         cost += instance->getCost(v, a);
    }

    return( cost );
}

/*
 *  Determine the static partitions and fill in the
 *  associated pattern databases for the partitions.
 */
void::
Bound::partition( bool *instantiated )
{
    partitionAux( instantiated, opt.bound_max_elem_small );
}

/*
 *  Determine the static partitions and fill in the
 *  associated pattern databases for the partitions.
 *
 *  This time do more work as the initial gap in the
 *  bounds was considered too wide.
 */
void::
Bound::repartition( bool *instantiated )
{
    for( int i = 0; i < nBlocks; i++ ) {
        delete pattern[i];
    }
    delete[] pattern;

    partitionAux( instantiated, opt.bound_max_elem_large );
}

void
Bound::partitionAux( bool *instantiated, int maxPartitionSize )
{
    /*
     *  Find SCCs using all parent sets.
     */
    instance->addEdges( graph.get() );
    graph->findSCCs();

    /*
     *  Case 1:
     *  If largest SCC fits, each scc becomes a pattern database.
     */
    if( graph->maxSCC() <= maxPartitionSize ) {
        printf( "%s: partition, Case 1: max size of SCC = %d (%d)\n",
            instance->benchmarkName(), graph->maxSCC(), maxPartitionSize );
        int m = 0;
        for( int i = 0; i < graph->nSCCs(); i++ ) {
            Graph::Component c = graph->getSCC( i );
            if( c.size() == 1 && instantiated[c[0]] ) continue;
            for( unsigned int j = 0; j < c.size(); j++ ) {
                block[m][j] = c[j];
            }
            last[m] = c.size();
            m++;
        }
        nBlocks = m;

        pattern = new Pattern*[nBlocks]();
        for( int i = 0; i < nBlocks; i++ ) {
            pattern[i] = new Pattern( instance, last[i] );
        }

        fillPartition();
        return;
    }

    /*
     *  Case 2:
     *  Look through SCCs based on top few lowest cost parent sets.
     */
    int top;
    for( top = MAX_TOP; top >= 1; top-- ) {
        graph->clear();
        instance->addEdges( graph.get(), top );
        graph->findSCCs();
        if( graph->maxSCC() <= maxPartitionSize ) {
            break;
        }
    }

    if( graph->maxSCC() <= maxPartitionSize ) {
        printf( "%s: partition, Case 2: top = %d, max = %d (%d)\n",
            instance->benchmarkName(), top, graph->maxSCC(), maxPartitionSize );
        int m = 0;
        for( int i = 0; i < graph->nSCCs(); i++ ) {
            Graph::Component c = graph->getSCC( i );
            if( c.size() == 1 && instantiated[c[0]] ) continue;
            /*
             *  Check if there is room for this SCC with the previous SCC.
             */
            int k = m-1;
            if( (k >= 0) &&
                c.size() <= (unsigned int)(maxPartitionSize - last[k]) ) {
                for( unsigned int j = 0; j < c.size(); j++ ) {
                    block[k][last[k]+j] = c[j];
                }
                last[k] = last[k] + c.size();
            }
            else {
                for( unsigned int j = 0; j < c.size(); j++ ) {
                    block[m][j] = c[j];
                }
                last[m] = c.size();
                m++;
            }
        }
        nBlocks = m;

        pattern = new Pattern*[nBlocks]();
        for( int i = 0; i < nBlocks; i++ ) {
            pattern[i] = new Pattern( instance, last[i] );
        }

        fillPartition();
        return;
    }

    /*
     *  Case 3:
     *  SCCs do not fit within a block.
     */
    printf( "%s: partition, Case 3: max = %d (%d)\n",
        instance->benchmarkName(), graph->maxSCC(), maxPartitionSize );
    int vertices[instance->nVars()];
    int m = 0;
    for( int i = 0; i < graph->nSCCs(); i++ ) {
        Graph::Component c = graph->getSCC( i );
        for( unsigned int j = 0; j < c.size(); j++ ) if( !instantiated[c[j]] ) {
            vertices[m] = c[j];
            m++;
        }
    }

    int blockSize = maxPartitionSize/2;
    nBlocks = m / blockSize;
    if( (nBlocks * blockSize) != instance->nVars() ) {
        nBlocks++;
    }

    for( int v = 0; v < nBlocks; v++ ) {
        last[v] = 0;
    }

    int i = 0;
    int j = 0;
    for( int v = 0; v < m; v++ ) {
        block[i][j] = vertices[v];
        last[i]++;
        j++;
        if( j >= blockSize ) {
            j = 0;
            i++;
        }
    }

    pattern = new Pattern*[nBlocks]();
    for( int i = 0; i < nBlocks; i++ ) {
        pattern[i] = new Pattern( instance, last[i] );
    }

    improvePartition();
    coalescePartition( maxPartitionSize );
    fillPartition();
}

Score
Bound::fillBlock( int i )
{
    Set ancestors = emptySet;
    Set partition = emptySet;
    for( int j = 0; j < instance->nVars(); j++ ) {
        add_element( j, ancestors );
    }
    for( int j = 0; j < last[i]; j++ ) {
        remove_element( block[i][j], ancestors );
        add_element( block[i][j], partition );
    }

    Score cost = pattern[i]->fill( ancestors, partition );

    return( cost );
}

/*
 *  Fill in the static pattern databases.
 */
Score
Bound::fillPartition()
{
    /*
     *  For each block, fill in the pattern database.
     */
    Score cost = 0;
    for( int i = 0; i < nBlocks; i++ ) {
        cost += fillBlock( i );
    }

    return( cost );
}

/*
 *  Local hill-climbing search to improve a partition.
 */
void
Bound::improvePartition()
{
    Score bestCost = fillPartition();

    bool improving = false;
    int iter = 0;
    printf( "hill climbing cost (iter = %d) = %lu\n",
        iter, bestCost );
    do {
        /*
         *  Swaps as neighborhood function and
         *  first-improvement move.
         */
        improving = false;
        iter++;
        for( int i = 0; i < nBlocks && !improving; i++ )
        for( int a = 0; a < last[i] && !improving; a++ )
        for( int j = i+1; j < nBlocks && !improving; j++ )
        for( int b = 0; b < last[j] && !improving; b++ ) {

            Score cost = bestCost;
            cost -= fillBlock( i ) +
                    fillBlock( j );

            int v = block[i][a];
            int w = block[j][b];
            block[i][a] = w;
            block[j][b] = v;

            cost += fillBlock( i ) +
                    fillBlock( j );

            if( lt( bestCost, cost ) ) {
                bestCost = cost;
                printf( "hill climbing cost (iter = %d) = %lu\n",
                    iter, bestCost );
                improving = true;
            }
            else {
                int v = block[i][a];
                block[i][a] = block[j][b];
                block[j][b] = v;
            }
        }
    } while( improving );
}

/*
 *  Coalesce smaller partitions into larger partitions
 *  after hill climbing.
 */
void
Bound::coalescePartition( int maxPartitionSize )
{
    int vertices[instance->nVars()];
    int m = 0;
    for( int i = 0; i < nBlocks; i++ ) {
        for( int a = 0; a < last[i]; a++ ) {
            vertices[m] = block[i][a];
            m++;
        }
        delete pattern[i];
    }
    delete[] pattern;

    int blockSize = maxPartitionSize;
    nBlocks = m / blockSize;
    if( (nBlocks * blockSize) != instance->nVars() ) {
        nBlocks++;
    }

    for( int v = 0; v < nBlocks; v++ ) {
        last[v] = 0;
    }

    int i = 0;
    int j = 0;
    for( int v = 0; v < m; v++ ) {
        block[i][j] = vertices[v];
        last[i]++;
        j++;
        if( j >= blockSize ) {
            j = 0;
            i++;
        }
    }

    pattern = new Pattern*[nBlocks]();
    for( int i = 0; i < nBlocks; i++ ) {
        pattern[i] = new Pattern( instance, last[i] );
    }
}

/*
 *  Construct a lower bound on the cost of completing
 *  a partial solution using static partitioning.
 *
 *  The idea is to partition the vertices into blocks
 *  of fixed size and find the optimal solution within
 *  each partition using dynamic programming.
 *
 *  See work by Malone et al.
 */
Score
Bound::getLowerBound( bool *instantiated )
{
    Score cost = 0;
    for( int i = 0; i < nBlocks; i++ ) {
        cost += pattern[i]->lookup( instantiated );
    }

    return( cost );
}

/*
 *  Construct an upper bound on the best solution using local search.
 */
Score
Bound::getUpperBound( int *bestOrdering, int *bestSolution, int strength )
{
    Score cost = localSearch->hillClimbWithRestarts(
                    bestOrdering, bestSolution,
                    (strength == 0) ? opt.ls_nrestarts_small
                                    : opt.ls_nrestarts_large );

    return( cost );
}

