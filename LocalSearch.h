/*
 *  Routines for performing local search on an instance of the
 *  combinatorial optimization problem for learning a Bayesian
 *  network from data.
 */
#ifndef _LOCALSEARCH_H_
#define _LOCALSEARCH_H_

class Instance;

class LocalSearch
{
    public:
        LocalSearch( Instance *instance, const Options& opt );
        ~LocalSearch();

        /*
         *  Hill-climbing local search with restarts.
         */
        Score hillClimbWithRestarts(
                int *bestOrdering, int *bestSolution, int nRestarts );

        void sortByDepth( int *ordering, int *solution );

    private:
        Instance *instance;
        const Options& opt;

        /*
         *  For randomized versions of routines, randomly select a variable
         *  from among the MAX_VALUES variables with the lowest cost.
         */
        static const int MAX_VALUES = 5;

        /*
         *  Given an ordering of m of the parent variables, m <= n,
         *  find the variable that is not in the ordering and has the lowest
         *  cost parent set (domain value) that is a subset of the variables
         *  in the ordering. Returns -1 if there is no such variable.
         *  The randomized version returns a randomly selected variable
         *  from among the MAX_VALUES variables with the lowest cost.
         */
        int findSmallestConsistentWithOrdering( int *ordering, int m );
        int findSmallestConsistentWithOrderingRandom( int *ordering, int m );

        /*
         *  Greedily construct an ordering in forward order, starting
         *  with first position. Pick node with smallest cost consistent
         *  with ordering so far and place in next open position. Repeat.
         */
        void greedyHeuristic(
                int *ordering, int *solution, Score *cost );
        /*
         *  The randomized heuristic randomly picks from among a small
         *  number of nodes with the smallest cost. Useful for restarts.
         */
        void greedyRandomizedHeuristic(
                int *ordering, int *solution, Score *cost );

        /*
         *  Perform a simple hill-climbing search using swaps as
         *  the neighborhood function and the first-improvement or
         *  the best-improvement move.
         *
         *  Assumes that ordering and solution have been initialized
         *  and that cost represents the cost of the initial ordering.
         */
        void hillClimbingFirstImprovement(
                int *ordering, int *solution, Score *cost );
        void hillClimbingBestImprovement(
                int *ordering, int *solution, Score *cost );
};

#endif // _LOCALSEARCH_H_
