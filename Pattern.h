/*
 *  Routines for static pattern database for lower bound.
 *
 *  Fan, X. and Yuan, C.
 *  An improved lower bound for Bayesian network structure learning.
 *  AAAI 2015.
 */
#ifndef _PATTERN_H_
#define _PATTERN_H_

#include <Instance.h>

class Pattern
{
    public:
	Pattern( Instance *instance, int m );
	~Pattern();

	void printNonZeroQueries(int n);

	Score lookup();
	Score lookup( bool *instantiated );
	Score fill( Set ancestors, Set partition );

    private:
	Instance *instance;

	/*
	 *  Pattern database.
	 */
	int max_m;
	Score *db;
	int *counter;
	Set ancestors;
	Set partition;

	/*
	 *  Determine cardinality of a set.
	 */
	int card( Set node );
	int card64( Set64 node );
};

#endif // _PATTERN_H_
