/*
 *  Routines for memoizing partial solutions.
 */
#ifndef _MEMO_H_
#define _MEMO_H_

#include <Instance.h>

class Memo
{
    public:
	Memo();
	~Memo();

	enum MemoVal {
	    Updated = 1,
	    Dominated = 2,
	    Inserted = 4,
	    Equal = 8,
	    Failed = 16,
	    NotFound = 32
	};

	/*
	 *  Memoize the ordering with prefix cost pCost and
	 *  suffix cost sCost in the memoization database.
	 *  Returns:
	 *	NotFound  - insert = false, ordering not in db
	 *	Inserted  - insert = true, ordering not in db, inserted into db
	 *	Updated   - ordering in db, pCost < existing entry
	 *	Dominated - ordering in db, explored, pCost >= existing entry
	 *	Equal     - ordering in db, not yet explored, pCost == existing
	 *	Failed    - too many probes
	 */
	MemoVal memoize( int *ordering, int m,
		    Score pCost, Score sCost, bool insert, long long nbnodes );

#ifdef RESTART
	long long erase(std::set<long long> &incompletenodes);
#endif

	void printStats();

    private:
	/*
	 *  Hash table approximately 1 GB in size,
	 *  if using mask of 0x3FFFFFF;
	 */
	static const unsigned int mask = 0x3FFFFFF;
	unsigned int hash( Set key );

	Set   *key;
	Score *prefixCost;
	Score *suffixCost;
#ifdef RESTART
	long long *timeStamp;
#endif
	/*
	 *  Performance, statistics.
	 */
	int nLookups;
	int nCollisions;
	int nTotalProbes;
	int nInserted;
	int nDominated;
	int nUpdated;
	int nEqual;
	int nFailed;
};

#endif // _MEMO_H_
