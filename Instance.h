/*
 *  Instance: An instance of the combinatorial optimization problem
 *            for learning a Bayesian network from data.
 */
#ifndef _INSTANCE_H_
#define _INSTANCE_H_

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <vector>
#include <set>
#include <bitset>
#include <functional>
#include <memory>

#include "Types.hpp"
#include "Domain.hpp"
#include "Options.hpp"

// update value (parent variable) ordering at each best solution (DAG) found (aka solution phase saving) and restart
//#define RESTART

#define VERBOSE 0


class Graph;

class Instance
{
public:
        Instance( const Options& opt );
        ~Instance();

        /*
         *  Number of random variables in instance.
         */
        int nVars() const;

        /*
         *  Domain size of
         *      parent variable v,
         *      ordering variable v,
         *      depth variable v.
         */
        int getDomainSizeP( int v ) const;
        int getDomainSizeO( int v ) const;
        int getDomainSizeD( int v ) const;

        /*
         *  Current domain size of
         *      parent variable v,
         *      ordering variable v,
         *      depth variable v.
         */
        int getCurrentDomainSizeP( int v ) const;
        int getCurrentDomainSizeO( int v ) const;
        int getCurrentDomainSizeD( int v ) const;

        /*
         *  Name of instance.
         */
        const char *benchmarkName() const;

        /*
         *  Determine the minimum cost subnetwork (solution) and its cost
         *  for the ordering, given the ancestors.
         */
        Score minCostOrdering( Set ancestors,
                int *ordering, int m, int *solution = NULL );

        /*
         *  Determine the parent set for the variable v that has the
         *  minimum cost given v that comes next in the ordering;
         *  i.e., all parents of v must be in the ordering or in the
         *  set of ancestors. First two versions only look at current
         *  domain, the latter two look at the entire domain (used in
         *  preprocessing)
         */
        int getMinCostParentSet( int *ordering, int m, int v );
        int getMinCostParentSet( Set ancestors, int v );
        int getMinCostParentSet_any( int *ordering, int m, int v );
        int getMinCostParentSet_any( Set ancestors, int v );

        /*
         *  Determine the depth of the parent set a for the variable v given
         *  that v comes next in the ordering; i.e., all parents a of v will
         *  exist in the ordering. Ordering is valid for ordering[0..m-1].
         */
        int getDepthParentSet(
                int *ordering, int *depth, int m, int v, int a );
        int getDepthParentSetUnordered(
                int *ordering, int *depth, int m, int v, int a );

        /*
         *  Given parents set a of variable v, determine whether there
         *  exists a parent set b that is of lower cost and is contained
         *  in the induced parent set of a; i.e., where it is known that
         *  all of the variables in the ordering precede variable v.
         *
         */
        bool containedInIP( int *ordering, int m, int v, int a );
        bool containedInIP_condensed( int *ordering, int m, int v,
                                      const DomainPs::vec_type& dom, size_t idx );

        /*
         *  Return the cost of domain element a of parent variable v.
         */
        Score getCost( int v, int a );

        /*
         *  Determine whether x = a and y = b form a covered edge;
         *  i.e., x = P and y = (P union { x }).
         *  Determine whether y = b and z = c form a covered edge;
         *  if the covered edge x --> y is flipped to be y --> x.
         */
        bool coveredEdge( int x, int a, int y, int b );
        bool flippedCoveredEdge( int x, int a, int y, int b, int z, int c );
        bool triangleCoveredEdge(
                int w, int a,
                int x, int b,
                int y, int c,
                int z, int d );

        /*
         *  Print the parent set of element a of variable v.
         */
        void printParentSet( int v, int a );
        void printSet( Set aSet );

        /*
         *  Print a solution.
         */
        void printSolution( int *ordering, int *solution, Score cost );
        void writeSolution( FILE *fp, int *ordering, int *solution, Score cost );

        /*
         *  Routines for querying and setting whether domain element a of
         *  variable v is valid. A value of -1 indicates valid;
         *  0, ..., n-1 indicate the element was removed at that level
         *  in the search.
         *
         *  setValidP is an exception here. calling it with value ==
         *  -1 is incorrect. getDomainPs().reset(dlvl) will handle
         *  restoring all values.
         */
        bool validP( int v, int a );
        bool validO( int v, int a );
        bool validD( int v, int a );
    //int  getValidP( int v, int a );
        int  getValidO( int v, int a );
        int  getValidD( int v, int a );
        void setValidP( int v, int a, int value );
        void setValidO( int v, int a, int value );
        void setValidD( int v, int a, int value );

        DomainP& getDomainP(int v);
        DomainPs& getDomainPs();

        /*
         *  Determine set of necessary parents of vertices.
         *  Determine set of necessary edges for vertices.
         */
        void findNecessaryParents();
        void findNecessaryParents( int w );
        Set necessaryEdges( int v );
        Set getParentSet( int v, int a );

        /*
         *  Update parents sets given that parent variables
         *  v and w are symmetric.
         */
        void symmetricParentVariables( int v, int w );

        /*
         *  Add all or just the top few parent sets as
         *  edges in the graph.
         */
        void addEdges( Graph *graph );
        void addEdges( Graph *graph, int top );
        void addEdges( Graph *graph, bool *instantiated );

        /*
         * Reconstruct all decision trees using the current best solution
         * to get the variable ordering
         */
        void reconstructDtrees(std::span<int> bestOrdering);

        /*
         *  Debugging.
         */
        void dumpInstance();
        void dumpVariable(int v);
        void dumpState(
            int currentLevel,
            int *currentParents,
            int *currentOrdering,
            int *currentDepth,
            bool *instantiatedP );
        void dumpStateFull(
            int currentLevel,
            int *currentParents,
            int *currentOrdering,
            int *currentDepth,
            bool *instantiatedP );
        void dumpDebug(
            bool *instantiatedP,
            std::vector< std::vector<Score> > deltaCost,
            Set variables );

        int *initial_ordering{NULL};

    private:

        const Options& opt;
        void initialize( const char *fileName );

        static int compare( const void *a, const void *b );

        /*
         *  Constraint model.
         */
        int n;                          // number of random variables

        DomainPs domainP;
        std::vector<DomainP> domainPshims;
#ifdef DYNSET
        int *domainSizeO;      // domain size of ordering variables
        int **domainO;         // domain of ordering variables
        int *domainSizeD;      // domain size of depth variables
        int **domainD;         // domain of depth variables

        /*
         *  Current domain sizes and last valid domain element,
         *  maintained during search.
         */
        int *domainSizeCurrentO;
        int *domainSizeCurrentD;
#else
        int domainSizeO[MAX_VARS];      // domain size of ordering variables
        int *domainO[MAX_VARS];         // domain of ordering variables
        int domainSizeD[MAX_VARS];      // domain size of depth variables
        int *domainD[MAX_VARS];         // domain of depth variables

        /*
         *  Current domain sizes and last valid domain element,
         *  maintained during search.
         */
        int domainSizeCurrentO[MAX_VARS];
        int domainSizeCurrentD[MAX_VARS];
#endif

        /*
         *  Determine set of necessary parents of vertices.
         *
         *  A vertex v is a necessary parent of vertex w if v occurs
         *  in every currently valid parent set for variable w.
         */
#ifdef DYNSET
        Set *necessaryParents;
#else
        Set necessaryParents[MAX_VARS];
#endif
};

#endif // _INSTANCE_H_
