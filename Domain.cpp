#include <tuple>
#include "Domain.hpp"
#include "Timer.h"

#if defined(SET64) || defined(SET128)
#include <x86intrin.h>
#endif

void DomainPs::addVariable(std::vector<Value>&& values)
{
    costs.push_back({});
    parentSets.push_back({});
    cardinalities.push_back({});
    valids.push_back({});
    sizes.push_back(values.size());

    int thisvar = costs.size() - 1;

    // sort by cost, break ties by cardinality
    std::sort(values.begin(), values.end(),
              [&](auto&& a, auto&& b) {
                  return std::make_tuple(a.cost, a.cardinality)
                      < std::make_tuple(b.cost, b.cardinality);
              });

    // aligned_alloc is awkward
    auto *p = std::aligned_alloc(32, (values.size()+1)*sizeof(Set));
    auto *vp = new (p) Set[values.size()+4];
    parentSets[thisvar] = std::unique_ptr<Set[]>(vp);

    auto *c = std::aligned_alloc(32, (values.size()+1)*sizeof(Score));
    auto *vc = new (c) Score[values.size()+1];
    costs[thisvar] = std::unique_ptr<Score[]>(vc);

    cardinalities[thisvar] = std::make_unique<int[]>(values.size());
    valids[thisvar] = std::make_unique<int[]>(values.size());
    int idx = 0;
    for (auto v : values) {
        costs[thisvar][idx] = v.cost;
        parentSets[thisvar][idx] = v.parentSet;
        cardinalities[thisvar][idx] = v.cardinality;
        valids[thisvar][idx] = v.valid;
        ++idx;
    }

    // add the empty set at the end, so in minCostSubsetOf_any, we
    // know that we will always find a subset of s
    parentSets[thisvar][values.size()] = emptySet;

    currentSize.push_back(totalSize(thisvar));
    lastValid.push_back(totalSize(thisvar) - 1);

    condensedDomain.push_back({});
    condensedParentSets.push_back({});
    needsReset.push_back(true);
    needsResetPSets.push_back(true);
    prepareCondensedDomain(thisvar);
    prepareCondensedPSets(thisvar);

    delta_costs.push_back({});
    delta_costs[thisvar].resize(sizes[thisvar]);

    stats.push_back({});
    stats[thisvar].size = sizes[thisvar];

    ddRepr.push_back({});
    mask.push_back(std::nullopt);
    if (opt.dtrees != dtree_use::NONE) {
        // create decision tree
        Timer timer;
        timer.start();
        // fall back to IG for now if we want to use best ordering
        // heuristic. We will reconstruct the trees later, once we
        // actually have an ordering
        auto fallback_heuristic = [&]() {
            switch (opt.dtree_heuristic) {
            case dtree_construction::BO:
                return dtree_construction::IG;
            case dtree_construction::BO_VEB:
                return dtree_construction::IG_VEB;
            default:
                return opt.dtree_heuristic;
            }
        }();
        ddRepr[thisvar] = std::make_unique<ZDD>(nvars,
            std::span(parentSets[thisvar].get(), values.size()),
            std::span(costs[thisvar].get(), values.size()), thisvar,
            fallback_heuristic);
        mask[thisvar] = ddRepr[thisvar]->getnewmask();
        stats[thisvar].dt_construction_time = timer.elapsedTime();
        stats[thisvar].dt_nodes = ddRepr[thisvar]->getNbNodes();
    }
}

void DomainPs::reconstructDtrees(std::span<int> bestOrdering)
{
    for (int thisvar = 0; thisvar != nvars; ++thisvar) {
        Timer timer;
        timer.start();

        auto olddd = std::move(ddRepr[thisvar]);
        ddRepr[thisvar] = std::make_unique<ZDD>(nvars,
            std::span(parentSets[thisvar].get(), sizes[thisvar]),
            std::span(costs[thisvar].get(), sizes[thisvar]), thisvar,
            opt.dtree_heuristic, bestOrdering);

        // prune all that should be pruned
        for (size_t a = 0; a != sizes[thisvar]; ++a)
            if (!valid(thisvar, a))
                ddRepr[thisvar]->pruneParentSet(getParentSet(thisvar, a));

        stats[thisvar].dt_construction_time += timer.elapsedTime();
    }
}

void DomainPs::updateCost(int var, int a, Score newCost)
{
    costs[var][a] = newCost;
}

void DomainPs::updateParentSet(int var, int a, Set newpset)
{
    parentSets[var][a] = newpset;
}

bool DomainPs::valid(int var, int a) const
{
    return valids[var][a] == -1;
}

bool DomainPs::remove(int var, int a, int level )
{
    assert(level >= 0);
    if (valids[var][a] >= 0)
        return !empty(var);

    valids[var][a] = level;
    if (ddRepr[var])
        ddRepr[var]->pruneParentSet(getParentSet(var, a));

    if (level < nvars) {
        while(level > 0 && trail_sep.size() < static_cast<std::size_t>(level))
            trail_sep.push_back(trail.size());

        trail.push_back({var, a});
    }
    --currentSize[var];
    if (empty(var))
        return true;
    if( lastValid[var] == a ) {
        /*
         *  Have removed last, so find the new last.
         */
        for( int b = a-1; b >= 0; b-- ) {
            if( valid(var, b) ) {
                lastValid[var] = b;
                break;
            }
        }
    }

    assert(!ddRepr[var] || !ddRepr[var]->valueExists(getParentSet(var, a)));

    return false;
}

bool DomainPs::removeSetsIntersecting(int var, Set vars, int level)
{
    prepareCondensedDomain(var);
    for (int a : condensedDomain[var]) {
        Set parent = getParentSet(var, a);
        if (!set_empty(intersection(vars, parent))) {
            remove(var, a, level);
            if (empty(var))
                return true;
        }
    }
    return false;
}

void DomainPs::restore(int level)
{
    // ugly, but better than ??::
    size_t lim = [&]() -> size_t {
        // level 0 always starts at 0
        if (level == 0)
            return 0;
        // if there have been no prunings for this variable at that
        // level, there is nothing to prune
        if (static_cast<size_t>(level) > trail_sep.size())
            return trail.size();
        return trail_sep[level-1];
    }();
    while(!trail.empty() && trail.size() > lim) {
        auto pruning = trail.back();
        int var = pruning.var;
        int a = pruning.val;
        trail.pop_back();
        assert (valids[var][a] >= 0);
        assert(!ddRepr[var] || !ddRepr[var]->valueExists(getParentSet(var, a)));
        valids[var][a] = -1;
        if (ddRepr[var])
            ddRepr[var]->restoreValue(getParentSet(var, a));
        ++currentSize[var];
        if( lastValid[var] < a ) {
            /*
             *  Have restored a value greater than last, so update.
             */
            lastValid[var] = a;
        }
        needsReset[var] = true;
        needsResetPSets[var] = true;
    }
    trail_sep.resize(std::min<size_t>(trail_sep.size(), level));
}

void DomainPs::prepareCondensedDomain(int var) const
{
    auto& d = condensedDomain[var];
    if (needsReset[var]) {
        d.clear();
        for(int a = 0; a <= lastValid[var]; ++a)
            if (valid(var, a))
                d.push_back(a);
        needsReset[var] = false;
    } else if (dirty(var)) {
        auto i = std::remove_if(d.begin(), d.end(), [&](int a) {
            return !valid(var, a); });
        d.erase(i, d.end());
        needsResetPSets[var] = true;
    }
}

void DomainPs::prepareCondensedPSets(int var) const
{
    prepareCondensedDomain(var);
    auto& d = condensedParentSets[var];
    if (needsResetPSets[var]) {
        // we reconstruct from scratch because
        // prepareCondensedDomain() is called much more often and we
        // do not want to slow it down, but we have no mapping from
        // parentset back to value index if condensedDomain and
        // condensedParentSets are not in sync, so there is no easy
        // way to do erase_if(d, ...)
        d.clear();
        for (auto a : condensedDomain[var])
            d.push_back(getParentSet(var, a));
        needsResetPSets[var] = false;
    }
}

bool DomainPs::dirty(int var) const
{
    return condensedDomain[var].size() != static_cast<size_t>(currentSize[var]);
}

int DomainPs::hasSubsetOf(int var, Set s) const
{
    ++stats[var].subsetcalls;

    if (ddRepr[var]) {
        ddRepr[var]->set_node_visit_stat(&stats[var].dt_nodes_visited_subset);
        int rv = ddRepr[var]->getSubsetOf(s, std::nullopt);
        assert(rv < 0 || subset_of(getParentSet(var, rv), s));
        return rv;
    }

    for (int a : asvec(var)) {
        ++stats[var].subsetchecks;
        if (subset_of(getParentSet(var, a), s))
            return a;
    }
    return -1;
}

int DomainPs::has0RCSubsetOf(int var, Set s) const
{
    ++stats[var].subsetcalls;

    if (ddRepr[var]) {
        ddRepr[var]->set_node_visit_stat(&stats[var].dt_nodes_visited_subset);
        int rv = ddRepr[var]->getSubsetOf(s, mask[var]);
        assert(rv < 0 ||
               (subset_of(getParentSet(var, rv), s)
                && getReducedCost(var, rv) == 0));
        return rv;
    }

    for (int a : asvec(var)) {
        ++stats[var].subsetchecks;
        if (subset_of(getParentSet(var, a), s) && getReducedCost(var, a) == 0)
            return a;
    }
    return -1;
}

int DomainPs::minCostSubsetOf(int var, Set s) const
{
    ++stats[var].mincsubsetcalls;

    if (ddRepr[var] && opt.mincostdtrees) {
        ddRepr[var]->set_minc_node_visit_stat(&stats[var].dt_nodes_visited_mincsubset);
        int rv = ddRepr[var]->getMincostSubsetOf(s, std::nullopt);
        assert(rv < 0 ||
               (subset_of(getParentSet(var, rv), s)));
        return rv;
    }

    /*
     *  Find the lowest cost value for variable v that is
     *  consistent with the ordering so far. The domains are
     *  sorted by increasing costs, so once a consistent
     *  parent set is found, it is guaranteed to be of
     *  the lowest cost.
     */
    prepareCondensedPSets(var);
    assert(!dirty(var));
    condensedParentSets[var].push_back(s);
    auto i = condensedParentSets[var].begin();
    while(!subset_of(*i, s))
        ++i;

    ptrdiff_t finalidx = std::distance(condensedParentSets[var].begin(), i);
    stats[var].mincsubsetchecks += 1+finalidx;
    condensedParentSets[var].pop_back();
    if (static_cast<size_t>(finalidx) == condensedParentSets[var].size())
        return -1;
    else
        return condensedDomain[var][finalidx];
}

int DomainPs::minCostSubsetOf_any(int var, Set s) const
{
    ++stats[var].mincsubsetcalls;

    /*
     *  Find the lowest cost value for variable v that is
     *  consistent with the ordering so far. The domains are
     *  sorted by increasing costs, so once a consistent
     *  parent set is found, it is guaranteed to be of
     *  the lowest cost.
     */

    auto psbegin = &(parentSets[var][0]), i = psbegin;
    // for SET64 and SET128, we can implement the subset check using
    // AVX. We perform the subset_of check for 4 sets simultaneoulsy
    // (2 for SET128). For larger set sizes, any use of AVX will have
    // to be in the implementation of subset_of
#if defined(SET64) || defined(SET128)
#ifdef SET64
    constexpr const int skip = 4;
    alignas(32) Set cmp[skip] = { s, s, s, s };
#else
    constexpr const int skip = 2;
    alignas(32) Set cmp[skip] = { s, s };
#endif
    __m256i vec = _mm256_load_si256((__m256i*)cmp);
    while (true) {
        __m256i psi = _mm256_load_si256((__m256i*)i);
        __m256i anded = _mm256_and_si256(vec, psi);
        __m256i eq = _mm256_cmpeq_epi64(anded, psi);
#ifdef SET128
        // AVX does not have cmpeq_epi128, so it may return
        // FF...00.... (or 00...FF....) if half the set matches. This
        // translates to 0xFF in the mask. So we check that we
        // actually have 0xFFFF, which is more complicated than the
        // non-zero check that is sufficient for SET64
        union {
            int r;
            uint16_t w[2];
        } x;
        x.r = _mm256_movemask_epi8(eq);
        if (x.w[0] == 0xFFFF || x.w[1] == 0xFFFF)
            break;
#else
        if (_mm256_movemask_epi8(eq))
            break;
#endif
        i += skip;
    }
    int q = 0;
    for (; !subset_of(i[q], s); ++q) { }
    i += q;
#else
    // the default implementation, for sets 256 bits or larger
    while (!subset_of(*i, s))
        ++i;
#endif

    ptrdiff_t finalidx = std::distance(psbegin, i);
    stats[var].mincsubsetchecks += 1+finalidx;
    // if this is true, we have hit the extra emptySet we added in
    // addVariable(), so no such parent set exists
    if (static_cast<size_t>(finalidx) == sizes[var])
        return -1;
    else
        return finalidx;
}

void DomainPs::resetReducedCosts()
{
    for (int var = 0; var != nvars; ++var) {
        std::fill(delta_costs[var].begin(), delta_costs[var].end(), 0);
        if (ddRepr[var])
            ddRepr[var]->reset_mask(*mask[var]);
    }
}

void DomainPs::verifyDDconsistency(int var)
{
    if (!ddRepr[var])
        return;

    for (std::size_t a = 0; a != sizes[var]; ++a) {
        bool valid = (valids[var][a] < 0);
        bool ddvalid = ddRepr[var]->valueExists(getParentSet(var, a));
        assert(valid == ddvalid);
    }
}
