/*
 *  Routines for static pattern database for lower bound.
 *
 *  Fan, X. and Yuan, C.
 *  An improved lower bound for Bayesian network structure learning.
 *  AAAI 2015.
 */

#include "Instance.h"
#include "Pattern.h"

/*
 *  Powers of 2:
 *      2^10 = 1,024
 *      2^11 = 2,048
 *      2^12 = 4,096
 *      2^13 = 8,192
 *      2^14 = 16,384
 *      2^15 = 32,768
 *      2^16 = 65,536
 *      2^17 = 131,072
 *      2^18 = 262,144
 *      2^19 = 524,288
 *      2^20 = 1,048,576
 *      2^21 = 2,097,152
 *      2^22 = 4,194,304
 *      2^23 = 8,388,608
 *      2^24 = 16,777,216
 *      2^25 = 33,554,432
 *      2^26 = 67,108,864
 *      2^27 = 134,217,728
 *      2^28 = 268,435,456
 */

/*
 *  Constructor for pattern database.
 */
Pattern::Pattern( Instance *instance, int m )
{
    this->instance = instance;
    this->max_m = m;

    unsigned int nDB = ((unsigned int)1 << m);
    db = new Score[nDB]();
    counter = new int[nDB]();
}

/*
 *  Destructor for pattern database.
 */
Pattern::~Pattern()
{
    delete[] db;
    delete[] counter;
}

void
Pattern::printNonZeroQueries(int n)
{
    unsigned int nDB = ((unsigned int)1 << this->max_m);
    printf("%d\n",nDB);

    int nbNodesQueried = 0;
    int nbQueries = 0;

    for( unsigned int i=0; i<nDB; i++){
        if(counter[i]>0){
            nbNodesQueried ++;
            nbQueries += counter[i];
        }
    }

    printf("%d different nodes queried %d times\n",nbNodesQueried,nbQueries);
}

/*
 *  Determine cardinality of a set.
 *  Peter Wegner in CACM 3 (1960).

 */
int
Pattern::card( Set node )
{
#if (defined(BITSET) || defined(DYNSET))
        return node.count();
#else
    Set v = node;
    int c;
    for( c = 0; !set_empty(v); c++ ) {
        v &= v-1; // clear the least significant bit set
    }

    return( c );
#endif
}

int
Pattern::card64( Set64 node )
{
    return count64(node);
}

/*
 *  Lookup subset in the pattern database that consists of all
 *  uninstantiated variables.
 *
 *  A set has to be packed in order to index into the database.
 */
Score
Pattern::lookup( bool *instantiated )
{
    int k = 0;
    Set64 node = emptySet64;
    for( int v = 0; v < instance->nVars(); v++ ) if( element_of( v, partition ) ) {
        if( !instantiated[v] ) {
            add_element64( k, node );
        }
        k++;
    }

    counter[node]++;
    return( db[node] );
}

/*
 *  Lookup subset in the pattern database. All variables in the
 *  partition are uninstantiated.
 *
 *  A set has to be packed in order to index into the database.
 */
Score
Pattern::lookup()
{
    int k = 0;
    Set64 node = emptySet64;
    for( int v = 0; v < instance->nVars(); v++ ) if( element_of( v, partition ) ) {
        add_element64( k, node );
        k++;
    }

    counter[node]++;
    return( db[node] );
}

/*
 *  Fill in the pattern database for the given partition and ancestors.
 *  Determines for all subsets S of partition: min cost ( S | ancestors ).
 *
 *  A set has to be packed in order to index into the database, and
 *  conversely a node in the database has to be unpacked to represent
 *  a set.
 */
Score
Pattern::fill( Set ancestors, Set partition )
{
        this->ancestors = ancestors;
        this->partition = partition;

        int m = card( partition );
        if( m > max_m ) {
                fprintf( stderr, "Partition mismatch (%d vs %d).\n", m, max_m );
                exit( 6 );
        }

        unsigned int nDB = ((unsigned int)1 << m);
        for( Set64 node = 0; node < nDB; node++ ) {
                db[node] = MAX_SCORE;
        }
        db[0] = 0;

        int unpack[instance->nVars()];
        int k = 0;
        for( int v = 0; v < instance->nVars(); v++ ) if( element_of( v, partition ) ) {
                unpack[k] = v;
                k++;
        }

        std::vector<int> partition_vars;
        for( int v = 0; v < instance->nVars(); v++ )
            if( element_of( v, partition ) )
                partition_vars.push_back(v);

        /*
         *  Process all subsets of size 0, 1, ..., m of the partition.
         *      node, successor --- indices into db
         *      uNode, uSuccessor       --- sets of parent variables
         */
        for( int layer = 0; layer < m; layer++ ) {
                for( Set64 node = 0; node < nDB; node++ ) if( card64( node ) == layer ) {
                        Score cost = db[node];
                        /*
                         *  Unpack the node.
                         */
                        Set uNode = emptySet;
                        for( int j = 0; j < m; j++ ) if( element_of64( j, node ) ) {
                                add_element( unpack[j], uNode );
                        }
                        /*
                         *  Generate all successors of the node by adding an
                         *  additional element from the partition.
                         */
                        int k = 0;
                        for (int q = 0; q != m; ++q) {
                                int v = partition_vars[q];
                                if( !element_of64( k, node ) ) {
                                        Set64 successor = node;
                                        add_element64( k, successor );
                                        Set uSuccessor = uNode;
                                        add_element( v, uSuccessor );
                                        /*
                                         *  Find the min cost parent.
                                         */
                                        Set newAncestors = union( ancestors,
                                                        intersection( partition, complement(uSuccessor) ) );
                                        int a = instance->getMinCostParentSet_any( newAncestors, v );
                                        if (a >= 0) {
                                                Score newCost = cost + instance->getCost( v, a );
                                                /*
                                                 *  Update the db database if lesser cost.
                                                 */
                                                if( db[successor] > newCost ) {
                                                        db[successor] = newCost;
                                                }
                                        }
                                }
                                k++;
                        }
                }
        }

        return( db[nDB-1] );
}

