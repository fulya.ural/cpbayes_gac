/*
 *  Routines for enforcing the constraint that the parent
 *  variables must form a directed acyclic graph.
 */

#include "Instance.h"
#include "LocalSearch.h"
#include "ClusterCut.h"
#include "ZDD.h"
#include "DAG.h"
#include "Timer.h"
#include <cassert>
#include <algorithm>
#include <fstream>
#include <inttypes.h>

using std::nullopt;

/*
data/Scores/flag_BDe.txt: backtracking solution, time = 1.221
        ordering[0] = 27 <--{ } depth = 0
        ordering[1] = 11 <--{ 27 }      depth = 1
        ordering[2] = 20 <--{ 27 }      depth = 1
        ordering[3] = 0 <--{ 11 }       depth = 2
        ordering[4] = 13 <--{ 20 27 }   depth = 2
        ordering[5] = 3 <--{ 0 }        depth = 3
        ordering[6] = 21 <--{ 0 }       depth = 3
        ordering[7] = 23 <--{ 0 20 }    depth = 3
        ordering[8] = 2 <--{ 3 }        depth = 4

        ordering[9] = 14 <--{ 11 23 }   depth = 4
        ordering[10] = 4 <--{ 0 14 }    depth = 5
        ordering[11] = 17 <--{ 13 14 23 }       depth = 5
        ordering[12] = 6 <--{ 17 20 23 }        depth = 6
        ordering[13] = 16 <--{ 6 27 }   depth = 7
        ordering[14] = 19 <--{ 2 16 17 20 27 }  depth = 8
        ordering[15] = 22 <--{ 0 16 21 }        depth = 8
        ordering[16] = 7 <--{ 19 20 }   depth = 9
        ordering[17] = 10 <--{ 19 27 }  depth = 9
        ordering[18] = 1 <--{ 0 10 }    depth = 10

        ordering[19] = 12 <--{ 0 10 13 }        depth = 10
        ordering[20] = 15 <--{ 10 16 }  depth = 10
        ordering[21] = 18 <--{ 10 11 19 }       depth = 10
        ordering[22] = 5 <--{ 18 }      depth = 11
        ordering[23] = 25 <--{ 12 15 }  depth = 11
        ordering[24] = 28 <--{ 16 18 }  depth = 11
        ordering[25] = 9 <--{ 10 27 28 }        depth = 12
        ordering[26] = 24 <--{ 4 12 21 25 }     depth = 12
        ordering[27] = 8 <--{ 9 10 11 12 13 14 15 }     depth = 13
        ordering[28] = 26 <--{ 8 12 24 }        depth = 14
        minimum cost = 2693.08864
*/
/*static const int debug_solutionO[] = {
  27, 11, 20,  0, 13,  3, 21, 23,  2,
  14,  4, 17,  6, 16, 19, 22,  7, 10,  1,
  12, 15, 18,  5, 25, 28,  9, 24,  8, 26};*/

/*
data/Scores/flag_BIC.txt: backtracking solution, time = 0.612
        ordering[0] = 27 <--{ } depth = 0
        ordering[1] = 11 <--{ 27 }      depth = 1
        ordering[2] = 20 <--{ 27 }      depth = 1
        ordering[3] = 6 <--{ 20 }       depth = 2
        ordering[4] = 16 <--{ 6 27 }    depth = 3
        ordering[5] = 2 <--{ 16 }       depth = 4
        ordering[6] = 3 <--{ 2 }        depth = 5
        ordering[7] = 19 <--{ 2 20 }    depth = 5
        ordering[8] = 0 <--{ 3 11 }     depth = 6

        ordering[9] = 7 <--{ 19 }       depth = 6
        ordering[10] = 10 <--{ 19 27 }  depth = 6
        ordering[11] = 1 <--{ 0 10 }    depth = 7
        ordering[12] = 8 <--{ 10 11 }   depth = 7
        ordering[13] = 18 <--{ 10 19 }  depth = 7
        ordering[14] = 21 <--{ 0 }      depth = 7
        ordering[15] = 22 <--{ 0 }      depth = 7
        ordering[16] = 5 <--{ 18 }      depth = 8
        ordering[17] = 14 <--{ 8 11 }   depth = 8
        ordering[18] = 25 <--{ 0 8 }    depth = 8

        ordering[19] = 28 <--{ 16 18 }  depth = 8
        ordering[20] = 4 <--{ 0 14 }    depth = 9
        ordering[21] = 9 <--{ 8 28 }    depth = 9
        ordering[22] = 15 <--{ 16 25 }  depth = 9
        ordering[23] = 23 <--{ 14 }     depth = 9
        ordering[24] = 24 <--{ 11 25 }  depth = 9
        ordering[25] = 12 <--{ 4 24 }   depth = 10
        ordering[26] = 17 <--{ 6 23 }   depth = 10
        ordering[27] = 26 <--{ 8 24 }   depth = 10
        ordering[28] = 13 <--{ 11 12 }  depth = 11

*/
/*static const int debug_solutionO[] = {
    27, 11, 20,  6, 16,  2,  3, 19,  0,
     7, 10,  1,  8, 18, 21, 22,  5, 14, 25,
    28,  4,  9, 15, 23, 24, 12, 17, 26, 13};*/

/*
Scores-BLIP/kdd.ts.jkl:
        ordering[0] = 5 <--{ }  depth = 0
        ordering[1] = 18 <--{ 5 }       depth = 1
        ordering[2] = 23 <--{ 5 18 }    depth = 2
        ordering[3] = 28 <--{ 5 18 }    depth = 2
        ordering[4] = 15 <--{ 18 23 28 }        depth = 3
        ordering[5] = 22 <--{ 15 18 }   depth = 4
        ordering[6] = 16 <--{ 18 22 23 28 }     depth = 5
        ordering[7] = 25 <--{ 16 18 22 28 }     depth = 6
        ordering[8] = 24 <--{ 15 16 22 25 }     depth = 7
        ordering[9] = 32 <--{ 15 18 22 24 }     depth = 8
        ordering[10] = 27 <--{ 15 22 28 32 }    depth = 9
        ordering[11] = 19 <--{ 16 22 25 27 28 } depth = 10
        ordering[12] = 17 <--{ 16 19 22 24 32 } depth = 11
        ordering[13] = 20 <--{ 16 19 22 24 27 } depth = 11
        ordering[14] = 29 <--{ 15 16 17 28 }    depth = 12
        ordering[15] = 21 <--{ 20 22 24 27 29 } depth = 13
        ordering[16] = 12 <--{ 15 16 19 21 28 } depth = 14
        ordering[17] = 31 <--{ 20 21 22 24 29 } depth = 14
        ordering[18] = 34 <--{ 17 18 21 24 }    depth = 14
        ordering[19] = 33 <--{ 21 23 27 28 31 } depth = 15
        ordering[20] = 39 <--{ 15 18 27 34 }    depth = 15
        ordering[21] = 14 <--{ 12 17 19 22 33 } depth = 16
        ordering[22] = 26 <--{ 16 17 25 29 33 } depth = 16
        ordering[23] = 2 <--{ 5 14 33 } depth = 17
        ordering[24] = 13 <--{ 15 16 19 26 }    depth = 17
        ordering[25] = 45 <--{ 14 18 24 39 }    depth = 17
        ordering[26] = 4 <--{ 2 5 33 }  depth = 18
        ordering[27] = 1 <--{ 2 4 5 14 15 }     depth = 19
        ordering[28] = 6 <--{ 1 4 5 33 }        depth = 20
        ordering[29] = 11 <--{ 1 2 4 14 }       depth = 20
        ordering[30] = 3 <--{ 1 4 11 33 }       depth = 21
        ordering[31] = 0 <--{ 3 11 12 14 17 }   depth = 22
        ordering[32] = 7 <--{ 2 3 6 15 33 }     depth = 22
        ordering[33] = 30 <--{ 3 18 21 22 29 }  depth = 22
        ordering[34] = 8 <--{ 0 3 4 33 }        depth = 23
        ordering[35] = 10 <--{ 0 2 11 12 28 }   depth = 23
        ordering[36] = 9 <--{ 0 5 10 22 25 }    depth = 24
        ordering[37] = 43 <--{ 8 18 }   depth = 24
        ordering[38] = 60 <--{ 10 12 19 24 }    depth = 24
        ordering[39] = 50 <--{ 17 28 60 }       depth = 25
        ordering[40] = 58 <--{ 16 17 34 50 60 } depth = 26
        ordering[41] = 42 <--{ 8 18 43 58 }     depth = 27
        ordering[42] = 59 <--{ 17 18 39 58 }    depth = 27
        ordering[43] = 62 <--{ 16 30 50 58 60 } depth = 27
        ordering[44] = 48 <--{ 12 50 58 60 62 } depth = 28
        ordering[45] = 52 <--{ 48 50 58 60 62 } depth = 29
        ordering[46] = 56 <--{ 12 14 48 58 60 } depth = 29
        ordering[47] = 54 <--{ 48 50 56 58 60 } depth = 30
        ordering[48] = 55 <--{ 30 34 54 59 62 } depth = 31
        ordering[49] = 37 <--{ 2 34 55 60 }     depth = 32
        ordering[50] = 35 <--{ 12 34 37 60 62 } depth = 33
        ordering[51] = 36 <--{ 9 18 34 37 55 }  depth = 33
        ordering[52] = 40 <--{ 8 36 39 42 }     depth = 34
        ordering[53] = 41 <--{ 10 18 40 }       depth = 35
        ordering[54] = 44 <--{ 19 40 42 45 54 } depth = 35
        ordering[55] = 38 <--{ 18 30 39 42 44 } depth = 36
        ordering[56] = 46 <--{ 26 38 44 50 }    depth = 37
        ordering[57] = 47 <--{ 23 25 29 46 }    depth = 38
        ordering[58] = 49 <--{ 46 48 60 }       depth = 38
        ordering[59] = 51 <--{ 47 48 50 }       depth = 39
        ordering[60] = 63 <--{ 35 49 54 55 60 } depth = 39
        ordering[61] = 61 <--{ 28 35 60 62 63 } depth = 40
        ordering[62] = 57 <--{ 54 56 59 61 63 } depth = 41
        ordering[63] = 53 <--{ 35 52 57 58 59 } depth = 42
        minimum cost = 367065.62048
*/
/*static const int debug_solutionO[] = {
                5,18,28,23,15,22,16,25,24,32,27,19,17,29,20,21,31,33,12,14,2,4,1,6,34,11,3,0,
                8,10,60,26,39,45,30,50,58,62,59,48,9,56,54,13,43,42,55,37,52,35,7,36,40,44,
                38,41,46,49,63,61,47,51,57,53};*/

/*
 * Data/Scores/steel_BDe.jkl
        ordering[0] = 5 <--{ }  depth = 0
        ordering[1] = 17 <--{ 5 }       depth = 1
        ordering[2] = 19 <--{ 17 }      depth = 2
        ordering[3] = 20 <--{ 5 17 19 } depth = 3
        ordering[4] = 7 <--{ 5 17 20 }  depth = 4
        ordering[5] = 6 <--{ 5 7 20 }   depth = 5
        ordering[6] = 4 <--{ 6 7 }      depth = 6
        ordering[7] = 16 <--{ 6 17 19 20 }      depth = 6
        ordering[8] = 18 <--{ 6 16 19 20 }      depth = 7
        ordering[9] = 24 <--{ 5 16 20 } depth = 7
        ordering[10] = 9 <--{ 7 16 24 } depth = 8
        ordering[11] = 15 <--{ 5 18 19 }        depth = 8
        ordering[12] = 21 <--{ 5 6 17 18 }      depth = 8
        ordering[13] = 8 <--{ 4 9 15 21 }       depth = 9
        ordering[14] = 26 <--{ 15 18 21 }       depth = 9
        ordering[15] = 22 <--{ 5 16 20 21 26 }  depth = 10
        ordering[16] = 23 <--{ 4 16 21 24 26 }  depth = 10
        ordering[17] = 25 <--{ 6 8 9 16 }       depth = 10
        ordering[18] = 12 <--{ 7 23 25 }        depth = 11
        ordering[19] = 11 <--{ 12 }     depth = 12
        ordering[20] = 27 <--{ 7 11 16 20 }     depth = 13
        ordering[21] = 13 <--{ 11 16 19 27 }    depth = 14
        ordering[22] = 0 <--{ 12 13 17 27 }     depth = 15
        ordering[23] = 1 <--{ 0 5 12 18 }       depth = 16
        ordering[24] = 10 <--{ 1 12 13 27 }     depth = 17
        ordering[25] = 14 <--{ 0 1 7 24 }       depth = 17
        ordering[26] = 3 <--{ 10 11 13 27 }     depth = 18
        ordering[27] = 2 <--{ 3 }       depth = 19
        minimum cost = 18633.23008
*/
//static const int debug_solutionO[] = {5,17,19,20,7,6,16,18,24,21,15,26,9,4,8,25,23,12,27,13,0,22,1,14,11,10,3,2};

/*
Data/Scores/alarm1000_BIC.txt: upper bound solution, time = 0.055
        ordering[0] = 4 <--{ }  depth = 0
        ordering[1] = 6 <--{ }  depth = 0
        ordering[2] = 14 <--{ } depth = 0
        ordering[3] = 17 <--{ } depth = 0
        ordering[4] = 22 <--{ } depth = 0
        ordering[5] = 34 <--{ } depth = 0
        ordering[6] = 5 <--{ 4 }        depth = 1
        ordering[7] = 16 <--{ 17 }      depth = 1
        ordering[8] = 23 <--{ 34 }      depth = 1
        ordering[9] = 7 <--{ 5 6 }      depth = 2
        ordering[10] = 24 <--{ 22 23 }  depth = 2
        ordering[11] = 26 <--{ 22 23 }  depth = 2
        ordering[12] = 25 <--{ 24 }     depth = 3
        ordering[13] = 31 <--{ 7 }      depth = 3
        ordering[14] = 36 <--{ 24 }     depth = 3
        ordering[15] = 2 <--{ 31 }      depth = 4
        ordering[16] = 3 <--{ 31 }      depth = 4
        ordering[17] = 8 <--{ 3 7 }     depth = 5
        ordering[18] = 10 <--{ 3 8 }    depth = 6
        ordering[19] = 33 <--{ 3 8 }    depth = 6
        ordering[20] = 12 <--{ 10 }     depth = 7
        ordering[21] = 15 <--{ 10 }     depth = 7
        ordering[22] = 11 <--{ 12 }     depth = 8
        ordering[23] = 18 <--{ 15 17 }  depth = 8
        ordering[24] = 29 <--{ 8 15 }   depth = 8
        ordering[25] = 19 <--{ 18 }     depth = 9
        ordering[26] = 27 <--{ 19 26 }  depth = 10
        ordering[27] = 32 <--{ 19 }     depth = 10
        ordering[28] = 28 <--{ 17 27 }  depth = 11
        ordering[29] = 30 <--{ 32 }     depth = 11
        ordering[30] = 0 <--{ 30 }      depth = 12
        ordering[31] = 1 <--{ 0 }       depth = 13
        ordering[32] = 9 <--{ 0 3 }     depth = 13
        ordering[33] = 13 <--{ 9 12 }   depth = 14
        ordering[34] = 20 <--{ 9 }      depth = 14
        ordering[35] = 21 <--{ 19 20 }  depth = 15
        ordering[36] = 35 <--{ 19 20 }  depth = 15
        minimum cost = 11783.04640
*/
//static const int debug_solutionO[] = {4,5,6,7,31,3,8,10,12,34,15,17,18,23,22,24,33,19,26,32,30,27,0,1,9,11,20,25,35,13,2,28,21,36,16,14,29};

/*
Data/Scores/bands_BIC.txt: backtracking solution 5117.94931, time = 2.968, level = 23 (LP primal)
        ordering[0] = 4 <--{ }  depth = 0
        ordering[1] = 6 <--{ }  depth = 0
        ordering[2] = 7 <--{ }  depth = 0
        ordering[3] = 10 <--{ } depth = 0
        ordering[4] = 15 <--{ } depth = 0
        ordering[5] = 31 <--{ } depth = 0
        ordering[6] = 33 <--{ } depth = 0
        ordering[7] = 1 <--{ 4 }        depth = 1
        ordering[8] = 24 <--{ 1 }       depth = 2
        ordering[9] = 30 <--{ 24 }      depth = 3
        ordering[10] = 34 <--{ 30 }     depth = 4
        ordering[11] = 3 <--{ 1 34 }    depth = 5
        ordering[12] = 8 <--{ 3 34 }    depth = 6
        ordering[13] = 5 <--{ 1 8 }     depth = 7
        ordering[14] = 13 <--{ 1 8 }    depth = 7
        ordering[15] = 9 <--{ 3 13 }    depth = 8
        ordering[16] = 14 <--{ 3 13 24 }        depth = 8
        ordering[17] = 16 <--{ 3 13 }   depth = 8
        ordering[18] = 11 <--{ 9 }      depth = 9
        ordering[19] = 17 <--{ 8 9 }    depth = 9
        ordering[20] = 25 <--{ 13 16 }  depth = 9
        ordering[21] = 26 <--{ 9 }      depth = 9
        ordering[22] = 38 <--{ 9 13 }   depth = 9
        ordering[23] = 32 <--{ 17 }     depth = 10
        ordering[24] = 37 <--{ 1 38 }   depth = 10
        ordering[25] = 36 <--{ 26 37 }  depth = 11
        ordering[26] = 2 <--{ 1 36 }    depth = 12
        ordering[27] = 29 <--{ 2 26 }   depth = 13
        ordering[28] = 27 <--{ 2 29 }   depth = 14
        ordering[29] = 28 <--{ 26 29 }  depth = 14
        ordering[30] = 18 <--{ 2 27 }   depth = 15
        ordering[31] = 20 <--{ 18 36 }  depth = 16
        ordering[32] = 22 <--{ 18 }     depth = 16
        ordering[33] = 21 <--{ 20 }     depth = 17
        ordering[34] = 23 <--{ 22 27 }  depth = 17
        ordering[35] = 35 <--{ 2 18 20 }        depth = 17
        ordering[36] = 0 <--{ 2 35 }    depth = 18
        ordering[37] = 19 <--{ 3 35 }   depth = 18
        ordering[38] = 12 <--{ 0 3 }    depth = 19
        minimum cost = 5117.94931

 */
static const int debug_solutionO[] = {4,6,7,10,15,31,33,1,24,30,34,3,8,5,13,9,14,16,11,17,25,26,38,32,37,36,2,29,27,28,18,20,22,21,23,35,0,19,12};

/*
 *  Constructor for DAG.
 */
DAG::DAG( Instance *instance, LocalSearch *localSearch, Timer *timer,
          const Options& options)
    : instance(instance)
    , localSearch(localSearch)
    , timer(timer)
    , opt(options)
{
    for( int i = 0; i < instance->nVars(); i++ ) {
        deltaCostMax.push_back( 0 );
        firstValidValueP.push_back( 0 );
        lastModifiedValueP.push_back( 0 );
    }
    clusterFailed = false;

    lastClusterCutID = 1;
}

Score DAG::updateBestOrdering( int *bestSolution, int *bestOrdering )
{
//    int depth[instance->nVars()];
//    for( int i = 0; i < instance->nVars(); i++ ) {
//            bestParents[i] = supportIndex[i];
//            bestOrdering[i] = supportOrdering[i];
//            depth[i] = instance->getDepthParentSetUnordered( &supportOrdering[0], &depth[0], i, supportOrdering[i], supportIndex[supportOrdering[i]]);
//            supportDepth[supportOrdering[i]] = depth[i];
//    }
//    std::sort(bestOrdering, bestOrdering+instance->nVars(), [&](int a, int b) {
//            return std::tie(supportDepth[a], a) < std::tie(supportDepth[b], b);
//        });
        for( int i = 0; i < instance->nVars(); i++ ) {
                bestOrdering[i] = supportOrdering[i];
        }

    /*
     *  Determine best solution for ordering.
     */
    Score ub = instance->minCostOrdering( emptySet,
        bestOrdering,
        instance->nVars(),
        bestSolution );

    /*
     *  Sort ordering by depth and return the min cost.
     *  Sorted ordering guaranteed to have a min cost that
     *  is less than or equal to that of the original ordering.
     */
    localSearch->sortByDepth( bestOrdering, bestSolution );

    /*
     *  Determine cost and best solution for sorted ordering.
     */
    ub = instance->minCostOrdering( emptySet,
        bestOrdering,
        instance->nVars(),
        bestSolution );

    return( ub );

}

/*
 *  Destructor for DAG.
 */
DAG::~DAG()
{
}

/*
 *  Propagate the acyclic constraint over the parent variables.
 */
bool
DAG::propagate(
        int *currentParents, int *currentOrdering, int *currentDepth,
        int currentLevel, bool *instantiatedP, int v,
        Score currentLB, Score *currentUB, int **bestParents, int **bestOrdering,
        bool *restart )
{
#ifndef NDEBUG
    Score replLB0{0};
    for( int w = 0; w < instance->nVars(); w++ ) {
        if (instantiatedP[w]) {
            int a = currentParents[w];
            Score minCost = instance->getCost(w, a);
            replLB0 += minCost;
            continue;
        }
        for (int a : instance->getDomainP(w)) {
            Score minCost = instance->getCost(w, a);
            replLB0 += minCost;
            break;
        }
    }
    assert(replLB0 == currentLB);
#endif

    clusterFailed = false;
    bool success;
    success = propagateNecessaryEdges( currentLevel, instantiatedP, v );
    if( !success ) return( false );

    // GAC++
    success = isAcyclic( currentOrdering, currentLevel, currentParents,
                         instantiatedP, v );
    if( !success ) return( false );
    assert(supportCost >= currentLB);
    if (opt.GAC && opt.gacsupport) {
        if (supportCost < *currentUB) {
            *currentUB = updateBestOrdering(*bestParents, *bestOrdering);
            printf(FGREEN "%s: backtracking solution %0.*f, time = %0.3f "
                   "level = %d (GAC primal)\n" FNONE,
                   instance->benchmarkName(), precision,
                   (double)supportCost / scale,
                   timer->elapsedTime(), currentLevel );
            if (opt.printsolutions)
                instance->printSolution(
                    *bestOrdering, *bestParents, *currentUB);
            if (opt.restarts) {
                *restart = true;
                return( false );
            }
        }
    }

    // For each variable, find the first valid value (we know it has
    // the minimum cost) and increase the deltaCost of all its values
    // by that cost.
    //
    // The lower bound implied by this may be higher than currentLB,
    // because we have pruned some values in propagateNecessaryEdges
    // and isAcyclic, so we update currentLB at the end of this.
    Score replLB{0};
    instance->getDomainPs().resetReducedCosts();
    for( int w = 0; w < instance->nVars(); w++ ) {
        auto& dom = instance->getDomainP(w);
        if (instantiatedP[w]) {
            int a = currentParents[w];
            Score minCost = instance->getCost(w, a);
            dom.incReducedCost(a, minCost);
            deltaCostMax[w] = minCost;
            replLB += minCost;
            continue;
        }
        assert(dom.size() > 0);
        int mina = *(dom.begin());
        firstValidValueP[w] = mina;
        Score minCost = instance->getCost(w, mina);
        deltaCostMax[w] = minCost;
        replLB += minCost;
        for (int a : dom) {
            dom.incReducedCost(a, minCost);
            if (instance->getCost(w, a) == minCost)
                lastModifiedValueP[w] = a;
            assert(lastModifiedValueP[w] >= firstValidValueP[w]);
        }
    }
    assert(replLB >= currentLB);
    currentLB = replLB;

    if (opt.RCclusters)
        success = propagateClusters( currentParents, currentOrdering, currentLevel, instantiatedP,
                                     v, currentLB, bestParents, bestOrdering, currentUB,
                                     restart);
    else
        success = true;

    if( !success ) {
        clusterFailed = true;
        return( false );
    }

    /*
     *  Prune low cost parent sets based on acyclic constraint.
     *  Does not seem to improve performance under various parameter settings.
     */
/*
    for( int i = 0; i < instance->nVars(); i++ ) {
        if( !instantiatedP[i] && (instance->getCurrentDomainSizeP( i ) < 5) ) {
            int nTest = 0;
            for( int a = 0; a <= instance->getLastValidP( i ); a++ ) {
                if( instance->validP( i, a ) ) {
                    if( !isAcyclic( currentOrdering, currentLevel, i, a ) ) {
                        //printf("(l) prune %d from domain of parent variable %d\n",a,i);
                        instance->setValidP( i, a, currentLevel );
                        checkingP[v][i] = true;
                        if( instance->getCurrentDomainSizeP( i ) == 0 ) {
                            return( false );
                        }
                    }
                    nTest++;
                    if( nTest >= 3 ) break;
                }
            }
        }
    }
*/

    return( true );
}


/*
 *  Pruning rule based on necessary edges.
 *
 *  If a directed edge i --> j is a necessary edge,
 *  can remove any parent set that contains j --> i
 *  from the domain of (the uninstantiated variable) i.
 *  An edge i --> j is necessary if i occurs in every
 *  currently valid parent set for variable j.
 *
 *  Determining something more general---reachability in the graph
 *  of necessary edges---is not worthwhile as the difference in
 *  pruning was negligible and as pruning progresses new edges
 *  become necessary (so there is the expense of maintaining
 *  the graph of necessary edges).
 */
bool
DAG::propagateNecessaryEdges(
        int currentLevel, bool *instantiatedP, int v )
{
    instance->findNecessaryParents();
    for( int i = 0; i < instance->nVars(); i++ ) if( !instantiatedP[i] ) {
        /*
         *  Set necessary is the set of all j such that i --> j is a
         *  necessary edge.
         */
        Set necessary = instance->necessaryEdges( i );
        if( set_empty( necessary ) ) continue;
        for (int a : instance->getDomainP(i)) {
            /*
             *  Remove the parent set a from domain of i if the
             *  intersection of necessary and a is not empty.
             */
            Set parent = instance->getParentSet( i, a );
            if( !set_empty( intersection( necessary, parent ) ) ) {
                instance->setValidP( i, a, currentLevel );
                instance->findNecessaryParents( i );
                if( instance->getCurrentDomainSizeP( i ) == 0 ) {
                    return( false );
                }
            }
        }
    }

    return( true );
}

void
DAG::saveSeenSubsequences(int minSubSeqSize){

    Set subseq = emptySet;
    for(int i = 0; i < minSubSeqSize; i++){
        add_element(seenOrdering[i], subseq);
    }

    auto subseqSupport = std::make_shared<cache_element>(instance->nVars());
    std::fill(subseqSupport->begin(), subseqSupport->end(), -1);

    for(int i = 0; i < instance->nVars(); i++){
        int v = seenOrdering[i];
        assert(seenOrderingIndexOf[v] == i);
        assert(v >= 0);
        assert(v < instance->nVars());
        (*subseqSupport)[v] = seenOrderingSupportOf[v];
    }

    for(int subseqSize = minSubSeqSize; subseqSize < instance->nVars(); subseqSize++) {
        if (subseqSize > 0)
            add_element(seenOrdering[subseqSize-1], subseq);

        seenSubsequences.insert_or_assign(subseq, subseqSupport);
    }
}

template<typename P>
bool DAG::subSeqIsValid(Set *subseq, Set *cluster, int *k, bool *instantiatedP, P pred){

    bool subseqIsValid = true;

    const auto& subSeqSupport = *seenSubsequences[*subseq];
    for( int w = 0; w < instance->nVars(); w++ ) if( !element_of( w, *subseq ) ) {
        assert(!instantiatedP[w]);
        int a = subSeqSupport[w];
        assert(a > -1);
        if(instance->validP(w,a) && subset_of(instance->getParentSet(w,a), *subseq) && pred(w, a)){
            add_element( w, *subseq );
            remove_element( w, *cluster );
            seenOrdering[*k] = w; // new
            seenOrderingIndexOf[w] = *k; // new
            seenOrderingSupportOf[w] = a; // new
            (*k)++;
            continue;
        }
        subseqIsValid = false;
        break;
    }

    return (subseqIsValid);
}

/*
 *  Satisfiability test.
 *
 *  For every subset C of the variables, there must exist a
 *  v in C such that some of v's parents are not in C.
 */
Set DAG::orderVariables_0rc(int* currentParents, int* currentOrdering,
    int currentLevel, bool* instantiatedP, int currentOrder)
{
    seenOrdering.clear();
    seenOrderingIndexOf.clear();
    seenOrderingSupportOf.clear();
    seenOrdering.resize(instance->nVars());
    seenOrderingIndexOf.resize(instance->nVars());
    seenOrderingSupportOf.resize(instance->nVars());

    if (opt.gacsupport) {
        support.clear();
        supportIndex.clear();
        supportDepth.clear();
        supportOrdering.clear();
        support.resize(instance->nVars());
        supportIndex.resize(instance->nVars());
        supportDepth.resize(instance->nVars());
        supportOrdering.resize(instance->nVars());
        supportCost = 0;
    }

    Set inOrdering = emptySet;

    for( int i = 0; i < currentLevel + ((currentOrder < instance->nVars())?1:0) ; i++ ){
        int parentVar = currentOrdering[i];
        add_element( parentVar, inOrdering );
        seenOrdering[i] = parentVar;
        seenOrderingIndexOf[parentVar] = i;
        seenOrderingSupportOf[parentVar] = currentParents[parentVar];
        if (opt.gacsupport) {
            supportCost
                += instance->getCost(parentVar, currentParents[parentVar]);
            supportIndex[parentVar] = currentParents[parentVar];
            supportOrdering[i] = parentVar;
        }
    }

    Set cluster = emptySet;
    for (int i = 0; i != instance->nVars(); ++i) {
        if (!element_of(i, inOrdering))
            add_element(i, cluster);
    }

    int k = currentLevel + ((currentOrder < instance->nVars()) ? 1 : 0);

    assert(set_empty(intersection(cluster, inOrdering)));

    bool changes;
    do {
        changes = false;
        for( int w = 0; w < instance->nVars(); w++ ) if( !element_of( w, inOrdering ) ) {
            ++nbDomainIterations;
            const auto& wdom = instance->getDomainP(w);
            int a = wdom.has0RCSubsetOf(inOrdering);
            if (a >= 0) {
                Set parent = wdom.getParentSet(a);
                add_element( w, inOrdering );
                //printf("add %d to inOrdering in orderVariables\n", w);
                remove_element( w, cluster );
                seenOrdering[k] = w; // new
                seenOrderingIndexOf[w] = k; // new
                seenOrderingSupportOf[w] = a; // new
                if (opt.gacsupport) {
                    support[w] = parent;
                    supportIndex[w] = a;
                    supportOrdering[k] = w;
                    supportCost += instance->getCost(w, a);
                }
                k++;
                if(k == instance->nVars())
                    break;
                changes = true;
            }
        }
    } while( changes );

    //printf("EXIT orderVariables\n");
    return cluster;
}

bool DAG::isClusterValue(int v, int a, Set clustervars, const bool *instantiatedP, const int *currentParents) const
{
    Set parent = instance->getParentSet(v, a);
    if (!set_empty( intersection(parent, clustervars)))
        return false;
    assert(!instance->validP(v, a) ||
           getCost(v, a, instantiatedP, currentParents) > 0);
    return true;
}

bool DAG::isSatisfyingValue(int v, int a, Set clustervars, const bool *instantiatedP, const int *currentParents) const
{
    assert( set_empty(intersection(instance->getParentSet(v, a), clustervars)) );
    return (getCost(v, a, instantiatedP, currentParents) == 0);
}

Score DAG::getCost(int v, int a, const bool *instantiatedP, const int *currentParents) const
{
    if (instantiatedP[v]) {
        if (a == currentParents[v])
            return 0;
        else
            return MAX_SCORE;
    }
    if (!instance->validP(v, a))
        return MAX_SCORE;
    else
        return instance->getDomainP(v).getReducedCost(a);
}

Set DAG::minimizeCluster(Set cluster, bool *instantiatedP, int *currentParents)
{
        Set N = emptySet;
        // cluster <=> C in the pseudo code
        Set N_union_C; // need this to give N \union C to ACYC-CHECKER

        bool changes = false;

        while(!set_empty(cluster)){
            int c;
            for( c = 0; c < instance->nVars(); c++ ) if( element_of( c, cluster ) ) {
                break;
            } // line 4

            remove_element(c, cluster); // line 5

            N_union_C = union(N, cluster);

            // Now, run ACYC-CHECKER on N_union_C

            seenOrdering.clear();
            seenOrderingIndexOf.clear();
            seenOrderingSupportOf.clear();
            seenOrdering.resize(instance->nVars());
            seenOrderingIndexOf.resize(instance->nVars());
            seenOrderingSupportOf.resize(instance->nVars());
            Set inOrdering = emptySet;
            for( int i = 0; i < instance->nVars(); i++ ) if( !element_of( i, N_union_C ) ) {
                add_element(i, inOrdering);
            }
            //inOrdering = complement(N_union_C);
            int k = 0;
            for( int i = 0; i < instance->nVars(); i++ ) if( element_of( i, inOrdering ) ) {
                seenOrdering[k] = i;
                seenOrderingIndexOf[i] = k;
                k++;
            }

            /*do {
                changes = false;
                for( int w = 0; w < instance->nVars(); w++ ) if(!element_of( w, inOrdering ) ) {
                    ++nbDomainIterations;
                    for(int a : instance->getDomainP(w)) {
                        if( getCost(w,a, instantiatedP, currentParents) == 0 ) {
                            Set parent = instance->getParentSet( w, a );
                            if( subset_of( parent, inOrdering ) ) {

                                add_element( w, inOrdering );
                                //printf("add %d to inOrdering in minimizeCluster\n", w);
                                remove_element(w, N_union_C);
                                seenOrdering[k] = w; // new
                                seenOrderingIndexOf[w] = k; // new
                                seenOrderingSupportOf[w] = a; // new
                                k++; // new
                                if(k == instance->nVars())
                                    break;
                                changes = true;

                                break;
                            }
                        }
                    }
                }
            } while( changes );*/

            do {
                changes = false;
                for( int w = 0; w < instance->nVars(); w++ ) if(!element_of( w, inOrdering ) ) {
                    ++nbDomainIterations;
                    const auto& wdom = instance->getDomainP(w);
                    int a = wdom.has0RCSubsetOf(inOrdering);
                    if(a >= 0) {
                        add_element( w, inOrdering );
                        //printf("add %d to inOrdering in minimizeCluster\n", w);
                        remove_element( w, N_union_C );
                        k++;
                        if(k == instance->nVars())
                            break;
                        changes = true;
                        break;
                    }
                }
            } while( changes );

            // Note that at this point, after all the modifs, N_union_C = ACYC-CHECKER(N_union_C)

            if(set_empty(N_union_C)){
                assert(k == instance->nVars());
                add_element(c, N);
            }
            else
            {
                cluster = union(emptySet, N_union_C);
                for( int w = 0; w < instance->nVars(); w++ ) if( element_of( w, N ) ) {
                    remove_element(w, cluster);
                }
            }
        }
        // Set cluster to N so that from now on you use the minimized cluster
        cluster = union(emptySet, N);
        return cluster;
}

Score DAG::clusterCost(const ClusterCut& cut, int *currentParents, bool *instantiatedP) const
{
        long long totaldomains = 0;
        cut.incChecked();
        ++nbclusterpoolchecked;
        Set clusterVars = cut.getVariables();
        assert(cut.getSupportVar() != -1 && cut.getSupportVal() != -1 && set_empty(intersection(instance->getParentSet(cut.getSupportVar(), cut.getSupportVal()), clusterVars)));
        Score minCost = getCost(cut.getSupportVar(), cut.getSupportVal(), instantiatedP, currentParents);
        if (minCost == 0) {
                ++nbclusterpoolfastzero;
                cut.incZero();
                return 0;
        }
        // check whether the value saved in currentParents is in the
        // cluster constraint, in which case it has reduced cost 0, so
        // the min reduced cost of the constraint is 0, hence early
        // return
        for( int w = 0; w < instance->nVars(); w++ ) if( element_of( w, clusterVars ) ) {
                totaldomains += instance->getCurrentDomainSizeP(w);
                if (instantiatedP[w]) {
                        if( set_empty(intersection(instance->getParentSet(w, currentParents[w]), clusterVars)) ) {
                                ++nbclusterpoolfastzero;
                                cut.incZero();
                                return 0;
                        }
                }
        }
        // check whether the DD of values with 0 reduced cost has a
        // value in the constraint, in which case we early return 0
        if (opt.dtrees != dtree_use::NONE) {
            Set nonclusterVars = complement(clusterVars);
            for (int w = 0; w != instance->nVars(); ++w) {
                if (!element_of(w, clusterVars))
                    continue;
                int a = instance->getDomainP(w).has0RCSubsetOf(nonclusterVars);
                if (a >= 0) {
                    cut.setSupportVar(w);
                    cut.setSupportVal(a);
                    return 0;
                }
            }
        }

        // now actually traverse the domains to find the (non-zero)
        // min reduced cost of the constraint
        nbparentsetvalueactive += totaldomains;
        for( int w = 0; w < instance->nVars(); w++ ) if (!instantiatedP[w] && element_of( w, clusterVars )) {
                const auto& dom = instance->getDomainP(w);
                int last = dom.asvec().back();
                if (!instance->validP(w, instance->getDomainSizeP(w) - 1) && cut.getSupSupportNotLast(w) < last) {
                        last = cut.getSupSupportNotLast(w);
                }
                auto ibeg = std::lower_bound(dom.begin(), dom.end(), std::max(cut.getInfSupport(w), firstValidValueP[w]));
                ++nbDomainIterations;
                for (auto iend = dom.end(); ibeg != iend; ++ibeg) {
                        int a = *ibeg;
                        if (a > last)
                                break;
                        ++nbparentsetvaluechecked;
                        if( set_empty(intersection(instance->getParentSet(w, a), clusterVars)) ) {
                                Score score = getCost(w, a);
                                if (score < minCost) {
                                        minCost = score;
                                        cut.setSupportVar(w);
                                        cut.setSupportVal(a);
                                }
                                if (minCost == 0) return 0;
                                if ( a >= lastModifiedValueP[w] || instance->getCost(w, a) - deltaCostMax[w] >= minCost ) {
                                        break;
                                }
                        }
                }
        }

        assert(minCost>0);
        ++nbclusterpoolactive;
        return minCost;
}

bool
DAG::propagateClusters( int *currentParents, int *currentOrdering, int currentLevel, bool *instantiatedP,
                        int currentOrder, Score currentLB, int **bestParents, int **bestOrdering, Score *currentUB, bool *restart )
{
    // Find the cluster cuts from currentLevel-1 that are still valid
    // Use the valid ones to increase the lower bound

    static int counter{0};
    ++counter;

#if 0
    printf("--------------------------------------------------\nCall %d @%d\n",
           counter, currentLevel);
    printf("Current instantiation:\n");
    for (int i = 0; i <= currentLevel; ++i)
        printf("\tP[%d] = %d\n", currentOrdering[i], currentParents[currentOrdering[i]]);
    for (int i = 0; i <= currentLevel; ++i)
        printf("\tO[%d] = %d\n", i, currentOrdering[i]);
    fflush(stdout);
#endif

    bool nodeconsistency = false;


    auto iterCut = clusterCuts.begin();
    while(iterCut != clusterCuts.end()) {
        const auto &cCut = *iterCut;
        Score minCost = clusterCost(cCut, currentParents, instantiatedP);
        if (minCost == 0) {
            auto iterCutErase = iterCut;
            ++iterCut;
            if (cCut.getSize() > CLUSTERPOOLMINSIZE &&
                cCut.getActivity() * CLUSTERPOOLACTIVITYRATIO < cCut.getNbChecked()) {
                clusterCuts.erase(iterCutErase);
            }
            continue;
        }
        if (minCost == MAX_SCORE)
                return ( false );
        Score prevLB = currentLB;

        // If the cluster cut is still valid, then add it to the cuts of the current level
        //clusterCuts[currentLevel].push_back(cCut);

#if VERBOSE >= 5
        printf("Using saved cluster mincost = %lu ", minCost);
        cCut.print();

        if (minCost >= MAX_SCORE) {
                // First, find the min non-zero cost to be transfered to the clause:
                printf("Full domains:\n");
                auto cluster = cCut.getVariables();
                for( int w = 0; w < instance->nVars(); w++ ) {
                        if( !element_of( w, cluster ) )
                                continue;
                        printf("\tv%d: (", w);
                        for( int a = 0; a <= instance->getLastValidP( w ); a++ ) {
                                Score cost_wa = getCost(w,a, instantiatedP, currentParents);
                                printf(" %d (%lu): ", a, cost_wa);
                                instance->printParentSet(w, a);
                        }
                        printf(")\n");
                }

                printf("minCost = %ld >= MAX_SCORE = %ld\n", minCost, MAX_SCORE);
                fflush(stdout);
        }
#endif

        assert(minCost < MAX_SCORE);

        // Then, transfer the min cost to currentLB (c0_0) from all non-zero costs:
        currentLB += minCost;
        cCut.incActivity();
        if(ge( currentLB, *currentUB )){
#if VERBOSE >= 6
                printf("Call %d, currentLb = %ld >= currentUB = %ld, diff = %ld\n",
                                counter, currentLB, *currentUB, currentLB - *currentUB);
#endif
#ifdef EXTRADEBUG
                assert(debugBacktrack(currentLevel, currentOrdering));
#endif
                return (false);
        }
        nodeconsistency = true;

        Set clusterVars = cCut.getVariables();
        for( int w = 0; w < instance->nVars(); w++ ) if( !instantiatedP[w] && element_of( w, clusterVars ) ) {
                auto& dom = instance->getDomainP(w);
                int last = dom.asvec().back();
                if (!instance->validP(w, instance->getDomainSizeP(w) - 1) && cCut.getSupSupportNotLast(w) < last) {
                        last = cCut.getSupSupportNotLast(w);
                }
                auto ibeg = std::lower_bound(dom.begin(), dom.end(), std::max(cCut.getInfSupport(w), firstValidValueP[w]));
                ++nbDomainIterations;
                for (auto iend = dom.end(); ibeg != iend; ++ibeg) {
                    int a = *ibeg;
                    if (a > last)
                        break;
                    if( set_empty(intersection(instance->getParentSet(w, a), clusterVars)) ) {
                        Score cost_wa = getCost(w,a);
                        if ( cost_wa > 0 && !ge(cost_wa + prevLB, *currentUB) ) {
                            dom.incReducedCost(a, minCost);
                            if (dom.getDeltaCost(a) > deltaCostMax[w]) {
                                deltaCostMax[w] = dom.getDeltaCost(a);
                            }
                            if (a > lastModifiedValueP[w]) {
                                lastModifiedValueP[w] = a;
                            }
                        }
                    }
                }
        }

        ++iterCut;
    }
    bool LBimproves = true;
    int iter = 0;
    while(LBimproves){
        LBimproves = false;
        Score prevLB = currentLB;

        Set cluster = orderVariables_0rc(currentParents, currentOrdering,
            currentLevel, instantiatedP, currentOrder);
        if (set_empty(cluster)) {
            if (opt.gacsupport) {
                if (supportCost < *currentUB) {
                    *currentUB
                        = updateBestOrdering(*bestParents, *bestOrdering);
                    printf(FGREEN "%s: backtracking solution %0.*f, time = "
                                  "%0.3f, level = %d (LP primal)\n" FNONE,
                        instance->benchmarkName(), precision,
                        ((double)supportCost) / scale, timer->elapsedTime(),
                        currentLevel);
                    if (opt.printsolutions)
                        instance->printSolution(
                            *bestOrdering, *bestParents, *currentUB);
                    if (opt.restarts) {
                        *restart = true;
                        return (false);
                    }
                }
                if (ge(currentLB, *currentUB)) {
#ifdef EXTRADEBUG
                    assert(debugBacktrack(currentLevel, currentOrdering));
#endif
                    return false;
                }
            }
            break;
        }

        cluster = minimizeCluster(cluster, instantiatedP, currentParents);

        // Add the minimized cluster to the set of cluster cuts
        Score minCost = MAX_SCORE;

        // First, find the min non-zero cost to be transfered to the lower bound:
        for( int w = 0; w < instance->nVars(); w++ ) if( element_of( w, cluster ) ) {
            for (int a : instance->getDomainP(w)) {
                Score cost_wa = getCost(w,a, instantiatedP, currentParents);
                if( cost_wa > 0 && cost_wa < MAX_SCORE && !ge(cost_wa + prevLB, *currentUB)
                    && set_empty( intersection( instance->getParentSet(w, a), cluster ) ) ) {
                    if (cost_wa < minCost) {
                        minCost = cost_wa;
                    }
                    if (a > lastModifiedValueP[w]) {
                        break;
                    }
                }
            }
        }
        assert(minCost > 0);
        nodeconsistency = true;
        clusterCuts.insert(ClusterCut(instance, cluster, lastClusterCutID));
        lastClusterCutID++;

#if VERBOSE >= 2
        printf(FRED "Using new cluster mincost = %lu at depth %d (LB: %lu, UB: %lu)" FNONE, minCost, currentLevel, currentLB, currentUB);
#if VERBOSE >= 3
        printf(FRED " with " FNONE);
        cCut.print();
#else
        printf("\n");
#endif
#endif
#if VERBOSE >= 4
        printf("Full domains:\n");
        for( int w = 0; w < instance->nVars(); w++ ) {
            if( !element_of( w, cluster ) )
                continue;
            printf("\tv%d: (", w);
            for( int a = 0; a <= instance->getLastValidP( w ); a++ ) {
                Score cost_wa = getCost(w,a, instantiatedP, currentParents);
                if (cost_wa == minCost) {
                        printf(" %d (" FPURPLE "%lu" FNONE "): ", a, cost_wa);
                } else {
                        printf(" %d (%lu): ", a, cost_wa);
                }
                instance->printParentSet(w, a);
            }
            printf(")\n");
        }
#endif

        LBimproves = (minCost > 0) ? true:false;
        if (LBimproves) iter++;
        if (minCost == MAX_SCORE)
                return ( false );
        // Then, transfer the min cost to currentLB (c0_0) from all non-zero costs:
        currentLB += minCost;
        if(ge( currentLB, *currentUB )){
#if 0
            printf("Call %d, currentLb = %ld >= currentUB = %ld, diff = %ld\n",
                   counter, currentLB, currentUB, currentLB - currentUB);
#endif
#ifdef EXTRADEBUG
            assert(debugBacktrack(currentLevel, currentOrdering));
#endif
            return (false);
        }

        for( int w = 0; w < instance->nVars(); w++ ) if( element_of( w, cluster ) ) {
            assert(!instantiatedP[w]);
            auto& wdom = instance->getDomainP(w);
            for (int a : wdom) {
                Score cost_wa = getCost(w,a, instantiatedP, currentParents);
                if( cost_wa > 0 && cost_wa < MAX_SCORE && !ge(cost_wa + prevLB, *currentUB) && set_empty( intersection( instance->getParentSet(w, a), cluster ) ) ) {
                        wdom.incReducedCost(a, minCost);
                        if (wdom.getDeltaCost(a) > deltaCostMax[w]) {
                            deltaCostMax[w] = wdom.getDeltaCost(a);
                        }
                        if (a > lastModifiedValueP[w]) {
                                lastModifiedValueP[w] = a;
                        }
                }
            }
        }
    }

    if (nodeconsistency) {
        nbNC++;
        // If LB is improved and not (yet) equal to UB, then do node consistency:
        for( int w = 0; w < instance->nVars(); w++ ) if( !instantiatedP[w] ) {
                int moreThanOne = 0;
                for (int a : instance->getDomainP(w)) {
                    Score costEst = getCost(w,a) + currentLB;
                    if (costEst==currentLB) {
                        moreThanOne++;
                    }
                    if( ge( costEst, *currentUB ) ) {
                        instance->setValidP( w, a, currentLevel );
                        if( instance->getCurrentDomainSizeP( w ) == 0 ) {
                            return( false );
                        }
                    }
                }
                assert(moreThanOne >= 1);
                if (moreThanOne <= 1) {
                        nbone++;
                }
        }
    }

    if (currentOrder == instance->nVars() && printonlyonce) {
#if VERBOSE >= 1
        char fname[256];
        strcpy(fname, instance->benchmarkName());
        strcat(fname, "_cuts_pre.lp");
        FILE *file = fopen(fname,"w");
        dump_01LP(file);
        fclose(file);
#endif
        printf("Before backtrack search, currentLB: %0.*f currentUB: %0.*f\n", precision, (double)currentLB / scale, precision, (double)*currentUB / scale);
        printonlyonce = false;
    }
    return ( true );
}

// this one does GAC++
bool
DAG::isAcyclic( int *currentOrdering, int currentLevel, int *currentParents, bool *instantiatedP, int v )
{
    validOrdering.clear();
    validOrderingIndexOf.clear();
    support.clear();
    supportIndex.clear();
    supportDepth.clear();
    supportOrdering.clear();
    validOrdering.resize(instance->nVars());
    validOrderingIndexOf.resize(instance->nVars());
    support.resize(instance->nVars());
    supportIndex.resize(instance->nVars());
    supportDepth.resize(instance->nVars());
    supportOrdering.resize(instance->nVars());
    supportCost = 0;

    Set inOrdering = emptySet;
    for( int i = 0; i < currentLevel + ((v < instance->nVars())?1:0); i++ ) {
                add_element( currentOrdering[i], inOrdering );
                validOrdering[i] = currentOrdering[i]; // new
                validOrderingIndexOf[currentOrdering[i]] = i; // new
                int avar = currentOrdering[i];
                supportCost += instance->getCost( avar, currentParents[avar] );
                supportIndex[avar] = currentParents[avar];
                supportOrdering[i] = avar;
    }

    int k = currentLevel+((v < instance->nVars())?1:0);
    bool changes;
    do {
        changes = false;
        for( int w = 0; w < instance->nVars(); w++ ) {
            if( element_of( w, inOrdering ) )
                continue;
            const auto &wdom = instance->getDomainP(w);
            int a = wdom.hasSubsetOf(inOrdering);
            if (a < 0)
                continue;

            // found a support, add it to the ordering
            Set parent = wdom.getParentSet(a);
            add_element( w, inOrdering );
            validOrdering[k] = w; // new
            validOrderingIndexOf[w] = k; // new
            support[w] = parent;
            supportIndex[w] = a;
            supportOrdering[k] = w;
            supportCost += instance->getCost( w, a );
            k++;
            changes = true;
        }
    } while( changes );

    vO_buf = validOrdering;
    vOIdx_buf = validOrderingIndexOf;

    if( k != instance->nVars() ) {
        return( false );
    }

    if (!opt.GAC)
        return true;

    Set lastvar_support = emptySet; // any var in here cannot go into lastvars
    Set lastvars = emptySet;
    for( int j = instance->nVars()-1; j >= 0; --j) {
        int x = validOrdering[j];
        if( element_of(x, lastvar_support) )
            break;
        add_element(x, lastvars);
        lastvar_support = union(lastvar_support, support[x]);
    }

    for( int x = 0; x < instance->nVars(); x++ ) if( !instantiatedP[x] ) {
        if (element_of(x, lastvars)) continue;

        validOrdering = vO_buf;
        validOrderingIndexOf = vOIdx_buf;

        bool pushedDown;
        int i = validOrderingIndexOf[x]; // x is at position i in the valid ordering
        Set varsPrecedingX = emptySet;
        Set varsSucceedingX = emptySet;

        for( int j = 0; j < i; j++ ){
            add_element( validOrdering[j], varsPrecedingX );
        }

        for( int j = i+1; j < instance->nVars(); j++ ){
            add_element( validOrdering[j], varsSucceedingX );
        }

        int finalxpos = i;
        do {
            pushedDown = false;
            for( int j = i+1; j < instance->nVars(); j++ ) if( !element_of( validOrdering[j], varsPrecedingX ) ) {
                    int y = validOrdering[j];
                    if ( subset_of(support[y], varsPrecedingX) ) {
                        add_element( y, varsPrecedingX );
                        remove_element( y, varsSucceedingX );
                        pushedDown = true;
                        ++finalxpos;
                        continue;
                    }

                    const auto& ydom = instance->getDomainP(y);
                    if (ydom.hasSubsetOf(varsPrecedingX) >= 0) {
                        add_element( y, varsPrecedingX );
                        remove_element(y, varsSucceedingX);
                        pushedDown = true;
                        ++finalxpos;
                    }
                }
        } while( pushedDown );

        // now variable x reached its latest possible position in the valid ordering

        assert(finalxpos < instance->nVars());
        if (finalxpos == instance->nVars()-1) {
            // it's in last position, everything is supported
            continue;
        }
        if (element_of(x, lastvars)) {
            printf ("var %d in lastvars but only reached pos %d/%d in pushdown\n",
                    x, finalxpos, instance->nVars()-1);
            fflush(stdout);
            assert(0);
        }
        if (instance->getDomainP(x).removeSetsIntersecting(varsSucceedingX, currentLevel))
            return false;
    }

    return( true );
}

/*
 *  Satisfiability test. Alternate version.
 *  This version is quite a bit slower than isAcyclic()
 *  but could be used to prune the ordering and
 *  depth variables.
 *
 *  For every subset C of the variables, there must exist a
 *  v in C such that some of v's parents are not in C.
 */
bool
DAG::isAcyclic( int *currentOrdering, int *currentDepth, int currentLevel )
{
    int ordering[instance->nVars()];
    int depth[instance->nVars()];
    int cd = currentDepth[currentLevel];
    int k = currentLevel+1;
    for( int i = 0; i < k; i++ ) {
        ordering[i] = currentOrdering[i];
        depth[i] = currentDepth[i];
    }
    bool inOrdering[instance->nVars()];
    for( int i = 0; i < instance->nVars(); i++ ) {
        inOrdering[i] = false;
    }
    for( int i = 0; i < k; i++ ) {
        int w = ordering[i];
        inOrdering[w] = true;
    }

    /*
     *  Step 1. Complete finding all the remaining w not
     *          yet in the ordering that belong at depth d.
     *          A variable w belongs at depth d if there exists a
     *          p in dom(w) such that depth( p | ... ) = d
     */
    int m = k;
    for( int w = 0; w < instance->nVars(); w++ ) if( !inOrdering[w] ) {
        for (int a : instance->getDomainP(w)) {
            int d = instance->getDepthParentSet( ordering, depth, m, w, a );
            if( d == cd ) {
                ordering[k] = w;
                depth[k] = cd;
                inOrdering[w] = true;
                k++;
                break;
            }
        }
    }
    m = k;
    /*
     *  Step 2. d++
     *          while not all of the variables are in the ordering
     *              find all w not yet in ordering that belong at depth d
     *              if there are no such variables, return false
     *              d++
     */
    cd++;
    while( m < instance->nVars() ) {
        for( int w = 0; w < instance->nVars(); w++ ) if( !inOrdering[w] ) {
            for (int a : instance->getDomainP(w)) {
                int d = instance->getDepthParentSet(
                            ordering, depth, m, w, a );
                if( d == cd ) {
                    ordering[k] = w;
                    depth[k] = cd;
                    inOrdering[w] = true;
                    k++;
                    break;
                }
            }
        }

        /*
         *  If the depth layer is empty, then unsatisfiable.
         */
        if( m == k ) {
            //printf("(k) cycle detected, current level = %d\n", currentLevel);
            return( false );
        }

        m = k;
        cd++;
    }

    return( true );
}

/*
 *  Satisfiability test when v = a.
 *
 *  For every subset C of the variables, there must exist a
 *  v in C such that some of v's parents are not in C.
 */
bool
DAG::isAcyclic( int *currentOrdering, int currentLevel, int v, int a )
{
    Set inOrdering = emptySet;
    for( int i = 0; i <= currentLevel; i++ ) {
        add_element( currentOrdering[i], inOrdering );
    }

    int k = currentLevel+1;
    bool changes;
    do {
        changes = false;
        if( !element_of( v, inOrdering ) ) {
            Set parent = instance->getParentSet( v, a );
            if( subset_of( parent, inOrdering ) ) {
                add_element( v, inOrdering );
                k++;
                changes = true;
            }
        }
        for( int w = 0; w < instance->nVars(); w++ ) if( (w != v) && !element_of( w, inOrdering ) ) {
            for (int a : instance->getDomainP(w)) {
                Set parent = instance->getParentSet( w, a );
                if( subset_of( parent, inOrdering ) ) {
                    add_element( w, inOrdering );
                    k++;
                    changes = true;
                    break;
                }
            }
        }
    } while( changes );

    if( k != instance->nVars() ) {
        return( false );
    }

    return( true );
}

void DAG::printStats()
{
        printf("\nNumber of cluster cuts: %lu\n", clusterCuts.size());
#if VERBOSE >= 1
        for (auto cut: clusterCuts) {
                cut.print();
        }
        char fname[256];
        strcpy(fname, instance->benchmarkName());
        strcat(fname, "_cuts.lp");
        FILE *file = fopen(fname,"w");
        dump_01LP(file);
        fclose(file);
#endif

        printf("\nNC calls: %lld nbVACSingletonVariables: %lld ratio: %g", nbNC, nbone, (double)nbone/nbNC);

    printf("\nDomain iterations: %lld\n", nbDomainIterations);
    if (nbclusterpoolchecked>0) {
        printf("Percentage of productive cluster cuts: %f %% (%lld / %lld)\n", 100. * nbclusterpoolactive/nbclusterpoolchecked, nbclusterpoolactive, nbclusterpoolchecked);

        printf("Percentage of unproductive cluster cuts quickly detected: %f %% (%lld / %lld)\n", 100. * nbclusterpoolfastzero/(nbclusterpoolchecked-nbclusterpoolactive), nbclusterpoolfastzero, nbclusterpoolchecked-nbclusterpoolactive);

        printf("Percentage of parentset domains visited by cluster cut pool analysis: %f %% (%lld / %lld)\n", 100. * nbparentsetvaluechecked / nbparentsetvalueactive, nbparentsetvaluechecked, nbparentsetvalueactive);
    }

}

bool
DAG::debugBacktrack(int currentLevel, const int *currentOrdering) const
{
    for (int i = 0; i <= currentLevel; ++i) {
        if(currentOrdering[i] != debug_solutionO[i])
            return true;
    }
    printf("Backtracking with current assignment\n");
    for (int i = 0; i <= currentLevel; ++i) {
        printf("\tordering[%d] = %d (%d)\n", i, currentOrdering[i], debug_solutionO[i]);
    }
    printf("which matches optimal solution\n");
    fflush(stdout);
    return false;
}

void
DAG::dump_01LP(FILE *file) const
{
        fprintf(file, "minimize\n");
        Score greedyLB = 0;
        for( int w = 0; w < instance->nVars(); w++ ) {
                Score best = instance->getCost(w, 0);
                greedyLB += best;
                for( int a = 0; a < instance->getDomainSizeP( w ); a++ ) {
                        if (w!=0 || a!=0)
                                fprintf(file, " + ");
                        fprintf(file, "%0.*f x%d_%d", precision, (double)(instance->getCost(w, a)-best)/scale, w, a);
                }
        }
        fprintf(file, " + %0.*f\nsubject to\n", precision, (double)greedyLB / scale);
        for( int w = 0; w < instance->nVars(); w++ ) {
                for( int a = 0; a < instance->getDomainSizeP( w ); a++ ) {
                        if (a!=0) fprintf(file, " + ");
                        fprintf(file, "x%d_%d", w, a);
                }
                fprintf(file, " = 1\n");
        }
        for(const auto& cCut : clusterCuts) {
                cCut.dump_01LP(file);
        }
        fprintf(file, "Bounds\n");
        for( int w = 0; w < instance->nVars(); w++ ) {
                for( int a = 0; a < instance->getDomainSizeP( w ); a++ ) {
                        fprintf(file, "0 <= x%d_%d <= 1\n", w, a);
                }
        }
        fprintf(file, "\nend\n");
}

void
DAG::printParentSetsWithDeltaCosts(int v, const bool *instantiatedP, const int *currentParents) const
{
    const auto& vdom = instance->getDomainP(v);
    for (int a : vdom) {
        Set parentSet = instance->getParentSet( v, a );
        printf( "{" );
        for( int i = 0; i < instance->nVars(); i++ ) {
            if( element_of( i, parentSet ) ) {
                printf( " %d", i );
            }
        }
        printf( " } " );
        printf("Cost: %ld  Delta: %ld  DAGcost: %ld\n",
               vdom.getCost(a), vdom.getDeltaCost(a),
               getCost(v, a, instantiatedP, currentParents));
    }
}
