/*
 *  Routines for determining a lower bound on the cost of an instance
 *  of the combinatorial optimization problem for learning a Bayesian
 *  network from data.
 */
#ifndef _LOWERBOUND_H_
#define _LOWERBOUND_H_

class Instance;
class Graph;
class Pattern;
class LocalSearch;

class Bound
{
    public:
        Bound( Instance *instance, const Options& options );
        ~Bound();

        void printNonZeroQueries();

        /*
         *  Greedily construct a lower bound on the cost of completing
         *  a partial solution by picking the lowest cost domain element
         *  for each variable. Relies on the domains being sorted by
         *  increasing costs.
         */
        Score getGreedyLowerBound( bool *instantiated );

        /*
         *  Greedily construct an upper bound on the cost of completing
         *  a partial solution by picking the highest cost domain element
         *  for each variable. Relies on the domains being sorted by
         *  increasing costs.
         */
        Score getGreedyUpperBound( bool *instantiated );

        /*
         *  Construct a lower bound on the cost of completing
         *  a partial solution using static partitioning.
         *
         *  The idea is to partition the vertices into blocks
         *  of fixed size and find the optimal solution within
         *  each partitiion using dynamic programming.
         */
        Score getLowerBound( bool *instantiated );

        /*
         *  Construct an upper bound on the best solution using
         *  local search.
         */
        Score getUpperBound(
                int *bestOrdering, int *bestSolution, int strength );

        LocalSearch *getLocalSearch() {return localSearch.get();}

        /*
         *  Construct the partitions and pattern databases for
         *  the lower bound computation.
         */
        void partition( bool *instantiated );
        void repartition( bool *instantiated );

    private:
        const Options& opt;
        Instance *instance;
        std::unique_ptr<Graph> graph;

        /*
         *  Routines for finding upper bounds.
         */
        std::unique_ptr<LocalSearch> localSearch;

        /*
         *  Routines and data structures for finding lower bounds.
         *  The idea is to partition the vertices into blocks and
         *  find the optimal solution within each partition using
         *  dynamic programming.
         *
         *    MAX_TOP: use top = 1, ..., MAX_TOP of lowest cost parent sets
         *    MAX_ELEM_SMALL: max elements in pattern db for partition
         *    MAX_ELEM_LARGE: max elements in pattern db for repartition
         */
        static const int MAX_TOP = 10;
        Pattern **pattern;
        void partitionAux( bool *instantiated, int maxPartitionSize );
        Score fillBlock( int i );
        Score fillPartition();
        void improvePartition();
        void coalescePartition( int maxPartitionSize );
#ifdef DYNSET
        int** block; // i^th block's elements
        int* last;                   // i^th block's number of elements
#else
        int* block[MAX_VARS]; // i^th block's elements
        int last[MAX_VARS];                  // i^th block's number of elements
#endif
        int nBlocks;                         // number of blocks in partition
};

#endif // _LOWERBOUND_H_
