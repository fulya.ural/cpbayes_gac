--------------------------------------------------------------------------------------------------------
Bayesian Network Structure Learning solver based on CPBayes source code (Van Beek and Hoffmann, CP 2015)
--------------------------------------------------------------------------------------------------------

On linux, compile using a recent C++ compiler (g++-8 or above):

>make

It produces four excutables for different maximum problem size (number of Bayesian network variables):
search64 for instances with less than 64 variables
search128 for instances with less than 128 variables
search256 for instances with less than 256 variables
search512 for instances with less than 512 variables

Command line parameters for the solvers are ('[]' means optional parameter(s) with their default value given after '|'):

>./searchXXX inFile [timeLimit|3600] [partitionBoundMin|20] [partitionBoundMax|26] [localSearchRestartsMin|50] [localSearchRestartsMax|500] [widthPrefixPermute|4]

Run the right solver depending on your problem size. 
E.g. for a 1-hour CPU-time limit,  

> ./search64 kdd.test.jkl 3600 20 20 15 30

