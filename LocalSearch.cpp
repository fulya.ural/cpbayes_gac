/*
 *  Routines for performing local search on an instance of the
 *  combinatorial optimization problem for learning a Bayesian
 *  network from data.
 */

#include "Instance.h"
#include "LocalSearch.h"

/*
 *  Constructor for local search.
 */
LocalSearch::LocalSearch( Instance *instance, const Options& options )
    : instance(instance),
      opt(options)
{
    srand( 0 );
}

/*
 *  Destructor for local search.
 */
LocalSearch::~LocalSearch()
{
}

/*
 *  Given an ordering of m of the parent variables, m <= n,
 *  find the variable that is not in the ordering and has the lowest
 *  cost parent set (domain value) that is a subset of the variables
 *  in the ordering. Returns -1 if there is no such variable.
 */
int
LocalSearch::findSmallestConsistentWithOrdering( int *ordering, int m )
{
    /*
     *  Step through the domains of the variables not in the ordering,
     *  looking for smallest cost among those consistent with the
     *  variables in the ordering.
     */
    Set inOrdering = emptySet;
    for( int i = 0; i < m; i++ ) {
        add_element( ordering[i], inOrdering );
    }

    int minVar = -1;
    Score minCost = MAX_SCORE;
    for( int v = 0; v < instance->nVars(); v++ ) if( !element_of( v, inOrdering ) ) {
        for( int a = 0; a < instance->getDomainSizeP( v ); a++ ) if( instance->validP( v, a ) ) {
            Set p = instance->getParentSet( v, a );
            if( subset_of( p, inOrdering ) ) {
                if( minCost > instance->getCost( v, a ) ) {
                    minCost = instance->getCost( v, a );
                    minVar = v;
                }
            }
        }
    }

    return( minVar );
}

/*
 *  Given an ordering of m of the parent variables, m <= n,
 *  find the variable that is not in the ordering and has the lowest
 *  cost parent set (domain value) that is a subset of the variables
 *  in the ordering. Returns -1 if there is no such variable.
 *  The randomized version returns a randomly selected variable
 *  from among the MAX_VALUES variables with the lowest cost.
 */
int
LocalSearch::findSmallestConsistentWithOrderingRandom( int *ordering, int m )
{
    /*
     *  Set inOrdering to true for all vertices that are
     *  in the ordering; otherwise, false.
     */
    Set inOrdering = emptySet;
    for( int i = 0; i < m; i++ ) {
        add_element( ordering[i], inOrdering );
    }

    /*
     *  Step through the domains of the variables not in the ordering,
     *  looking for smallest cost among those disjoint with the
     *  variables in the ordering.
     */
    int minVar[MAX_VALUES];
    Score minCost[MAX_VALUES];
    for( int i = 0; i < MAX_VALUES; i++ ) {
        minVar[i] = -1;
        minCost[i] = MAX_SCORE;
    }
    for( int v = 0; v < instance->nVars(); v++ ) if( !element_of( v, inOrdering ) ) {
        for( int a = 0; a < instance->getDomainSizeP( v ); a++ ) if( instance->validP( v, a ) ) {
            Set p = instance->getParentSet( v, a );
            if( subset_of( p, inOrdering ) ) {
                Score s = instance->getCost( v, a );
                if( minCost[MAX_VALUES-1] > s ) {
                    /*
                     *  Insert s into the right place in the ordered sequence.
                     *  Find where to insert it, shift the other values up,
                     *  then insert it.
                     */
                    int l = 0;
                    while( (s > minCost[l]) && (l < MAX_VALUES-1) ) {
                        l++;
                    }
                    for( int k = l; k < MAX_VALUES-1; k++ ) {
                        minCost[k+1] = minCost[k];
                        minVar[k+1] = minVar[k];
                    }
                    minCost[l] = s;
                    minVar[l] = v;
                }
            }
        }
    }

    int mod = MAX_VALUES;
    while( (minVar[mod-1] == -1) && (mod > 1) ) {
        mod--;
    }
    return( minVar[rand() % mod] );
}


/*
 *  Greedily construct an ordering in forward order, starting
 *  with first position. Pick node with smallest cost consistent
 *  with ordering so far and place in next open position. Repeat.
 */
void
LocalSearch::greedyHeuristic(
        int *ordering, int *solution, Score *cost )
{
    /*
     *  Determine the ordering.
     */
    for( int i = 0; i < instance->nVars(); i++ ) {
        int v = findSmallestConsistentWithOrdering( ordering, i );
        ordering[i] = v;
    }

    *cost = instance->minCostOrdering( emptySet,
        ordering, instance->nVars(), solution );
}

/*
 *  Same as the greedyHeuristic except that
 *  the randomized heuristic randomly picks from among a
 *  small number of nodes with the smallest cost.
 */
void
LocalSearch::greedyRandomizedHeuristic(
        int *ordering, int *solution, Score *cost )
{
    /*
     *  Determine the ordering.
     */
    for( int i = 0; i < instance->nVars(); i++ ) {
        int v = findSmallestConsistentWithOrderingRandom( ordering, i );
        ordering[i] = v;
    }

    *cost = instance->minCostOrdering( emptySet,
        ordering, instance->nVars(), solution );
}

/*
 *  Perform a simple hill-climbing search using swaps as
 *  the neighborhood function and first-improvement move.
 *  Assumes that ordering has been initialized and that cost
 *  represents the cost of the initial ordering.
 */
void
LocalSearch::hillClimbingFirstImprovement(
        int *ordering, int *solution, Score *cost )
{
    bool improving = false;
    int newSolution[instance->nVars()];

    do {
        improving = false;
        for( int i =   0; i < instance->nVars() && !improving; i++ )
        for( int j = i+1; j < instance->nVars() && !improving; j++ ) {

            /*
             *  Determine change in costs that would result from swap.
             */
            Score aCost = 0;
            for( int k = i; k <= j; k++ ) {
                int v = ordering[k];
                int p = solution[v];
                aCost += instance->getCost( v, p );
            }

            int v = ordering[i];
            ordering[i] = ordering[j];
            ordering[j] = v;

            Score bCost = 0;
            bool nosolution = false;
            for( int k = i; k <= j; k++ ) {
                int v = ordering[k];
                int p = instance->getMinCostParentSet_any( ordering, k, v );
                if (p == -1) {
                        nosolution = true;
                        break;
                }
                bCost += instance->getCost( v, p );
                newSolution[v] = p;
            }
            Score newCost = *cost - aCost + bCost;

            if( !nosolution && gt( *cost, newCost ) ) {
                /*
                 *  Update the new cost and the solution.
                 */
                *cost = newCost;
                for( int k = i; k <= j; k++ ) {
                    int v = ordering[k];
                    solution[v] = newSolution[v];
                }
                improving = true;
            }
            else {
                int v = ordering[i];
                ordering[i] = ordering[j];
                ordering[j] = v;
            }
        }
    } while( improving );
}

/*
 *  Perform a simple hill-climbing search using swaps as
 *  the neighborhood function and best-improvement move.
 *  Assumes that ordering has been initialized and that cost
 *  represents the cost of the initial ordering.
 */
void
LocalSearch::hillClimbingBestImprovement(
        int *ordering, int *solution, Score *cost )
{
    bool improving = false;
    int best_i = 0;
    int best_j = 0;

    do {
        improving = false;
        Score saveCost = *cost;
        for( int i =   0; i < instance->nVars(); i++ )
        for( int j = i+1; j < instance->nVars(); j++ ) {

            /*
             *  Determine change in costs that would result from swap.
             */
            Score aCost = 0;
            for( int k = i; k <= j; k++ ) {
                int v = ordering[k];
                int p = solution[v];
                aCost += instance->getCost( v, p );
            }

            int v = ordering[i];
            ordering[i] = ordering[j];
            ordering[j] = v;

            Score bCost = 0;
            bool nosolution = false;
            for( int k = i; k <= j; k++ ) {
                int v = ordering[k];
                int p = instance->getMinCostParentSet_any( ordering, k, v );
                if (p == -1) {
                        nosolution = true;
                        break;
                }
                bCost += instance->getCost( v, p );
            }
            Score newCost = saveCost - aCost + bCost;

            if( !nosolution && gt( *cost, newCost ) ) {
                *cost = newCost;
                best_i = i;
                best_j = j;
                improving = true;
            }
            v = ordering[i];
            ordering[i] = ordering[j];
            ordering[j] = v;
        }

        if( improving ) {
            int v = ordering[best_i];
            ordering[best_i] = ordering[best_j];
            ordering[best_j] = v;
            /*
             *  Update solution corresponding to best move.
             */
            *cost = instance->minCostOrdering( emptySet,
                ordering, instance->nVars(), solution );
        }

    } while( improving );
}

/*
 *  Sort the ordering by the depth of the variables.
 */
void
LocalSearch::sortByDepth( int *ordering, int *solution )
{
        int depth[instance->nVars()];

        for( int i = 0; i < instance->nVars(); i++ ) {
                int v = ordering[i];
                depth[i] = instance->getDepthParentSetUnordered(
                                ordering, depth, i, v, solution[v] );
        }

        for( int i = 0; i < instance->nVars(); i++ ) {
                int first = i;
                for( int j = i+1; j < instance->nVars(); j++ ) {
                        if( (depth[j] < depth[first]) ||
                                        ((depth[j] == depth[first]) &&
                                                        (ordering[j] < ordering[first])) ) {
                                first = j;
                        }
                }

                int t = ordering[i];
                ordering[i] = ordering[first];
                ordering[first] = t;

                t = depth[i];
                depth[i] = depth[first];
                depth[first] = t;
        }
}

/*
 *  Hill-climbing local search with restarts.
 */
Score
LocalSearch::hillClimbWithRestarts(
                int *bestOrdering, int *bestSolution, int nRestarts )
{
    Score cost;
    Score ub = MAX_SCORE;
    int ordering[instance->nVars()];
    int solution[instance->nVars()];

    for( int i = 0; i < nRestarts; i++ ) {
        /*
         *  Find a good, random initial ordering.
         */
        if (i==0 && instance->initial_ordering) { // if given as file input: starts from an initial ordering
                for (int j=0; j < instance->nVars(); j++) {
                        ordering[j] = instance->initial_ordering[j];
                }
                cost = instance->minCostOrdering(emptySet, ordering, instance->nVars(), solution);
        } else {
                greedyRandomizedHeuristic( ordering, solution, &cost );
        }
        /*
         *  Perform first improvement hill climbing.
         *  First improvement appears to work the best.
         */
        hillClimbingFirstImprovement( ordering, solution, &cost );
        //hillClimbingBestImprovement( ordering, solution, &cost );

        if( gt( ub, cost ) ) {
                ub = cost;
                for( int v = 0; v < instance->nVars(); v++ ) {
                        bestOrdering[v] = ordering[v];
                }
        }
    }

    /*
     *  Determine best solution for ordering.
     */
    ub = instance->minCostOrdering( emptySet,
        bestOrdering,
        instance->nVars(),
        bestSolution );

    /*
     *  Sort ordering by depth and return the min cost.
     *  Sorted ordering guaranteed to have a min cost that
     *  is less than or equal to that of the original ordering.
     */
    sortByDepth( bestOrdering, bestSolution );

    /*
     *  Determine cost and best solution for sorted ordering.
     */
    ub = instance->minCostOrdering( emptySet,
        bestOrdering,
        instance->nVars(),
        bestSolution );

    return( ub );
}

