
#include <stdio.h>

#include <numeric>
#include <optional>

#include "Instance.h"
#include "BacktrackingSearch.h"
#include "Bound.h"
#include "LocalSearch.h"
#include "Options.hpp"

// return an empty optional if opts contains neither "-opt" nor
// "-noopt", otherwise the optional contains true or false,
// respectively, and it modifies opts to remove that argument
std::optional<bool> has_opt(const std::string& opt, std::vector<std::string>& opts)
{
    std::string yesopt = "-" + opt;
    std::string noopt = "-no" + opt;
    for(auto i = opts.begin(); i != opts.end(); ++i) {
        if (*i == yesopt) {
            opts.erase(i);
            return true;
        } else if (*i == noopt) {
            opts.erase(i);
            return false;
        }
    }
    return std::nullopt;
}

void store_opt(bool& store, const std::string& opt, std::vector<std::string>& opts)
{
    auto has = has_opt(opt, opts);
    if (has)
        store = *has;
}

bool has_more_positional(const std::vector<std::string>& opts)
{
    return !opts.empty();
}

std::string get_next_positional(std::vector<std::string>& opts)
{
    auto s = opts.front();
    opts.erase(opts.begin());
    return s;
}

Options parse_cmdline_options(int argc, char *argv[])
{
    Options opt;

    std::vector<std::string> opts(argv+1, argv+argc);
    opt.cmdline = std::accumulate(argv+1, argv+argc, std::string(argv[0]),
                                  [&](std::string cmdline, char *arg) {
                                      return std::move(cmdline) + ' ' + arg;
                                  });


    store_opt(opt.printsolutions, "print-solutions", opts);
    store_opt(opt.GAC, "gac", opts);
    store_opt(opt.gacsupport, "gacsupport", opts);
    store_opt(opt.RCclusters, "cluster", opts);
    store_opt(opt.restarts, "restart", opts);
    store_opt(opt.mincostdtrees, "mincostdtrees", opts);

    auto dtreeopt = has_opt("dtrees", opts);
    if (dtreeopt) {
        if (*dtreeopt)
            opt.dtrees = dtree_use::TREES;
        else
            opt.dtrees = dtree_use::NONE;
    }

    auto bestorderingopt = has_opt("dtree-best-order", opts);
    if (bestorderingopt) {
        if (*bestorderingopt)
            opt.dtree_heuristic = dtree_construction::BO;
        else
            opt.dtree_heuristic = dtree_construction::IG;
    }

    // positional arguments
    if (!has_more_positional(opts)) {
        fprintf( stderr, "Usage: search inFile [timeLimit|3600] [partitionBoundMin|20] [partitionBoundMax|26] [localSearchRestartsMin|50] [localSearchRestartsMax|500] [widthPrefixPermute|4] [initialOrderFile]\n [opts]" );
        exit( 6 );
    }
    opt.inputfile = get_next_positional(opts);

    if (has_more_positional(opts))
        opt.timeLimit = stof(get_next_positional(opts));

    if (has_more_positional(opts))
        opt.bound_max_elem_small = stoi(get_next_positional(opts));

    if (has_more_positional(opts))
        opt.bound_max_elem_large = stoi(get_next_positional(opts));

    if (has_more_positional(opts))
        opt.ls_nrestarts_small = stoi(get_next_positional(opts));

    if (has_more_positional(opts))
        opt.ls_nrestarts_large = stoi(get_next_positional(opts));

    if (has_more_positional(opts))
        opt.bt_width_prefix_permute = stoi(get_next_positional(opts));

    if (has_more_positional(opts)) {
        opt.hassolutionfile = true;
        opt.solutionfile = get_next_positional(opts);
    }

    return opt;
}

int main( int argc, char *argv[] )
{
    Options opt = parse_cmdline_options(argc, argv);

    printf( "\n%s:\n", opt.inputfile.c_str() );

#ifdef DYNSET
    FILE *fp;
    int n;
    if( (fp = fopen( opt.inputfile.c_str(), "r" )) == NULL ) {
        fprintf( stderr, "Could not open file %s\n", argv[1]);
        exit( 6 );
    }
    fscanf( fp, "%d\n", &n );
    MAX_VARS = n;
    fclose(fp);
#endif
    Instance instance( opt );

    if (opt.hassolutionfile) {
        FILE *fsol;
        if( (fsol = fopen( opt.solutionfile.c_str(), "r" )) == NULL ) {
                fprintf( stderr, "Could not open file %s\n", opt.solutionfile.c_str());
                exit( 6 );
        }
        instance.initial_ordering = new int[instance.nVars()]();
        for (int i=0; i<instance.nVars(); i++) {
                fscanf( fsol, "%d", &instance.initial_ordering[i]);
        }
        fclose(fsol);
    }

    BacktrackingSearch btSearch( &instance, opt );

    /*
     *  Find the minimum cost solution and its associated cost.
     */
    int ordering[instance.nVars()];
    int solution[instance.nVars()];
    Score cost;
    btSearch.solve( ordering, solution, &cost );
    //instance.printSolution( ordering, solution, cost );
}

