
#include <stdio.h>
#include "Graph.h"

/*
 *  Constructor for graph.
 */
Graph::Graph( int n )
{
    this->n = n;

    graph = new bool*[n]();
    for( int v = 0; v < n; v++ ) {
	graph[v] = new bool[n]();
    }

    clear();
}

/*
 *  Destructor for graph.
 */
Graph::~Graph()
{
    for( int i = 0; i < n; i++ ) {
	delete[] graph[i];
    }
    delete[] graph;
}

void
Graph::clear()
{
    for( int v = 0; v < n; v++ ) {
	for( int w = 0; w < n; w++ ) {
	    graph[v][w] = false;
	}
    }
}

/*
 *  Number of vertices in graph.
 */
int
Graph::nVertices()
{
    return( n );
}

/*
 *  Add directed edge (v, w) to the graph.
 */
void
Graph::addEdge( int v, int w )
{
    graph[v][w] = true;
}

/*
 *  Kosaraju's algorithm to find all of the strongly connected
 *  components of a directed graph.
 */
void
Graph::findSCCs()
{
    sccs.clear();
    std::stack<int> stack;

    /*
     *  Perform a DFS and push vertex on stack in order of
     *  finishing expansion (deepest first).
     */
    bool visited[n];
    for( int i = 0; i < n; i++ )
	visited[i] = false;

    for( int v = 0; v < n; v++ ) {
        if( !visited[v] ) {
            depthFirstSearch( v, visited, stack );
	}
    }

    /*
     *  Perform a DFS on reversed graph processing vertices in
     *  order of stack.
     */
    bool inComponent[n];
    for( int i = 0; i < n; i++ ) {
         inComponent[i] = false;
    }

    while( !stack.empty() ) {

        int v = stack.top();
        stack.pop();

        if( !inComponent[v] ) {
            sccs.push_back(Component());
            reverseDFS( v, inComponent, stack );
        }
    }
}

/*
 *  Sort the SCCs by descending order of size.
 */
void
Graph::sortSCCs()
{
    for( unsigned int i = 0; i < sccs.size(); i++ ) {
	unsigned int first = i;
	for( unsigned int j = i+1; j < sccs.size(); j++ ) {
            if( sccs[j].size() > sccs[first].size() ) {
                first = j;
            }
	}

	std::vector<int> t = sccs[i];
	sccs[i] = sccs[first];
	sccs[first] = t;
    }
}

/*
 *  Return the size of the largest SCC.
 */
int
Graph::maxSCC()
{
    unsigned int max = 0;
    for( unsigned int j = 0; j < sccs.size(); j++ ) {
	if( max < sccs[j].size() ) {
	    max = sccs[j].size();
	}
    }

    return( max );
}

void
Graph::depthFirstSearch( int v, bool *visited, std::stack<int> &stack )
{
    visited[v] = true;

    for( int w = 0; w < n; w++ ) {
        if( graph[v][w] && !visited[w] )
            depthFirstSearch( w, visited, stack );
    }

    stack.push(v);
}

void
Graph::reverseDFS( int v, bool *inComponent, std::stack<int> &stack )
{
    /*
     *  Add v to the component that is currently built
     *  and continue search for other component elements.
     */
    sccs.back().push_back(v);
    inComponent[v] = true;

    for( int w = 0; w < n; w++ ) {
        if( graph[w][v] && !inComponent[w] ) {
            reverseDFS( w, inComponent, stack );
        }
    }
}

int
Graph::nSCCs()
{
    return( sccs.size() );
}

Graph::Component
Graph::getSCC( int i )
{
    return( sccs[i] );
}

/*
 *  Print out the graph for debug purposes.
 */
void
Graph::dumpGraph()
{
/*
    for( int v = 0; v < n; v++ ) {
	printf( "v_%d:", v );
	for( int w = 0; w < n; w++ ) {
	    if( graph[v][w] ) {
		printf( " v_%d", w );
	    }
	}
	printf( "\n" );
    }
*/

    printf( "Number of SCCs = %d\n", (int)sccs.size() );
    for( std::vector<Component>::iterator s = sccs.begin(); s != sccs.end(); s++ ) {
	printf( "%2d:", (int)s->size() );
	for( Component::iterator i = s->begin(); i != s->end(); i++ ) {
	    printf( " %d", *i );
	}
	printf( "\n" );
    }

    /*
     *  Verify that all edges are forward edges from an SCC i to an
     *  SCC j, where i < j. Only holds true if SCCs not sorted.
     */
/*
    for( std::vector<Component>::iterator s = sccs.begin(); s != sccs.end(); s++ )
    for( Component::iterator i = s->begin(); i != s->end(); i++ ) {
	for( std::vector<Component>::iterator t = s+1; t != sccs.end(); t++ )
	for( Component::iterator j = t->begin(); j != t->end(); j++ ) {
	    if( graph[*j][*i] ) {
		printf( "Not okay (%d, %d)\n", *i, *j );
	    }
	}
    }
*/
}

