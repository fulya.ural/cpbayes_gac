#ifndef ELSA_TYPES_HPP_
#define ELSA_TYPES_HPP_

#include <cstdint>
#include <bitset>

//#define COLORS

//fonts color
#ifdef COLORS
#define FBLACK      "\033[0;30m"
#define FRED        "\033[0;31m"
#define FGREEN      "\033[0;32m"
#define FYELLOW     "\033[0;33m"
#define FBLUE       "\033[0;34m"
#define FPURPLE     "\033[0;35m"
#define FNONE       "\033[0m"
#else
#define FBLACK      ""
#define FRED        ""
#define FGREEN      ""
#define FYELLOW     ""
#define FBLUE       ""
#define FPURPLE     ""
#define FNONE       ""
#endif

/*
 *  Definitions for scores.
 *
 *  Defines are in case the type definition changes.
 *  Using floats or doubles instead is possible but
 *  testing equality or even less than is problemetic.
 *  Would need something along these lines:
 *      static const double epsilon = 1.0e-05;
 *      #define lessThan(a,b) ((a - b) < -epsilon)
 */
typedef int64_t Score;
inline constexpr bool gt(Score a, Score b) { return a > b; }
inline constexpr bool ge(Score a, Score b) { return a >= b; }
inline constexpr bool lt(Score a, Score b) { return a < b; }
inline constexpr bool le(Score a, Score b) { return a <= b; }
static const Score MAX_SCORE = 9223372036854775807LL;// larger than any possible score
static const double scale = 100000.0;   // 5 digits of precision
static const int precision = 5;         // 5 digits of precision

/*
 *  Definitions for parent sets.
 */

// choose one of these implementation
//#define SET32
//#define SET64
//#define SET128
//#define BITSET 192
//#define DYNSET

#ifdef SET32
typedef uint32_t Set;
#define set_empty(s) (s == 0)
#define complement(s) (~s)
#define add_element(e,s) (s |= ((Set)1 << e))
#define remove_element(e,s) (s ^= ((Set)1 << e))
#define element_of(e,s) (s & ((Set)1 << e))
#define subset_of(s,t) ((s & t) == s)
#define intersection(s,t) (s & t)
#define union(s,t) (s | t)
#define set_equal(s,t) (s == t)
#define emptySet     ((Set)0x00000000)
#define universalSet ((Set)0xFFFFFFFF)
static const int MAX_VARS = 32; // sizeof(Set)

inline constexpr int count(Set s)
{
    int c{0};
    while (s) {
        ++c;
        s = s & (s-1);
    }
    return c;
}
#endif

#ifdef SET64
typedef uint64_t Set;
#define set_empty(s) (s == 0)
#define complement(s) (~s)
#define add_element(e,s) (s |= ((Set)1 << e))
#define remove_element(e,s) (s ^= ((Set)1 << e))
#define element_of(e,s) (s & ((Set)1 << e))
#define subset_of(s,t) ((s & t) == s)
#define intersection(s,t) (s & t)
#define union(s,t) (s | t)
#define set_equal(s,t) (s == t)
#define emptySet ((Set)0x0000000000000000)
#define universalSet ((Set)0xFFFFFFFFFFFFFFFF)
static const int MAX_VARS = 64; // sizeof(Set)

inline constexpr int count(Set s)
{
    int c{0};
    while (s) {
        ++c;
        s = s & (s-1);
    }
    return c;
}
#endif

#ifdef SET128
typedef unsigned __int128 Set;
#define set_empty(s) (s == 0)
#define complement(s) (~s)
#define add_element(e,s) (s |= ((Set)1 << e))
#define remove_element(e,s) (s ^= ((Set)1 << e))
#define element_of(e,s) (s & ((Set)1 << e))
#define subset_of(s,t) ((s & t) == s)
#define intersection(s,t) (s & t)
#define union(s,t) (s | t)
#define set_equal(s,t) (s == t)
#define emptySet ((Set)0)
#define universalSet (complement(emptySet))
static const int MAX_VARS = 128; // sizeof(Set)

inline constexpr int count(Set s)
{
    int c{0};
    while (s) {
        ++c;
        s = s & (s-1);
    }
    return c;
}
#endif

#ifdef BITSET
typedef std::bitset<BITSET> Set;
#define set_empty(s) ((s).none())
#define complement(s) (~s)
#define add_element(e,s) ((s).set(e))
#define remove_element(e,s) ((s).reset(e))
#define element_of(e,s) ((s).test(e))
#define subset_of(s,t) ((s & t) == s)
#define intersection(s,t) (s & t)
#define union(s,t) (s | t)
#define set_equal(s,t) (s == t)
#define emptySet (Set())
#define universalSet (complement(emptySet))
static const int MAX_VARS = BITSET; // sizeof(Set)

inline int count(const Set& s)
{
    return s.count();
}
#endif

#ifdef DYNSET
#include <boost/dynamic_bitset.hpp>
typedef boost::dynamic_bitset<uint64_t> Set;
#define set_empty(s) ((s).none())
#define complement(s) (~s)
#define add_element(e,s) ((s).set(e))
#define remove_element(e,s) ((s).reset(e))
#define element_of(e,s) ((s).test(e))
#define subset_of(s,t) ((s & t) == s)
#define intersection(s,t) (s & t)
#define union(s,t) (s | t)
#define set_equal(s,t) (s == t)
#define emptySet (Set(MAX_VARS))
#define universalSet (complement(emptySet))
static int MAX_VARS = 0;
inline int count(const Set& s)
{
    return s.count();
}
#endif

//needed for Pattern database memory addresses
typedef uint64_t Set64;
#define set_empty64(s) (s == 0)
#define complement64(s) (~s)
#define add_element64(e,s) (s |= ((Set64)1 << e))
#define remove_element64(e,s) (s ^= ((Set64)1 << e))
#define element_of64(e,s) (s & ((Set64)1 << e))
#define subset_of64(s,t) ((s & t) == s)
#define intersection64(s,t) (s & t)
#define union64(s,t) (s | t)
#define set_equal64(s,t) (s == t)
#define emptySet64     ((Set64)0x0000000000000000)
#define universalSet64 ((Set64)0xFFFFFFFFFFFFFFFF)
inline constexpr int count64(Set64 s)
{
    return __builtin_popcountl(s);
}


#endif

