#include "ZDD.h"
#include "Instance.h"
#include <algorithm>
#include <cmath>
#include <boost/functional/hash.hpp>
#include <unordered_map>
#include <memory>
#include <queue>
#include <set>
#include <cassert>
#include <fstream>
#include <inttypes.h>
#include <stack>
#include <random>

#include <iostream>
#include <utility>

#include <boost/multiprecision/float128.hpp>

using std::optional;
using std::nullopt;
typedef std::pair<ZDDnoderef, ZDDnoderef> pair;

struct pair_hash
{
    template <class T1, class T2>
    std::size_t operator() (const std::pair<T1, T2> &pair) const
    {
        return std::hash<int>()(pair.first.id) ^ std::hash<int>()(pair.second.id);

    }
};

ZDD::ZDD(int nvars, std::span<Set> values, std::span<Score> costs, int j,
    dtree_construction h)
    : nvars(nvars)
    , thisVariable(j)
    , width(1)
    , maxDepth(0)
    , nbNodes(1)
    , nbLevels(nvars)
    , heuristic(h)
{
    unionOfValues = emptySet;
    for (auto& s : values)
        unionOfValues = union(s, unionOfValues);

    for (int i = 0; i < nvars; i++) {
        nodesAtLevel.push_back({});
        widthAtLevel.push_back(0);
    }

    root = newNode();
    froot = root;
    setParent(root, {});
    nodesAtLevel[0].push_back(root);
    widthAtLevel[0]++;

    initialize(values, costs);
}

ZDD::ZDD(int nvars, std::span<Set> values, std::span<Score> costs, int j,
    dtree_construction h, std::span<int> bestorder)
    : nvars(nvars)
    , thisVariable(j)
    , width(1)
    , maxDepth(0)
    , nbNodes(1)
    , nbLevels(nvars)
    , heuristic(h)
    , bestOrdering(bestorder)
{
    unionOfValues = emptySet;
    for (auto& s : values)
        unionOfValues = union(s, unionOfValues);

    for (int i = 0; i < nvars; i++) {
        nodesAtLevel.push_back({});
        widthAtLevel.push_back(0);
    }

    root = newNode();
    froot = root;
    setParent(root, {});
    nodesAtLevel[0].push_back(root);
    widthAtLevel[0]++;

    initialize(values, costs);
}

void ZDD::initialize(std::span<Set> values, std::span<Score> costs)
{
    constructBinaryTree(values, costs);
    // mergeNodes();
    verifyValues(values);
    assert(verifyPaths(root) == static_cast<int>(values.size()));
}

ZDDnoderef ZDD::newNode()
{
    nodes.push_back({});
    node_branchinfo.push_back({});
    return ZDDnoderef(nodes.size()-1);
}

bool ZDD::isSink(ZDDnoderef r)
{
    auto *n = deref(r.id);
    return n->falseChild.isnull() && n->trueChild.isnull();
}

ZDDnode* ZDD::deref(int id)
{
    assert(id >= 0);
    assert(static_cast<unsigned>(id) < nodes.size());
    return &nodes[id];
}

void ZDD::restoreValue(Set value)
{
    assert(!valueExists(value));

    ZDDnoderef currentNoderef = root;
    while (!isSink(currentNoderef)) {
        ZDDnode* currentNode = deref(currentNoderef.id);
        assert(consistent(value, currentNoderef));
        if(element_of(varIndex(currentNoderef), value)){
            assert(currentNode->trueChild.nonnull());
            if(currentNode->trueChild.nonnull()){
                currentNode->trueEdgeCounter++;
                currentNoderef = currentNode->trueChild;
                continue;
            }
        }
        else{
            assert(currentNode->falseChild.nonnull());
            if(currentNode->falseChild.nonnull()){
                currentNode->falseEdgeCounter++;
                currentNoderef = currentNode->falseChild;
                continue;
            }
        }
    }

    froot = root; // we could be more clever about it and only push
                  // froot back up to the first branching node, but it
                  // will be set to its correct value in the first
                  // subset query
}

void ZDD::restoreValues(const std::vector<int>& values)
{
    auto& gains = restore_gained_paths;
    auto& q = restore_q;
    auto& inq = restore_inq;
    froot = root;

    if (gains.size() < nodes.size()) {
        gains.resize(nodes.size());
        inq.resize(nodes.size());
    }

    q.clear();
    for(int a : values) {
        auto nref = val_node[a];
        q.push_back(nref);
        inq[nref.id] = true;
        gains[nref.id] = 1;
        assert(isSink(nref));
    }

    // bottom up breadth first traversal, starting from the leaves
    // corresponding to the restored values
    size_t qhead = 0;
    while(qhead != q.size()) {
        ZDDnoderef nref = q[qhead];
        ZDDnoderef pref = parent[nref.id];
        ++qhead;

        if (nref == root)
            continue;

        // update parent's gain in #paths and its true/false edge
        // counter, as appropriate
        auto *p = deref(pref.id);
        assert(nref == p->trueChild || nref == p->falseChild);
        bool truechild = (nref == p->trueChild);
        p->trueEdgeCounter += truechild * gains[nref.id];
        p->falseEdgeCounter += (1-truechild)*gains[nref.id];
        gains[pref.id] += gains[nref.id];

        // reset gains/inq for current node, so that it is ready for
        // the next time restoreValues() is called
        gains[nref.id] = 0;
        inq[nref.id] = 0;

        // parent in queue
        if (!inq[pref.id]) {
            q.push_back(pref);
            inq[pref.id] = true;
        }
    }

    assert(gains[root.id] == static_cast<int>(values.size()));
}

ZDD::variableChoice ZDD::getImplied(
    detail::DTBuildState& state, int, ZDDnoderef curref, Set, Set)
{
    variableChoice rv { -1, universalSet, universalSet, universalSet };
    Set impliedFalse { universalSet };
    for (auto it = state.node_begin[curref.id], eit = state.node_end[curref.id];
         it != eit; ++it) {
        auto& value = *it;
        rv.implied = intersection(rv.implied, value);
        impliedFalse = intersection(impliedFalse, complement(value));
        rv.mask = union(rv.implied, impliedFalse);
        rv.all_intersection = intersection(rv.all_intersection, value);
    }
    // limit mask to the variables we actually have
    Set allvars { emptySet };
    for (int i = 0; i != nvars; ++i)
        add_element(i, allvars);
    rv.mask = intersection(rv.mask, allvars);
    return rv;
}

// lexicographic order
ZDD::variableChoice ZDD::chooseBranchVar_Lex(detail::DTBuildState& state,
    int currentLevel, ZDDnoderef curref, Set seenVars, Set path)
{
    auto rv = getImplied(state, currentLevel, curref, seenVars, path);
    for (int i = 0; i < nvars; i++) {
        if (i == thisVariable || element_of(i, seenVars)
            || element_of(i, rv.mask))
            continue;
        rv.var = i;
        return rv;
    }
    return rv;
}

// best ordering found so far
ZDD::variableChoice ZDD::chooseBranchVar_BO(detail::DTBuildState& state,
    int currentLevel, ZDDnoderef curref, Set seenVars, Set path)
{
    if (bestOrdering.empty())
        throw std::logic_error(
            "BestOrdering branching for dtrees, but no ordering provided");
    auto rv = getImplied(state, currentLevel, curref, seenVars, path);
    for (int i = 0; i < nvars; i++) {
        int q = bestOrdering[i];
        if (q == thisVariable || element_of(q, seenVars)
            || element_of(q, rv.mask))
            continue;
        rv.var = q;
        return rv;
    }
    return { -1, emptySet, emptySet };
}

ZDD::variableChoice ZDD::chooseBranchVar_IG(detail::DTBuildState& state,
    int currentLevel, ZDDnoderef curref, Set seenVars, Set path)
{
    variableChoice rv { -1, emptySet, emptySet };

    for (int i = 0; i < nvars; i++) {
        if (i == thisVariable || element_of(i, seenVars))
            continue;

        using IGtype = boost::multiprecision::float128;
        IGtype maxIG = -1.0 * std::numeric_limits<double>::max();
        int n = nvars;

        int domSizeWith = 0;
        int domSizeWithout = 0;
        Set all_intersection { universalSet };
        for (auto it = state.node_begin[curref.id],
                  eit = state.node_end[curref.id];
             it != eit; ++it) {
            auto& value = *it;
            if (!set_equal(path, intersection(value, seenVars)))
                continue;
            if (element_of(i, value))
                domSizeWith++;
            else
                domSizeWithout++;
            all_intersection = intersection(all_intersection, value);
        }
        // it is redundant to assign this for every variable
        // we evaluate, but the result is always the same
        rv.all_intersection = all_intersection;

        assert((domSizeWith + domSizeWithout) > 0);
        if (domSizeWithout == 0) {
            // we see this variable in all remaining sets
            add_element(i, rv.implied);
            add_element(i, rv.mask);
            continue;
        }
        if (domSizeWith == 0) {
            // we do not see this variable in any remaining sets
            assert(domSizeWithout > 0);
            add_element(i, rv.mask);
            continue;
        }

        IGtype probWith, probWithout, H, H_1, H_0, IG;
        probWith = 0.5; /*(double)domSizeWith / (double)domSize;*/
        probWithout = 0.5; /*(double)domSizeWithout / (double)domSize;*/
        if (n - currentLevel - 2 >= 100) {
            IG = -1.0 * probWith * (IGtype)domSizeWith
                    * (log2((IGtype)domSizeWith)
                        - (IGtype)(n - currentLevel - 2))
                - probWithout * (IGtype)domSizeWithout
                    * (log2((IGtype)domSizeWithout)
                        - (IGtype)(n - currentLevel - 2));
        } else {
            /*
             * Sometimes a subset of the following cases happen:
             * 1. two_pow_n_minus_one = domSize
             * 2. two_pow_n_minus_two = domSizeWith
             * 3. two_pow_n_minus_two = domSizeWithout
             * When this is the case, log2(0) would turn out to be -nan, then
             * the associated H value would also be -nan and I would fail to
             * pick a varWithMaxIG. In order to avoid this, I take log2(0) as
             * minus infinity and not -nan, which makes the associated H value
             * equal to 0.
             */

            IGtype two_pow_n_minus_two = pow(2.0, n - currentLevel - 2);
            H = 0.0;
            if ((two_pow_n_minus_two - (IGtype)domSizeWith) == 0.0)
                H_1 = 0.0;
            else
                H_1 = -1.0 * ((IGtype)domSizeWith / two_pow_n_minus_two)
                        * log2((IGtype)domSizeWith / two_pow_n_minus_two)
                    - ((two_pow_n_minus_two - (IGtype)domSizeWith)
                          / two_pow_n_minus_two)
                        * log2((two_pow_n_minus_two - (IGtype)domSizeWith)
                            / two_pow_n_minus_two);
            if ((two_pow_n_minus_two - (IGtype)domSizeWithout) == 0.0)
                H_0 = 0.0;
            else
                H_0 = -1.0 * ((IGtype)domSizeWithout / two_pow_n_minus_two)
                        * log2((IGtype)domSizeWithout / two_pow_n_minus_two)
                    - ((two_pow_n_minus_two - (IGtype)domSizeWithout)
                          / two_pow_n_minus_two)
                        * log2((two_pow_n_minus_two - (IGtype)domSizeWithout)
                            / two_pow_n_minus_two);
            IG = H - (probWith * H_1 + probWithout * H_0);
        }
        if (IG > maxIG || currentLevel == nbLevels - 2) {
            maxIG = IG;
            rv.var = i;
        }
    }
    return rv;
}

ZDD::variableChoice ZDD::chooseBranchVar(detail::DTBuildState& state,
    int currentLevel, ZDDnoderef curref, Set seenVars, Set path)
{
    switch (heuristic) {
    case dtree_construction::IG:
    case dtree_construction::IG_VEB:
        return chooseBranchVar_IG(state, currentLevel, curref, seenVars, path);
    case dtree_construction::LEX:
    case dtree_construction::LEX_VEB:
        return chooseBranchVar_Lex(state, currentLevel, curref, seenVars, path);
    case dtree_construction::BO:
    case dtree_construction::BO_VEB:
        return chooseBranchVar_Lex(state, currentLevel, curref, seenVars, path);
    default:
        return {};
    }
}

void ZDD::exploreNode(detail::DTBuildState& state,
                      ZDDnoderef currentNodeRef,
                      int currentLevel,
                      int falseCounter,
                      int trueCounter,
                      Set seenVars,
                      Set path)
{
    ZDDnode* currentNode = deref(currentNodeRef.id);

    auto branch
        = chooseBranchVar(state, currentLevel, currentNodeRef, seenVars, path);

    // we do this regardless whether branch.var == -1 or not,
    // because we may be at a leaf only after the implied literals
    auto& myi = node_branchinfo[currentNodeRef.id];
    myi.mask = union(myi.mask, branch.mask);
    myi.path = union(myi.path, branch.implied);

    if (parent[currentNodeRef.id].nonnull()) {
        auto& parenti = node_branchinfo[parent[currentNodeRef.id].id];
        myi.mask = union(myi.mask, parenti.mask);
        myi.path = union(myi.path, parenti.path);
    }

    assert(currentNodeRef == root
        || subset_of(node_branchinfo[parent[currentNodeRef.id].id].path,
            node_branchinfo[currentNodeRef.id].path));
    assert(currentNodeRef == root
        || subset_of(node_branchinfo[parent[currentNodeRef.id].id].mask,
            node_branchinfo[currentNodeRef.id].mask));

    if (branch.var == -1) {
        return;
    }

    if (currentLevel > maxDepth)
        maxDepth = currentLevel;

    setVarIndex(currentNodeRef, branch.var);
    seenVars = union(seenVars, branch.mask);
    path = union(path, branch.implied);

    auto mid = std::partition(state.node_begin[currentNodeRef.id],
        state.node_end[currentNodeRef.id],
        [&](Set pset) { return !element_of(branch.var, pset); });
    assert(mid != state.node_begin[currentNodeRef.id]
        && mid != state.node_end[currentNodeRef.id]);

    // create a new false child for the current node
    auto newFalseChild = newNode();
    // have to refetch because newNode() may have reallocated
    currentNode = deref(currentNodeRef.id);
    currentNode->falseChild.reset(newFalseChild.id);
    setParent(currentNode->falseChild, currentNodeRef);
    nodesAtLevel[currentLevel + 1].push_back(newFalseChild);
    widthAtLevel[currentLevel + 1]++;
    nbNodes++;
    if (widthAtLevel[currentLevel + 1] > width)
        width = widthAtLevel[currentLevel + 1];
    add_element(branch.var, seenVars);
    node_branchinfo[newFalseChild.id].path = path;
    node_branchinfo[newFalseChild.id].mask = seenVars;
    add_element(branch.var, node_branchinfo[newFalseChild.id].mask);
    state.init_node(newFalseChild, state.node_begin[currentNodeRef.id], mid);
    exploreNode(state, currentNode->falseChild, currentLevel + 1,
        currentNode->falseEdgeCounter, 0, seenVars, path);

    // create a new true child for the current node
    auto newTrueChild = newNode();
    // have to refetch because newNode() may have reallocated
    currentNode = deref(currentNodeRef.id);

    currentNode->trueChild.reset(newTrueChild.id);
    setParent(currentNode->trueChild, currentNodeRef);
    nodesAtLevel[currentLevel + 1].push_back(newTrueChild);
    widthAtLevel[currentLevel + 1]++;
    nbNodes++;
    if (widthAtLevel[currentLevel + 1] > width)
        width = widthAtLevel[currentLevel + 1];
    add_element(branch.var, seenVars);
    add_element(branch.var, path);
    node_branchinfo[newTrueChild.id].path = path;
    node_branchinfo[newTrueChild.id].mask = seenVars;
    state.init_node(newTrueChild, mid, state.node_end[currentNodeRef.id]);
    exploreNode(state, currentNode->trueChild, currentLevel + 1, 0,
        currentNode->trueEdgeCounter, seenVars, path);
}

bool ZDD::consistent(Set s, ZDDnoderef z)
{
    auto bi = node_branchinfo[z.id];
    return set_equal(bi.path, intersection(bi.mask, s));
}

bool ZDD::subset_consistent(Set s, ZDDnoderef z)
{
    auto bi = node_branchinfo[z.id];
    return subset_of(bi.path, s);
}

void ZDD::mergeNodes()
{
        //using cache_element = std::vector<int>; //lookup key is a vector [falseChildID, trueChildID]

        std::vector<ZDDnoderef> merged;
        auto setmerged = [&] (ZDDnoderef from, ZDDnoderef to) {
            if (merged.size() <= static_cast<size_t>(from.id))
                merged.resize(from.id+1);
            merged[from.id] = to;
        };
        auto getmerged = [&] (ZDDnoderef n) -> ZDDnoderef {
            if (merged.size() <= static_cast<size_t>(n.id))
                return {};
            else
                return merged[n.id];
        };

        for(int i = nbLevels-1; i >= 0; i--) {

                std::unordered_map< pair, ZDDnoderef, pair_hash > nodesAtCurrentLevel;

                for(auto&& currentNoderef : nodesAtLevel[i]) {
                        auto currentNode = deref(currentNoderef.id);
                        std::pair<ZDDnoderef, ZDDnoderef> key{}; //lookup key is a pair <falseChildID, trueChildID>
                        if(currentNode->falseChild.nonnull() && currentNode->falseEdgeCounter > 0) {
                                auto fc = currentNode->falseChild;
                                if (getmerged(fc).nonnull()){
                                        currentNode->falseChild.reset(getmerged(fc).id);
                                }
                                key.first = currentNode->falseChild;
                        }
                        else{
                                key.first = {};
                        }
                        if(currentNode->trueChild.nonnull() && currentNode->trueEdgeCounter > 0){
                                auto tc = currentNode->trueChild;
                                if (getmerged(tc).nonnull())
                                    currentNode->trueChild.reset(getmerged(tc).id);
                                key.second = currentNode->trueChild;
                        }
                        else{
                                key.second = {};
                        }

                        auto got = nodesAtCurrentLevel.find (key);

                        if (got != nodesAtCurrentLevel.end() ){
                                widthAtLevel[i]--;
                                setmerged(currentNoderef, got->second);
                                currentNode->trueChild.reset();
                                currentNode->falseChild.reset();
                        }
                        else {
                                nodesAtCurrentLevel.insert_or_assign(key, currentNoderef);
                        }
                }
        }
}

bool ZDD::markReachable(ZDDnoderef nref)
{
        auto n = deref(nref.id);
        if (isSink(nref)) {
            seen[nref.id] = true;
            return true;
        }
        if (seen[nref.id])
            return true;
        bool rv = false;
        if (n->falseChild.nonnull() && n->falseEdgeCounter > 0)
            rv = markReachable(n->falseChild) || rv;
        if (n->trueChild.nonnull() && n->trueEdgeCounter > 0)
            rv = markReachable(n->trueChild) || rv;
        if (rv)
            seen[nref.id] = true;
        return rv;
}

int ZDD::updateCounters(ZDDnoderef r)
{
    if (isSink(r))
        return 1;
    auto* n = deref(r.id);
    if (n->trueChild.nonnull())
        n->trueEdgeCounter = updateCounters(n->trueChild);
    if (n->falseChild.nonnull())
        n->falseEdgeCounter = updateCounters(n->falseChild);
    return n->trueEdgeCounter + n->falseEdgeCounter;
}

int
ZDD::verifyPaths(ZDDnoderef noderef)
{
    if(isSink(noderef))
        return 1;

    int falseCount = 0;
    int trueCount = 0;
    auto node = deref(noderef.id);

    if(node->falseChild.nonnull() && node->falseEdgeCounter > 0)
        falseCount = verifyPaths(node->falseChild);
    if(node->trueChild.nonnull() && node->trueEdgeCounter > 0)
        trueCount = verifyPaths(node->trueChild);

    return trueCount + falseCount;
}

void
ZDD::removeTrueChildrenOf(ZDDnoderef currentNodeRef, int currentLevel){
    auto theNode = deref(currentNodeRef.id);
    if(theNode->trueChild.nonnull() && theNode->trueEdgeCounter > 0){
        theNode->trueEdgeCounter = 0;
    }
}

void ZDD::pruneParentSet(Set ps)
{
    ZDDnoderef currentNoderef = root;
    ZDDnoderef lastref;
    bool lastchoicetrue{false};

    while(!isSink(currentNoderef)) {
        assert(consistent(ps, currentNoderef));
        ZDDnode* currentNode = deref(currentNoderef.id);
        if(element_of(varIndex(currentNoderef), ps)) {
            assert(currentNode->trueChild.nonnull());
            currentNode->trueEdgeCounter--;
            lastref = currentNoderef;
            lastchoicetrue = true;
            currentNoderef = currentNode->trueChild;
        } else {
            assert(currentNode->falseChild.nonnull());
            currentNode->falseEdgeCounter--;
            lastref = currentNoderef;
            lastchoicetrue = false;
            currentNoderef = currentNode->falseChild;
        }
    }

    ZDDnode* lastnode = deref(lastref.id);
    assert((!lastchoicetrue || lastnode->trueEdgeCounter ==0) &&
           (lastchoicetrue || lastnode->falseEdgeCounter == 0));
}

bool ZDD::hasValueThatIsSubsetOf(Set precedingVars, const optional<DTmask>& mask)
{
    fixEffectiveRoot();
    int a = followSubsetOf(froot, precedingVars, mask);
    return a >= 0;
}

int ZDD::getSubsetOf(Set precedingVars, const optional<DTmask>& mask)
{
    fixEffectiveRoot();
    return followSubsetOf(froot, precedingVars, mask);
}

int ZDD::getMincostSubsetOf(Set prec, const std::optional<DTmask>& mask)
{
    Score ub{MAX_SCORE};
    return minCostSearch(root, prec, &ub, mask);
}

void ZDD::reset_mask(DTmask& mask)
{
    mask.nonmask.resize(nodes.size());
    mask.reset();
    mask.marker = 1;
    mask.unmask(root);
}

void ZDD::unmask_value(DTmask& mask, Set a)
{
    ZDDnoderef cur = root;
    ++mask.marker;
    while(true) {
        mask.unmask(cur);
        {
            auto *n = deref(cur.id);
            assert(n->trueChild.isnull()
                   || n->falseChild.isnull()
                   || n->trueChild.id != n->falseChild.id);
        }
        if (isSink(cur))
            break;
        // follow the approprite edge. The value we are trying to
        // unmask must exist in the unmasked decision tree
        if (element_of(varIndex(cur), a)) {
            assert(hasTrue(cur, nullopt));
            cur = deref(cur.id)->trueChild;
        } else {
            assert(hasFalse(cur, nullopt));
            cur = deref(cur.id)->falseChild;
        }
    }
}

bool ZDD::unmasked(ZDDnoderef noderef, const optional<DTmask>& mask)
{
    if (mask)
        return !mask->masked(noderef);
    else
        return true;
}

bool ZDD::hasTrue(ZDDnoderef noderef, const optional<DTmask>& mask)
{
    auto node = deref(noderef.id);
    return (node->trueChild.nonnull()
            && node->trueEdgeCounter > 0
            && unmasked(node->trueChild, mask));
}

bool ZDD::hasFalse(ZDDnoderef noderef, const optional<DTmask>& mask)
{
    auto node = deref(noderef.id);
    return (node->falseChild.nonnull()
            && node->falseEdgeCounter > 0
            && unmasked(node->falseChild, mask));
}

void ZDD::fixEffectiveRoot()
{
    while (!isSink(froot)) {
        auto node = deref(froot.id);
        if (!hasTrue(froot, nullopt)) {
            froot = node->falseChild;
            continue;
        }
        if (!hasFalse(froot, nullopt)) {
            froot = node->trueChild;
            continue;
        }
        break;
    }
}

int ZDD::followSubsetOf(ZDDnoderef noderef, Set precedingVars,
                        const optional<DTmask>& mask)
{
        if (!subset_consistent(precedingVars, noderef))
            return -1;

        if(isSink(noderef)) {
                return node_val[noderef.id];
        }

        if (node_visit_stat)
            ++(*node_visit_stat);

        auto node = deref(noderef.id);

        if(element_of(varIndex(noderef), precedingVars)) {
            if (hasTrue(noderef, mask)) {
                auto rv = followSubsetOf(node->trueChild,
                                         precedingVars, mask);
                if (rv >= 0)
                    return rv;
            }
        }
        if (hasFalse(noderef, mask)) {
            auto rv = followSubsetOf(node->falseChild,
                                     precedingVars, mask);
            if (rv >= 0)
                return rv;
        }
        return -1;
}

template<typename Q>
void clearQ(Q& q)
{
    struct I_cannot_believe_this : protected Q {
        static void clearit(Q& q) {
            (q.*&I_cannot_believe_this::c).clear();
        }
    };
    I_cannot_believe_this::clearit(q);
};

int ZDD::minCostSearch(ZDDnoderef noderef, Set precedingVars,
                       Score *ub, const optional<DTmask>& mask)
{
    int rv{-1};

    ZDDnoderef n1, n2;

    auto& pq = minCostSearch_q;
    clearQ(pq);
    pq.push({noderef, node_costinfo[noderef.id]});

    while (!pq.empty()) {
        auto qe = pq.top();
        pq.pop();
        auto noderef = qe.r;
        auto node = deref(noderef.id);

        if (minc_node_visit_stat)
            ++(*minc_node_visit_stat);

        if (qe.lb >= *ub)
            continue;

        if (!subset_consistent(precedingVars, noderef))
            continue;

        if(isSink(noderef)) {
            int a = node_val[noderef.id];
            *ub = node_costinfo[noderef.id];
            return a;
        }

        if(element_of(varIndex(noderef), precedingVars)
           && hasTrue(noderef, mask)) {
            pq.push({node->trueChild, node_costinfo[node->trueChild.id]});
        }
        if (hasFalse(noderef, mask)) {
            pq.push({node->falseChild, node_costinfo[node->falseChild.id]});
        }
    }
    return rv;
}

bool ZDD::valueExists(Set ps)
{
    auto currentNodeRef = root;
    auto currentNode = deref(root.id);
    while (!isSink(currentNodeRef)) {
        int var = varIndex(currentNodeRef);
        if (!consistent(ps, currentNodeRef))
            return false;
        if(element_of(var, ps)){
            if(currentNode->trueChild.nonnull() && currentNode->trueEdgeCounter > 0){
                currentNodeRef = currentNode->trueChild;
                currentNode = deref(currentNode->trueChild.id);
                continue;
            }
            return false;
        }
        else{
            if(currentNode->falseChild.nonnull() && currentNode->falseEdgeCounter > 0){
                currentNodeRef = currentNode->falseChild;
                currentNode = deref(currentNode->falseChild.id);
                continue;
            }
            return false;
        }
    }
    assert(isSink(currentNodeRef));
    return true;
}

void ZDD::annotateLeaves(std::span<Set> values, std::span<Score> costs)
{
    node_val.resize(nodes.size());
    node_costinfo.resize(nodes.size());
    val_node.resize(values.size());

    std::vector<ZDDnoderef> costq;
    std::vector<uint8_t> inq(nodes.size());
    // follow the path for each value down to its leaf, annotate the
    // leaf
    for (std::size_t a = 0; a != values.size(); ++a) {
        Set s = values[a];
        ZDDnoderef curref = root;
        ZDDnode* cur = deref(curref.id);
        while (!isSink(curref)) {
            assert(consistent(s, curref));
            int var = varIndex(curref);
            if (element_of(var, s)) {
                assert(cur->trueChild.nonnull());
                curref = cur->trueChild;
            } else {
                assert(cur->falseChild.nonnull());
                curref = cur->falseChild;
            }
            cur = deref(curref.id);
        }
        node_val[curref.id] = a;
        val_node[a] = curref;
        node_costinfo[curref.id] = costs[a];
        // we have to check that its parent is nonnull, because if the
        // domain contains just one value, the tree will have only the
        // root
        if (parent[curref.id].nonnull() && !inq[parent[curref.id].id]) {
            costq.push_back(parent[curref.id]);
            inq[parent[curref.id].id] = true;
        }
    }

    // propagate costs upwards
    std::size_t qhead{0};
    while(qhead != costq.size()) {
        ZDDnoderef nref = costq[qhead];
        ++qhead;
        ZDDnode *n = deref(nref.id);
        auto& lb = node_costinfo[nref.id];
        lb = MAX_SCORE;
        if (n->falseChild.nonnull())
            lb = std::min(lb, node_costinfo[n->falseChild.id]);
        if (n->trueChild.nonnull())
            lb = std::min(lb, node_costinfo[n->trueChild.id]);
        ZDDnoderef p = parent[nref.id];
        if (p.nonnull() && !inq[p.id]) {
            costq.push_back(p);
            inq[p.id] = true;
        }
    }
}

void ZDD::setParent(ZDDnoderef cref, ZDDnoderef pref)
{
    while (parent.size() <= static_cast<size_t>(cref.id))
        parent.push_back({});

    parent[cref.id] = pref;
}

void ZDD::relayout()
{
    std::vector<ZDDnode> newnodes;
    std::vector<ZDDnoderef> newrefs(nodes.size());

    nodeLevel.resize(nodes.size());
    nodeLevel[root.id] = 0;
    for_all_descendants(root, [&](ZDDnoderef r) {
        nodeLevel[r.id] = nodeLevel[parent[r.id].id] + 1;
        maxDepth = std::max(maxDepth, nodeLevel[r.id]);
        return false;
    });
    relayout_levels(root, 0, maxDepth, newnodes, newrefs);
    assert(newnodes.size() == nodes.size());

    swap(nodes, newnodes);
    remap_ref(root, newrefs);
    for (auto& n : nodes) {
        remap_ref(n.trueChild, newrefs);
        remap_ref(n.falseChild, newrefs);
    }
    for (auto& l : nodesAtLevel)
        remap_refs(l, newrefs);
    remap_reverse(nodeLevel, newrefs);
    remap_reverse(node_branchinfo, newrefs);
    remap_reverse(node_costinfo, newrefs);
    remap_reverse(node_val, newrefs);
    remap_refs(val_node, newrefs);
    remap_refs(parent, newrefs);
}

void ZDD::remap_ref(ZDDnoderef& r, const std::vector<ZDDnoderef>& newrefs)
{
    if (r.nonnull())
        r = newrefs[r.id];
}

void ZDD::remap_refs(std::vector<ZDDnoderef>& v,
                     const std::vector<ZDDnoderef>& newrefs)
{
    for (ZDDnoderef& r : v)
        remap_ref(r, newrefs);
}

template<typename T>
void ZDD::remap_reverse(std::vector<T>& v,
                        const std::vector<ZDDnoderef>& newrefs)
{
    std::vector<T> newv(v.size());
    for(size_t i = 0; i != v.size(); ++i)
        newv[newrefs[i].id] = v[i];
    swap(v, newv);
}

template<typename CB>
void ZDD::for_all_children(ZDDnoderef root, CB cb)
{
    auto* node = deref(root.id);
    if (node->falseChild.nonnull())
        cb(node->falseChild);
    if (node->trueChild.nonnull())
        cb(node->trueChild);
}

// call CB for all descendands of root. Stop descending if cb return
// true
template<typename CB>
void ZDD::for_all_descendants(ZDDnoderef root, CB cb)
{
    for_all_children(root, [&](ZDDnoderef nr) {
        if (cb(nr))
            return;
        for_all_descendants(nr, cb);
    });
}

// relayout the tree from levels from to to, starting at position
// startpos in newnodes.
void ZDD::relayout_levels(ZDDnoderef root, int from, int to,
                          std::vector<ZDDnode>& newnodes,
                          std::vector<ZDDnoderef>& newrefs)
{
    if (from == to) {
        newnodes.push_back(nodes[root.id]);
        newrefs[root.id] = ZDDnoderef(newnodes.size()-1);
        return;
    }

    int mid = std::midpoint(from, to);
    relayout_levels(root, from, mid, newnodes, newrefs);

    for_all_descendants(root, [&](ZDDnoderef n) {
        if (nodeLevel[n.id] == mid+1) {
            relayout_levels(n, mid+1, to, newnodes, newrefs);
            return true;
        }
        return false;
    });
}

void ZDD::constructBinaryTree(std::span<Set> values, std::span<Score> costs)
{
    detail::DTBuildState state;
    state.vals.assign(values.begin(), values.end());
    state.init_node(root, begin(state.vals), end(state.vals));
    exploreNode(state, root, 0, 0, 0, emptySet, emptySet);
    updateCounters(root);
    if (heuristic == dtree_construction::LEX_VEB
        || heuristic == dtree_construction::IG_VEB)
        relayout();
    annotateLeaves(values, costs);
}
